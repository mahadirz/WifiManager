<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 11/13/2015
 * Time: 7:58 PM
 */



$factory('App\Models\WifiManagerPackages', [
    'name' => $faker->lexify('??????????'),
    'descriptions' => $faker->sentence($nbWords = 6),
    'max_users' => $faker->randomDigitNotNull,
    'max_connections_per_user' => $faker->randomDigitNotNull,
    'enable_user_support' => $faker->numberBetween($min = 0, $max = 1) ,
    'enable_user_billing' => $faker->numberBetween($min = 0, $max = 1) ,
    'monthly_price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 1, $max = 20),
    'semi_annually_price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 60, $max = 150),
    'annually_price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 200, $max = 240),
]);

$factory('App\Models\User', [
    'first_name' => $faker->firstName,
    'last_name' => $faker->lastName,
    'username' => $faker->userName,
    'email' => $faker->email,
    'password' => $faker->password(6),
]);