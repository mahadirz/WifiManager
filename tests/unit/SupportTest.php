<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/15/2015
 * Time: 10:04 AM
 */

use App\Libraries\Supports\Support;
use App\Models\Ticket;
use App\Models\Message;


class SupportTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Support
     */
    protected $support;

    protected function _before()
    {
        $this->support = new Support();
    }

    protected function _after()
    {
    }

    /**
     * Test Setter and Getter for Message and Ticket
     * Setter and getter are using trait
     */
    public function testSetterGetter()
    {
        $message = new Message();
        $ticket = new Ticket();
        $this->support->setMessage($message);
        $this->support->setTicket($ticket);

        $this->assertEquals($message, $this->support->getMessage());
        $this->assertEquals($ticket, $this->support->getTicket());
    }

    /**
     * Test new Ticket and message created
     */
    public function testCreateNewTicketMessage()
    {
        $body = "test message";
        $title = "test title";
        $sender_id = 5;
        $receiver_id = 2;
        $this->support->createNewTicket($title,$sender_id,$receiver_id)->addMessage($body);
        $this->tester->seeRecord('messages', ['body' => $body, 'read' => false]);
        $this->tester->seeRecord('tickets', ['title' => $title, 'status' => 'open']);
        //dd($support);
    }
}
