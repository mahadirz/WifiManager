<?php

class RegisterManagerTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected static $admin_id = 2;

    /**
     * Run before each method
     */
    protected function _before()
    {
        Auth::loginUsingId(self::$admin_id);
    }


    /**
     * Run after each method
     */
    protected function _after()
    {
    }

    public function testRegisterManager()
    {
        $this->tester->amOnRoute('admin.manager.getRegister');
        $this->tester->fillField('username','manager_username');
        $this->tester->fillField('first_name','Manager');
        $this->tester->fillField('last_name','Manager');
        $this->tester->fillField('email','manager@wifimanager.app');
        $this->tester->fillField('password','zaq123');
        $this->tester->fillField('password2','zaq123');
        $this->tester->fillField('password2','zaq123');
        $this->tester->click('Submit');
        $this->tester->canSee('The Manager has been registered!');
    }


}

?>
