<?php

class LoginTest extends \Codeception\TestCase\Test
{
    /**
     * @var \FunctionalTester
     */
    protected $tester;

    protected static $admin_username = "admin";
    protected static $admin_password = "zaq123";


    /**
     * Run before each method
     */
    protected function _before()
    {
    }

    /**
     * Run after each method
     */
    protected function _after()
    {
    }

    /**
     *Login as admin
     */
    public function testValidloginAsAdmin()
    {
        $this->tester->amOnRoute('login.index');
        $this->tester->fillField('username', self::$admin_username);
        $this->tester->fillField('password', self::$admin_password);
        $this->tester->click('Log In');
        $this->tester->see('Admin\'s Panel');
    }
    
}

?>
