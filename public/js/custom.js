/**
 * Created by Mahadir on 10/9/2015.
 */
function messageBox(title,text,putTo,type,icon){
    if(type==null){
        type = "warning";
    }
    if(icon==null){
        icon = "warning";
    }
    var boxHtml = '<div id="confirm-box-js" class="message-box message-box-warning animated fadeIn open" ' +
        'id="message-box-'+type+'"><div class="mb-container">' +
        '<div class="mb-middle"><div class="mb-title"><span class="fa fa-'+icon+'"' +
        '</span> '+title+'</div><div class="mb-content">'+text+'</div><div class="mb-footer">' +
        '<button class="btn btn-default btn-lg mb-control-yes pull-right">OK</button>' +
        '</div></div></div></div>';

    $(putTo).html(boxHtml);
    var box = $('#confirm-box-js');

    $('.mb-control-yes').on('click',function(){
        $( ".mb-control-yes" ).off( "click", "**" );
        box.removeClass("open");
    });

    return true;
}

function confirmBox(title,text,putTo,type,icon,yesCallback,noCallback){
    if(type==null){
        type = "warning";
    }
    if(icon==null){
        icon = "warning";
    }
    var boxHtml = '<div id="confirm-box-js" class="message-box message-box-'+type+' animated fadeIn open" ' +
        'id="message-box-'+type+'"><div class="mb-container">' +
        '<div class="mb-middle"><div class="mb-title"><span class="fa fa-'+icon+'"' +
        '</span> '+title+'</div><div class="mb-content">'+text+'</div><div class="mb-footer">' +
        '<button class="btn btn-default btn-lg mb-control-yes pull-right">Yes</button>' +
        '<button style="margin-right: 10px" class="btn btn-default btn-lg mb-control-close pull-right">No</button>' +
        '</div></div></div></div>';

    $(putTo).html(boxHtml);
    var box = $('#confirm-box-js');

    $('.mb-control-yes').on('click',function(){
        if(yesCallback != undefined)
            yesCallback();
        $( ".mb-control-yes" ).off( "click", "**" );
        box.removeClass("open");
    });

    $('.mb-control-close').on('click',function(){
        $( ".mb-control-close" ).off( "click", "**" );
        if(noCallback != undefined)
            noCallback();
        $(putTo).html("");
    });
    return true;
}

function createNotification(type,text,putTo,icon,styleCssCallback){
    if(!icon)
        icon = "check";

    div = '<div  class="col-md-6 col-md-offset-3">' +
        '<div id="alert" class="alert alert-'+type+'" role="alert">' +
        '<button type="button" class="close" data-dismiss="alert">' +
        '<span aria-hidden="true">x</span><span class="sr-only">Close</span></button>' +
         '<span class="fa fa-'+icon+'"></span> '+text+
        '</div></div>';
    notification = $(putTo).html(div);
    if(styleCssCallback)
        styleCssCallback(notification);

    $("#alert").fadeTo(4000, 500).fadeOut(1000, function(){
        $("#alert").alert('close');
    });

}

jQuery(document).ready(function () {


    //autoclose
    $("#alert").each(function(){
        $(this).fadeTo(4000, 500).fadeOut(1000, function() {
            $(this).alert('close');
        });
    });

});
