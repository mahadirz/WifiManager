<ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- TOGGLE NAVIGATION -->
    <li class="xn-icon-button">
        <a href="#" class="x-navigation-minimizex"><span class="fa fa-dedent"></span></a>
    </li>
    <!-- END TOGGLE NAVIGATION -->
    <!-- SEARCH -->
    <li class="xn-search">
            <input autocomplete="off" id="vertical_nav_search" width="100%" type="text" name="search" placeholder="Search wifi manager..."/>
            <div id="vertical_nav_livesearch"></div>
    </li>
    <!-- END SEARCH -->
    <!-- SIGN OUT -->
    <li class="xn-icon-button pull-right">
        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
    </li>
    <!-- END SIGN OUT -->
    <!-- MESSAGES -->
    <li class="xn-icon-button pull-right">
        <a href="#"><span class="fa fa-comments"></span></a>
        <div id="new_messages_count1" class="informer informer-danger"><!--4--></div>
        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-comments"></span> Unread Tickets</h3>
                <div class="pull-right">
                    <span style="display: none" id="new_messages_count2" class="label label-danger"><!--1 new--></span>
                </div>
            </div>
            <div id="new_messages_contents" class="panel-body list-group list-group-contacts scroll" >

            </div>
            <div class="panel-footer text-center">
                <a href="{{URL::route('admin.support.getTickets','all')}}">Show all tickets</a>
            </div>
        </div>
    </li>
    <!-- END MESSAGES -->

</ul>