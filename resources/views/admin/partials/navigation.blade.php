<ul class="x-navigation">
    <li class="xn-logo">
        <a href="{{URL::route('admin.dashboard.index')}}">Wi-Fi Management System</a>
        <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
        <a href="#" class="profile-mini">
            <img src="{{ URL::asset('assets/images/users/default-avatar.png')}}" alt="User Avatar"/>
        </a>
        <div class="profile">
            <div class="profile-image">
                <img width="100px" height="100px" id="avatar" src="{{ \App\Models\User::find(Auth::id())->getProfilePhotoUrl()}}?{{time()}}" alt="User Avatar"/>
            </div>
            <div class="profile-data">
                <div class="profile-data-name">{{Auth::user()->first_name." ".Auth::user()->last_name}}</div>
                <div class="profile-data-title">Admin's Panel</div>
            </div>
            <div class="profile-controls">
                <a href="{{URL::route('admin.dashboard.getSettings')}}" class="profile-control-left"><span class="fa fa-gear"></span></a>
                <a href="{{URL::route('admin.support.getTickets')}}" class="profile-control-right"><span class="fa fa-envelope"></span></a>
            </div>
        </div>
    </li>

    <li class="xn-title">Navigation</li>

    <li>
        <a href="{{URL::route('admin.dashboard.index')}}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
    </li>

    <li class="xn-openable">
        <a href="#"><span class="fa fa-pagelines"></span> Billing</a>
        <ul>
            <li><a href="{{URL::route('admin.billing.getPayments')}}"><span class="fa fa-credit-card"></span> Payment</a></li>
        </ul>
    </li>

    <li class="xn-openable">
        <a href="#"><span class="fa fa-signal"></span> Access Point</a>
        <ul>
            <li><a href="{{URL::route('admin.accessPoint.getRegister')}}"><span class="fa fa-sitemap"></span> Register NAS</a></li>
            <li><a href="{{URL::route('admin.accessPoint.getList')}}"><span class="fa fa-search-plus"></span> View NAS</a></li>
            <li><a href="{{URL::route('admin.accessPoint.getRadius')}}"><span class="fa fa-tachometer"></span> RADIUS Service</a></li>
        </ul>
    </li>



    <li class="xn-openable">
        <a href="#"><span class="fa fa-envelope"></span> Supports</a>
        <ul>
            <li><a href="{{URL::route('admin.support.getMailBlast')}}"><span class="fa fa-inbox"></span> Email Blast</a></li>
            <li>
                <a id="menu_all_tickets" href="{{URL::route('admin.support.getTickets','all')}}"><span class="fa fa-comments-o"></span> All Tickets</a>
            </li>
            <li>
                <a id="menu_opened_tickets" href="{{URL::route('admin.support.getTickets','opened')}}"><span class="fa fa-comment-o"></span> Opened Tickets</a>
                @if($openTicketCount)
                    <div class="informer informer-warning">{{$openTicketCount}}</div>
                @endif
            </li>
            <li><a href="{{URL::route('admin.support.getTickets','closed')}}"><span class="fa fa-comment"></span> Closed Tickets</a></li>
            <li><a href="{{URL::route('admin.support.getNewTicket')}}"><span class="fa fa-pencil"></span> Create Ticket</a></li>
        </ul>
    </li>

    <li class="xn-openable">
        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Wi-Fi Manager</span></a>
        <ul>
            <li><a href="{{URL::route('admin.package.getNew')}}"><span class="glyphicon glyphicon-folder-close"></span> Add Package</a></li>
            <li><a href="{{URL::route('admin.package.getList')}}"><span class="glyphicon glyphicon-folder-open"></span> View Packages</a></li>
            <li><a href="{{URL::route('admin.manager.getRegister')}}"><span class="fa fa-pencil-square"></span> Register Wi-Fi Manager</a></li>
            <li><a href="{{URL::route('admin.manager.getView')}}"><span class="fa fa-user"></span> View Wi-Fi Manager</a></li>
        </ul>
    </li>

</ul>