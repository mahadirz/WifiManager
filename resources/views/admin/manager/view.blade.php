@extends('admin.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Wi-Fi Manager</li>
    <li class="active">View</li>
@stop

@section('pageContentWrap')

    <div class="row">
        <div class="col-md-12">

            <form method="post" class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/manager.png')}}" /><strong>List of Wi-Fi Manager</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions">
                                <thead>
                                <tr>
                                    <th width="50">ID</th>
                                    <th>Name</th>
                                    <th>Package</th>
                                    <th>Email</th>
                                    <th>Register Date</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($managers as $manager)
                                    <tr id="trow_{{$manager['id']}}">
                                        <td class="text-center">{{$manager['id']}}</td>
                                        <td><strong>{{$manager['first_name'].' '.$manager['last_name']}}</strong></td>
                                        @if (isset($manager['subscription']['subscribable_id']) && $manager['subscription']['subscribable_id'] != 0)
                                            <td><a href="{{URL::route('admin.package.getEdit',
                                            $package->find($manager['subscription']['subscribable_id'])->id)}}">
                                                    {{$package->find($manager['subscription']['subscribable_id'])->name}}</a></td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$manager['email']}}</td>
                                        <td>{{$manager['created_at']}}</td>
                                        <td>
                                            <button type="button" onclick="location.href = '{{URL::route('admin.manager.getEdit',$manager['id'])}}' " class="btn btn-default btn-rounded btn-sm"><span class="fa fa-pencil"></span></button>
                                            <button type="button" onClick="delete_row('trow_{{$manager['id']}}',{{$manager['id']}});" class="btn btn-danger btn-rounded btn-sm" onClick="delete_row('trow_1');"><span class="fa fa-times"></span></button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                    </div>
                </div>
            </form>

        </div>

    </div>


    <div id="confirmBox"></div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function delete_row(row,user_id){

            confirmBox("Remove Wifi Manager","Are you sure you want to remove this user?<br>Click Yes if you are sure","#confirmBox","","warning",function() {

                $.ajax({
                    method: "POST",
                    url: "{{URL::route('admin.manager.postDelete')}}",
                    data: {user_id: user_id}
                })
                        .success(function (msg) {
                            //console.log(msg);
                            if (msg == "OK") {
                                $("#" + row).hide("slow", function () {
                                    $(this).remove();
                                });
                            }
                            else {
                                alert("Error, Failed to remove user!");
                                console.log(msg);
                            }

                        })
                        .fail(function (msg) {
                            alert("[Error] Error, Failed to remove user!");
                            console.log(msg);
                        });

            });

        }
    </script>


@stop