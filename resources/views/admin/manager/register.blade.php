@extends('admin.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/custom.css')}}"/>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Settings</li>
@stop

@section('pageContentWrap')

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            <form id="jvalidate" method="post"  class="form-horizontal">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/manager.png')}}" /><strong> {{isset($title) ? $title:"Register Wi-Fi Manager"}}</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Username</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input id="username" name="username"  type="text" class="form-control"/>
                                </div>
                                <div id="username_loading" style="margin-top: 5px;display: None" >
                                    <span class="glyphicon glyphiconX glyphicon-refresh spinning"></span> Checking...
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>First Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-tag"></span></span>
                                    <input name="first_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Last Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-tag"></span></span>
                                    <input name="last_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span> Email Address</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                    <input id="email" name="email" type="email" class="form-control"/>
                                </div>
                                <div id="email_loading" style="margin-top: 5px;display: None" >
                                    <span class="glyphicon glyphiconX glyphicon-refresh spinning"></span> Checking...
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Password</label>
                            <div id="pwd-container" class="col-md-6 col-xs-12">
                                <div id="passwordGroup" class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                    <input id="password" name="password" type="password" class="form-control"/>
                                </div>
                                {{--<span class="help-block">Guest Password</span>--}}
                                <div class="col-sm-6 col-sm-offset-0" style="padding-top: 5px;;margin-left:-10px;" >
                                    <div class="pwstrength_viewport_progress"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Retype Password</label>
                            <div id="pwd-container" class="col-md-6 col-xs-12">
                                <div id="passwordConfirmationGroup" class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                    <input id="password2" name="password2" type="password" class="form-control"/>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Package</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="package_id" name="package_id" class="form-control select">
                                    @if(isset($packages))
                                        @foreach($packages as $package)
                                            <option value="{{$package['id']}}">{{$package['name']}}</option>
                                        @endforeach
                                    @endif
                                        <option value="0">Please Select</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Phone No.</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                    <input name="land_phone" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Mobile Phone. No.</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-mobile"></span></span>
                                    <input name="mobile_phone" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Country</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="country" name="country" class="form-control select" data-live-search="true">

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">State</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input name="state" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">City</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input name="city" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Zip Code</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input name="zip_code" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Street Address</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input name="street1" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    <div class="panel-footer">
                        <button class="btn btn-warning pull-right">Submit</button>
                    </div>
                </div>
                </div>
            </form>

        </div>

    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>

    <script type="application/javascript" src="{{URL::asset('js/countries.js')}}"></script>

    <script type="text/javascript">
        function dynamicSort(property) {
            var sortOrder = 1;
            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a,b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        countries.country.sort(dynamicSort("countryName"));
        for(var i=0;i < countries.country.length;i++){
            var c = countries.country[i].countryName;
            if(c == "Malaysia")
                $('#country').append('<option selected value="'+c+'">'+c+'</option>');
            else
                $('#country').append('<option value="'+c+'">'+c+'</option>');
        }
    </script>

    <script type="text/javascript" src="{{URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pwstrength-bootstrap-1.2.7.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                showVerdictsInsideProgressBar: true,
                viewports: {
                    progress: ".pwstrength_viewport_progress"
                }
            };
            $('#password').pwstrength(options);

        });
    </script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.validator.addMethod("alpha", function(value, element) {
            return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
            // --                                    or leave a space here ^^
        });

        var jvalidate = $("#jvalidate").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                username: {
                    required: true,
                    minlength: 5,
                    maxlength: 48,
                    remote: {
                        url: "{{URL::route('ajax.postUsername')}}",
                        type: "post",
                        beforeSend: function( xhr ) {
                            $('#username_loading').show();
                        },
                        complete: function(data){
                            $('#username_loading').hide();
                        },
                        data: {
                            username: function() {
                                return $( "#username" ).val();
                            }
                        }
                    }
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 200
                },
                'password2': {
                    required: true,
                    equalTo: "#password"
                },
                first_name: {
                    required: true,
                    alpha: true,
                    minlength: 3,
                    maxlength: 100
                },
                last_name: {
                    required: true,
                    alpha: true,
                    minlength: 3,
                    maxlength: 100
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "{{URL::route('ajax.postEmail')}}",
                        type: "post",
                        beforeSend: function( xhr ) {
                            $('#email_loading').show();
                        },
                        complete: function(data){
                            $('#email_loading').hide();
                            console.log(data);
                        },
                        data: {
                            email: function() {
                                return $("#email").val();
                            }
                        }
                    }
                }
            },

            messages: {
                username: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                first_name: {
                    alpha: jQuery.validator.format("Only alphabetics characters allowed")
                },
                last_name: {
                    alpha: jQuery.validator.format("Only alphabetics characters allowed")
                },
                email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            }
        });

    </script>

    <!-- END SCRIPTS -->


@stop