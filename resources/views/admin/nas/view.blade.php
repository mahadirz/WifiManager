@extends('admin.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Access Point</li>
@stop

@section('pageContentWrap')

    <div class="row">
        <div class="col-md-12">

            <form method="post" class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/network_ip.png')}}" /><strong> Network Access Servers</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions">
                                <thead>
                                <tr>
                                    <th width="50">ID</th>
                                    <th>IP Address</th>
                                    <th>Short Name</th>
                                    <th>Shared Secret</th>
                                    <th>Added Date</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($nas as $n)
                                    <tr id="trow_{{$n['id']}}">
                                        <td class="text-center">{{$n['id']}}</td>
                                        <td><strong>{{$n['nasname']}}</strong></td>
                                        <td>{{$n['shortname']}}</td>
                                        <td>{{$n['secret']}}</td>
                                        <td>{{$n['created_at']}}</td>
                                        <td>
                                            <button type="button" onclick="location.href = '{{URL::route('admin.accessPoint.getUpdate',$n['id'])}}' " class="btn btn-default btn-rounded btn-sm"><span class="fa fa-pencil"></span></button>
                                            <button type="button" onclick="delete_row('trow_{{$n['id']}}',{{$n['id']}});" class="btn btn-danger btn-rounded btn-sm" onClick="delete_row('trow_1');"><span class="fa fa-times"></span></button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="panel-footer">
                        {!!$nas->render()!!}
                    </div>
                </div>
            </form>

        </div>

    </div>


    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>NAS</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to remove this NAS</p>
                    <p>Press Yes if you sure.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function delete_row(row,nas_id){

            var box = $("#mb-remove-row");
            box.addClass("open");

            box.find(".mb-control-yes").on("click",function(){
                box.removeClass("open");


                $.ajax({
                    method: "POST",
                    url: "{{URL::route('admin.accessPoint.postDelete')}}",
                    data: { nas_id: nas_id }
                })
                        .success(function( msg ) {
                            //console.log(msg);
                            if(msg == "OK"){
                                $("#"+row).hide("slow",function(){
                                    $(this).remove();
                                });
                            }
                            else{
                                alert("Error, Failed to remove NAS!");
                            }

                        })
                        .fail(function(msg){
                            alert("Error, Failed to remove NAS!");
                            console.log(msg);
                        });

            });

        }
    </script>



@stop