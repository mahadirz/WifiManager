@extends('admin.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/overlayLoading.css')}}">
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Access Point</li>
@stop

@section('pageContentWrap')

    <div style="display: none" class="loading"></div>

    <div id="notification" style="margin-bottom: 20px;" class="row">
    </div>

    <div class="row">


        <div class="col-md-6">

            <form method="post" class="form-horizontal">
                <div class="panel panel-colorful">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/icon-service.png')}}" /><strong> RADIUS Service</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li><a href="#" onclick="getRadiusStatus();" ><span class="fa fa-refresh"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                            <div align="center" id="radius_status_loading">
                                <img width="48px" height="48px" src="{{URL::asset('img/icons/loading_spinner.gif')}}">
                            </div>

                            <div id="radius_status" style="display:none" class="panel-body list-group list-group-contacts">
                                <a href="#" class="list-group-item" style="border: none">
                                    {{--<div id="radius_status_icon" class="list-group-status status-online"></div>--}}
                                    <img id="radius_status_icon" src="{{URL::asset('img/icons/activateIcon.png')}}" class="pull-left" alt="Dmitry Ivaniuk">
                                    <span id="radius_status_text" class="contacts-title">RADIUS service running</span>
                                    <p><span title="" id="radius_status_time" style="font-size: 11px;color:#CCC;font-weight: 600;">12/12/12 13:08:21</span></p>
                                </a>
                            </div>

                    </div>

                    <div class="panel-footer">
                        <div class="form-group">
                            <div class="col-md-4">
                                <button id="button_start" disabled type="button" class="btn btn-success btn-block">Start</button>
                            </div>
                            <div class="col-md-4">
                                <button id="button_stop" disabled type="button" class="btn btn-danger btn-block">Stop</button>
                            </div>
                            <div class="col-md-4">
                                <button id="button_restart" disabled type="button" class="btn btn-warning btn-block">Restart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <div class="col-md-6">

            <form method="post" class="form-horizontal">
                <div class="panel panel-colorful">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/monitor.png')}}" /><strong> Monitor Service</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <div class="col-md-4">
                                <label class="check"><input {{$conf->get('monitorRadius')}} id="monitorRadius" type="checkbox" class="icheckbox"/> Enable Monitoring Service Health</label>
                            </div>
                            <div class="col-md-4">
                                <label class="check"><input {{$conf->get('monitorAutoStartRadius')}} id="monitorAutoStartRadius" type="checkbox" class="icheckbox" /> Auto Start Service</label>
                            </div>
                            <div class="col-md-4">
                                <label class="check"><input {{$conf->get('monitorDownSendEmail')}} id="monitorDownSendEmail" type="checkbox" class="icheckbox" /> Send Email Notification</label>
                            </div>
                            <p style="padding-bottom:50px;"></p>
                        </div>
                    </div>

                    <div class="panel-footer">

                        <button id="button_update"  type="button" class="pull-left btn btn-primary">Update</button>
                        @if($conf->get('monitorRadius') == "checked")
                            <span class="pull-right" style="font-size: 11px;color:#CCC;font-weight: 600;">Last Check: {{$conf->get('monitorLastCheck')->diffForHumans()}}</span>
                        @endif
                    </div>
                </div>
            </form>

        </div>

    </div>

    <div class="row">


        <div class="col-md-12">

            <form method="post" class="form-horizontal">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong> RADIUS Log</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" onclick="clearLogs()"  ><span class="fa  fa-trash-o"></span> Clear Logs</a></li>
                                    <li><a href="javascript:void(0)" onclick="refreshLogs()" ><span class="fa fa-refresh"></span> Refresh</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <textarea id="logs" style="background-color : #040404;color:white;width: 100%" rows="20" >
                            {{$logs}}
                        </textarea>

                    </div>

                    <div class="panel-footer">

                    </div>
                </div>
            </form>

        </div>

    </div>


    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn"  id="msg_box">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"></div>
                <div class="mb-content">

                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-default btn-lg mb-control-yes">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>


    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.timeago.js')}}"></script>



    <script type="text/javascript" >

        function confirmBox(type,title,text,callback){
            var box = $('#msg_box');
            if(box.length > 0){
                $('.mb-content').html('<p>'+text+'</p><p>Press Yes if you sure.</p>');
                $('.mb-title').html('<span class="fa fa-times"></span>'+title);
                box.removeClass('message-box-success').removeClass('message-box-warning')
                        .removeClass('message-box-danger').removeClass('message-box-info');
                box.toggleClass('message-box-'+type);
                box.toggleClass("open");
                $('.mb-control-yes').on('click',function(){
                    //console.log('a');
                    callback();
                    $( ".mb-control-yes" ).off( "click", "**" );
                    box.removeClass("open");
                });
                return true;
            }
            return false;
        }

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#button_start").on('click',function(){
            radiusActions('start');
        });

        $("#button_stop").on('click',function(){
            confirmBox('warning','Are you sure?','<p>Please confirm your action to stop the ' +
                    'service</p>',function(){radiusActions('stop');});
        });

        $("#button_restart").on('click',function(){
            confirmBox('warning','Are you sure?','<p>Please confirm your action to restart the' +
                    'service</p>',function(){radiusActions('restart');});
        });

        function radiusActions(action)
        {
            $.ajax({
                method: "POST",
                url: "{{URL::route('admin.accessPoint.postRadiusAction')}}",
                beforeSend :function(){
                    $('#radius_status').hide();
                    $('#radius_status_loading').show();
                },
                data:{
                    action:action
                }
            }).always(function(data){
                console.log(data);
                getRadiusStatus();
            }).done(function(data){

            }).fail(function(){

            });
        }

        function getRadiusStatus()
        {
            var icon_online = '{{URL::asset('img/icons/activateIcon.png')}}';
            var icon_offline = '{{URL::asset('img/icons/off.png')}}';
            $.ajax({
                method: "POST",
                url: "{{URL::route('admin.accessPoint.postRadiusStatus')}}",
                beforeSend :function(){
                    $('#radius_status_loading').show();
                    $('#radius_status').hide();
                }
            })
                    .always(function(data){
                        $('#radius_status_loading').hide();
                    })
                    .done(function( msg ) {
                        if(msg.active) {
                            $('#radius_status_icon').attr("src", icon_online);
                            $('#button_stop').prop( "disabled", false );
                            $('#button_restart').prop( "disabled", false );
                            $('#button_start').prop( "disabled", true );
                        }
                        else {
                            $('#radius_status_icon').attr("src", icon_offline);
                            $('#button_start').prop( "disabled", false );
                            $('#button_restart').prop( "disabled", true );
                            $('#button_stop').prop( "disabled", true );
                        }

                        $('#radius_status').show();
                        $('#radius_status_text').text(msg.radius_status_text);
                        dateAgo = jQuery.timeago(Date.parse(msg.radius_status_time));
                        $('#radius_status_time').text(dateAgo)
                                .attr('title',msg.radius_status_time);
                        console.log(msg);
                    })
                    .fail(function(msg){
                        console.log(msg);
                    });


        }

        getRadiusStatus();


        //save service configurations
        $('#button_update').on('click',function(){
            var conf = {monitorRadius:$('#monitorRadius').is(':checked') ,
                monitorAutoStartRadius:$('#monitorAutoStartRadius').is(':checked') ,
                monitorDownSendEmail:$('#monitorDownSendEmail').is(':checked') };
            console.log(conf);

            $.ajax({
                method:"POST",
                url:"{{URL::route('admin.accessPoint.postRadiusMonitor')}}",
                data:conf,
                beforeSend:function(){
                    $('.loading').show();
                }
            }).always(function(){}).done(function(data){
                $('.loading').hide();
                console.log(data);
                createNotification('success','Monitoring options updated','#notification');
            }).fail(function(){});

        });

        function clearLogs()
        {
            $.ajax({
                url:'{{URL::route('admin.accessPoint.postRadiusAction')}}',
                method:'POST',
                data:{action:'clearlogs'},
                beforeSend:function(){$('.loading').show();}
            }).always(function(data){
                $('.loading').hide();
                console.log(data);
                $('#logs').val('');
            });

        }

        function refreshLogs()
        {
            $.ajax({
                url:'{{URL::route('admin.accessPoint.postRadiusAction')}}',
                method:'POST',
                data:{action:'getlogs'},
                beforeSend:function(){$('.loading').show();}
            }).always(function(data){
                $('.loading').hide();
                $('#logs').val(data);
            }).fail(function(data){
                console.log(data);
            });
        }



        var radiusstatus_timeago = setInterval(function(){
            //set for every 60 seconds
            var container = $('#radius_status_time');
            var o_time = container.attr('title');
            var dateAgo = jQuery.timeago(Date.parse(o_time));
            container.text(dateAgo);
        }, 60000);

    </script>



@stop