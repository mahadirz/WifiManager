@extends('admin.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Access Point</li>
@stop

@section('pageContentWrap')

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">

            <form id="jvalidate" method="post" class="form-horizontal">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/network_ip.png')}}" /><strong> Modify Network Access Server</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">IP Address</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-laptop"></span></span>
                                    <input name="nasname" value="{{$nas['nasname']}}"  type="text" class="form-control"/>
                                </div>
                                <span class="help-block">IP Address or subnet of NAS</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Short-Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-tag"></span></span>
                                    <input name="shortname" value="{{$nas['shortname']}}" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">NAS Identifier</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Shared Secret Key</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-unlock"></span></span>
                                    <input name="secret" value="{{$nas['secret']}}" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">NAS Secret</span>
                            </div>
                        </div>



                    </div>
                    <div class="panel-footer">
                        <button onclick="location.href = '{{URL::route('admin.accessPoint.getList')}}'" type="button" class="btn btn-default pull-left">Back</button>
                        <button class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>
            </form>

        </div>

        <div class="col-md-6">

            <form method="post" class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/notes_pin.png')}}" /><strong>Notes</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">


                        <p>
                            <a href="#" data-toggle="tooltip" data-placement="left"  title="Network Access Server">NAS</a> is a system that provides access to a network.
                            In some cases also known as a Terminal Server or Remote Access Server (RAS).
                            The NAS is meant to act as a gateway to guard access to a protected resource.
                            This can be anything from a telephone network, to printers, to the Internet.
                            The client connects to the NAS. The NAS then connects to another resource asking whether
                            the client's supplied credentials are valid. Based on that answer the NAS then allows or
                            disallows access to the protected resource. <br />
                            In this context the NAS is the Access Point or Router<br /><br />
                            <strong>IP Address :</strong> The IP address of the Access Point, must be unique<br />
                            <strong>Short-Name :</strong> The short identifier of the server<br />
                            <strong>Shared Secret Key :</strong> The shared secret phrase for the NAS to authenticate to RADIUS and encrypt communication.
                        </p>



                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </form>

        </div>
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>


    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var check = false;
                    return this.optional(element) || regexp.test(value);
                },
                "Please check your input."
        );

        $("#jvalidate").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                nasname: {
                    required: true,
                    regex : /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}(\/\d{0,2})?$/
                },
                'shortname': {
                    required: true,
                    minlength: 5,
                    maxlength: 48
                },
                'secret': {
                    required: true,
                    minlength: 5,
                    maxlength: 48
                }
            }
        });
    </script>

    <script type="text/javascript" src="{{URL::asset('js/custom.js')}}"></script>



@stop