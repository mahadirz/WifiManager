@extends('user.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Guest</li>
@stop

@section('pageContentWrap')
    <div class="row">
        <div class="col-md-12">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Data Usage Chart</h3>
                </div>
                <div class="panel-body">
                    <div id="morris-line-usage-statistics" style="height: 300px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Hours Usage Chart</h3>
                </div>
                <div class="panel-body">
                    <div id="morris-line-hour-usage-statistics" style="height: 300px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
    </div>

@stop

@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/morris.min.js')}}"></script>


    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>



@stop

@section('customJs')
    <script type="text/javascript">
        var morrisCharts = function() {

            Morris.Line({
                element: 'morris-line-usage-statistics',
                data: [

                    @for ($i = 0; $i < 30; $i++)
                        {x: '{{\Carbon\Carbon::now()->subDay(30)->addDay($i)->format("Y-m-d")}}', a: {{\rand(50,1000)}}, b: {{\rand(50,1000)}},c:{{\rand(500,10000)}}},
                    @endfor
                    {x: '{{\Carbon\Carbon::now()->format("Y-m-d")}}', a: {{\rand(50,1000)}}, b: {{\rand(50,1000)}},c:{{\rand(500,10000)}}},
                ],
                xkey: 'x',
                ykeys: ['a', 'b','c'],
                labels: ['Downloads (MB)', 'Uploads (MB)','Total (MB)'],
                resize: true
            });

            Morris.Line({
                element: 'morris-line-hour-usage-statistics',
                data: [

                        @for ($i = 0; $i < 30; $i++)
                            {x: '{{\Carbon\Carbon::now()->subDay(30)->addDay($i)->format("Y-m-d")}}', a: {{\rand(1,24)}}, b: {{\rand(1,24)}},c:{{\rand(1,24)}}},
                        @endfor
                        {x: '{{\Carbon\Carbon::now()->format("Y-m-d")}}', a: {{\rand(1,24)}}, b: {{\rand(1,24)}},c:{{\rand(1,24)}}},
                ],
                xkey: 'x',
                ykeys: 'a',
                labels: 'Hours',
                resize: true
            });

        }();
    </script>
@stop

