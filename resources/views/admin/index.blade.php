@extends('admin.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Dashboard</li>
@stop

@section('pageContentWrap')

        <!-- START WIDGETS -->
            <div class="row">


        @foreach($widgets as $widget)

            <div class="col-md-3">

                <!-- START WIDGET REGISTRED -->
                <div class="widget widget-default widget-item-icon">
                    <div class="widget-item-left">
                        <span class="fa {{$widget['icon']}}"></span>
                    </div>
                    <div class="widget-data">
                        <div class="widget-int num-count">{{$widget['count']}}</div>
                        <div class="widget-title">{{$widget['title']}}</div>
                        <div class="widget-subtitle">{{$widget['subtitle']}}</div>
                    </div>
                    <div class="widget-controls">
                    </div>
                </div>
                <!-- END WIDGET REGISTRED -->

            </div>

        @endforeach



        <div class="col-md-3">

            <!-- START WIDGET CLOCK -->
            <div class="widget widget-danger widget-padding-sm">
                <div class="widget-big-int plugin-clock">00:00</div>
                <div class="widget-subtitle plugin-date">Loading...</div>
                <div class="widget-controls">
                </div>

            </div>
            <!-- END WIDGET CLOCK -->

        </div>
    </div>
        <!-- END WIDGETS -->

        <div class="row">
            <div class="col-md-12">

                <!-- START PROJECTS BLOCK -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>Account</h3>
                            <span>Information</span>
                        </div>
                        <ul class="panel-controls" style="margin-top: 2px;">

                        </ul>
                    </div>
                    <div class="panel-body panel-body-table">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="60%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><strong>Registration Date</strong></td>
                                    <td>{{$accountInfo['created_at']}}</td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Last Account Updated</strong></td>
                                    <td>{{$accountInfo['updated_at']}}</td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>RADIUS Service Status</strong></td>
                                    <td><span class="label {{$accountInfo['radius_status']['label']}}">{{$accountInfo['radius_status']['text']}}</span></td>
                                    <td>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- END PROJECTS BLOCK -->

            </div>
        </div>


@stop

@section('thisPageJs')
    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/morris.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/rickshaw/d3.v3.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/rickshaw/rickshaw.min.js')}}"></script>
    <script type='text/javascript' src='{{ URL::asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}'></script>
    <script type='text/javascript' src='{{ URL::asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}'></script>
    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/daterangepicker/daterangepicker.js')}}"></script>

@stop
