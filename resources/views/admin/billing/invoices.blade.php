@extends('admin.master')

@section('customHeader')
    <style>
        .modal-header, h4, .close {
            background-color: #3FBAE4;
            color:white !important;
            text-align: center;
            font-size: 30px;
        }
        .modal-footer {
            background-color: #3FBAE4;
        }
    </style>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Billing</li>
    <li class="active">Payments</li>
@stop

@section('pageContentWrap')

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/invoice.png')}}" /><strong>Invoices</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                            <tr>
                                <th width="10">ID</th>
                                <th>Wifi Manager Name</th>
                                <th>Package Name</th>
                                <th>Amount Paid</th>
                                <th>Notes</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($invoices as $invoice)
                                <tr>
                                    <td>{{$invoice->id}}</td>
                                    <td><a href="{{URL::route('admin.manager.getEdit',$invoice->user->id)}}">{{$invoice->user->getFullName()}}</a></td>
                                    <td><a href="{{URL::route('admin.package.getEdit',$invoice->packageable->id)}}">{{$invoice->packageable->name}}</a></td>
                                    <td>
                                        @if($invoice->paid_amount > 0)
                                            {{$billing->getCurrency().$invoice->paid_amount}}
                                        @else
                                            ---
                                        @endif
                                    </td>
                                    <td>{{$invoice->notes}}</td>
                                    <td>{{$invoice->created_at}}</td>
                                    <td>
                                        <span style="background-color:
                                        @if($invoice->status == "unpaid")
                                                red;
                                        @elseif($invoice->status == "paid")
                                                green;
                                        @else
                                                #BCC1C8;
                                        @endif
                                                color:white;padding: 4px;">
                                            {{$invoice->status}}
                                        </span>

                                    </td>
                                    <td>
                                        {!! Form::open(['method'=>'post','id'=>'form_invoice','class'=>'form_invoice_'.$invoice->id]) !!}
                                        <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                        @if($invoice->status == "unpaid" || $invoice->status == "incomplete")
                                            <button type="button" data-toggle="modal" onclick="javascript:showPaymentModal({{$invoice->id}})" class="btn btn-info"><i class="fa fa-file-text-o"></i> Add Payment</button>
                                            <button name="cancel" value="1" class="btn btn-danger"><i class="fa fa-times-circle"></i> Cancel</button>
                                        @endif
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        @if(count($invoices)>0)
                            {!!$invoices->render()!!}
                        @endif
                    </div>
                </div>

                <div class="panel-footer">
                </div>
            </div>

        </div>

    </div>

    <!-- MESSAGE BOX-->
    <div id="confirmBox">
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addPaymentModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header" style="padding:35px 50px;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4><span class="fa fa-credit-card"></span> Payment Details</h4>
                </div>
                <div class="modal-body" style="padding:40px 50px;">
                    {!! Form::open(['method'=>'post','role'=>'form']) !!}
                    {!! Form::hidden('invoice_id',null,['id'=>'invoice_id']) !!}
                        <div class="form-group">
                            <label for="usrname"><span class="fa fa-usd"></span> Amount</label>
                            <input required pattern="^[\d\.]+$" type="text" class="form-control" name="amount" placeholder="0.00">
                        </div>
                        <div class="form-group">
                            <label for="psw"><span class="fa fa-clipboard"></span> Notes</label>
                            <textarea class="form-control" name="notes" cols="5" rows="5"></textarea>
                        </div>
                        <button name="addPayment" value="save" type="submit" class="btn btn-info btn-block"><span class=""fa fa-save"></span> Save</button>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                </div>
            </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function showPaymentModal(invoice_id){
            $("#invoice_id").val(invoice_id);
            $("#addPaymentModal").modal();
        }

        $(document).ready(function () {

            var formClass;

            $('#form_invoice').submit(function (event) {
                formClass = $(this).attr('class');
                console.log(formClass);
                confirmBox("Confirmation","Are you sure to cancel the invoice?","#confirmBox",null,null,function(){
                    //console.log(formInstance.find('input[name=invoice_id]').val());
                    console.log(formClass);
                    $('.'+formClass).unbind('submit').submit();
                });
                event.preventDefault();
            });

        });

    </script>


@stop