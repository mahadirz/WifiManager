@extends('admin.master')

@section('customHeader')
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Wi-Fi Manager</li>
    <li class="active">View</li>
@stop

@section('pageContentWrap')

    <div class="row">
        <div class="col-md-12">

            <form method="post" class="form-horizontal">
                <input type="hidden" name="package_id" id="package_id">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img style="padding-right: 5px" width="32px" height="32px" src="{{URL::asset('img/icons/Box-icon.png')}}" /><strong>List of Packages</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions">
                                <thead>
                                <tr>
                                    <th width="100">Name</th>
                                    <th width="100">Descriptions</th>
                                    <th width="100">Created Date</th>
                                    <th width="100">actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($packages as $package)
                                <tr id="trow_{{$package['id']}}">
                                    <td class="text-center"><a href="{{URL::route('admin.package.getEdit',$package['id'])}}">{{$package['name']}}</a></td>
                                    <td><strong>{{$package['descriptions']}}</strong></td>
                                    <td>{{$package['created_at']}}</td>
                                    <td>
                                        <button type="button" class="btn btn-default btn-rounded btn-sm" onclick="location.href = '{{URL::route('admin.package.getEdit',$package['id'])}}' "><span class="fa fa-pencil"></span></button>
                                        <button type="button" class="btn btn-danger btn-rounded btn-sm" onClick="delete_row('trow_{{$package['id']}}',{{$package['id']}});"><span class="fa fa-times"></span></button>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {!! $packages->render() !!}
                        </div>
                    </div>

                    <div class="panel-footer">
                    </div>
                </div>
            </form>

        </div>

        <div class="col-md-6">
            <button type="button" onclick="location.href='{{URL::route('admin.package.getNew')}}'" class="btn btn-primary">Create New Package</button>
        </div>

    </div>

    <div id="confirmBox"></div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


    <script type="text/javascript" >


        function delete_row(row,package_id){

            confirmBox("Remove Package","Are you sure you want to remove this package?<br>Click Yes if you are sure","#confirmBox","","warning",function(){

                //ajax
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: "POST",
                    url: "{{URL::route('admin.package.postDelete')}}",
                    data: { package_id: package_id }
                })
                        .success(function( msg ) {
                            //console.log(msg);
                            $("#"+row).hide("slow",function(){
                                $(this).remove();
                            });

                        })
                        .fail(function(msg){
                            alert("Error, Failed to remove package!");
                            console.log(msg.responseText);
                        });
            });

        }
    </script>

@stop