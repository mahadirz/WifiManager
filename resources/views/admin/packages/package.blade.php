@extends('admin.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Wifi-Manager</li>
@stop

@section('pageContentWrap')

    @if(isset($data))
    <div class="row">
        <div class="col-md-12" style="padding: 15px;">
            <div class="alert alert-{{$data['alert']}}" id="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <strong>{{$data['title']}} </strong>
                {{$data['msg']}}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            <form method="post" class="form-horizontal">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <img style="padding-right: 5px" width="32px" height="32px" src="{{URL::asset('img/icons/Box-icon.png')}}" />
                            <strong>{{$title}}</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Package Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input required placeholder="Basic" name="name" @if(isset($input))value="{{$input['name']}}" @endif  type="text" class="form-control"/>
                                </div>
                                <span class="help-block">The Package name to identify the package</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Package Descriptions</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input name="descriptions" type="text" @if(isset($input))value="{{$input['descriptions']}}" @endif class="form-control"/>
                                </div>
                                <span class="help-block">Optional field to describe the package</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Total Number of Users</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input required name="max_users" @if(isset($input))value="{{$input['max_users']}}" @else value="5" @endif type="text" class="form-control"/>
                                </div>
                                <span class="help-block">Maximum number of users the manager can register</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Maximum Connections per User</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input required @if(isset($input))value="{{$input['max_connections_per_user']}}" @else value="10" @endif name="max_connections_per_user" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">The number of concurrent connections can be made per user including Guest</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Acct-Interim-Interval</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input @if(isset($input))value="{{$input['acctinterim']}}" @else value="10" @endif required  name="acctinterim" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">The Interval in minutes NAS should send accounting data (should not less than 10) </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Enable User Support</label>
                            <div class="col-md-6 col-xs-12">
                                <label class="switch">
                                    <input name="enable_user_support" type="checkbox" @if(isset($input)){{$input['enable_user_support']}} @endif >
                                    <span></span>
                                </label>
                                <span class="help-block">Enable support page in user panel </span>
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<label class="col-md-3 col-xs-12 control-label">Enable User Billing</label>--}}
                            {{--<div class="col-md-6 col-xs-12">--}}
                                {{--<label class="switch">--}}
                                    {{--<input name="enable_user_billing" type="checkbox" @if(isset($input) ){{$input['enable_user_billing']}} @endif >--}}
                                    {{--<span></span>--}}
                                {{--</label>--}}
                                {{--<span class="help-block">Enable billing page in user panel </span>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Monthly Price</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input @if(isset($input))value="{{$input['monthly_price']}}" @else value="0.00" @endif  required name="monthly_price" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">0.00 to disable the billing cycle</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Semi Annually Price</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input @if(isset($input))value="{{$input['semi_annually_price']}}" @else value="0.00" @endif required  name="semi_annually_price" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">0.00 to disable the billing cycle</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Annually Price</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    <input required @if(isset($input))value="{{$input['annually_price']}}" @else value="0.00" @endif  name="annually_price" type="text" class="form-control"/>
                                </div>
                                <span class="help-block">0.00 to disable the billing cycle</span>
                            </div>
                        </div>

                    <div class="panel-footer">
                        <button type="button" onclick="location.href = '{{URL::route('admin.package.getList')}}'" class="btn btn-default pull-left">Back</button>
                        @if($title == "Add New Package")
                            <button class="btn btn-info pull-right">Create</button>
                        @else
                            <button class="btn btn-info pull-right">Update</button>
                        @endif
                    </div>
                </div>
            </form>

        </div>

    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>

    <script type="application/javascript" src="{{URL::asset('js/countries.js')}}"></script>

    <script type="text/javascript">
        function dynamicSort(property) {
            var sortOrder = 1;
            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a,b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        countries.country.sort(dynamicSort("countryName"));
        for(var i=0;i < countries.country.length;i++){
            var c = countries.country[i].countryName;
            if(c == "Malaysia")
                $('#country').append('<option selected value="'+c+'">'+c+'</option>');
            else
                $('#country').append('<option value="'+c+'">'+c+'</option>');
        }
    </script>

    <script type="application/javascript" src="{{URL::asset('js/custom.js')}}"></script>


@stop