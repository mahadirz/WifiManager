@extends('admin.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/overlayLoading.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/fileinput.css')}}"/>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Page</li>
@stop

@section('pageContentWrap')
    <div style="display: none" class="loading"></div>

    <div id="confirmBox"></div>

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> Panel Title</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                </div>
            </div>

        </div>

    </div>

@stop

@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>


    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>


    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/dropzone/dropzone.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/fileinput/canvas-to-blob.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/fileinput/fileinput.min.js')}}"></script>


@stop

@section('customJs')

@stop
