@extends('manager.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>User Account</li>
    <li class="active">View</li>
@stop

@section('pageContentWrap')

    <div class="row">
        <div class="col-md-12">
            <!-- Search -->
            {!! Form::open(['method'=>'post']) !!}
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row stacked">
                        <div class="col-md-6">
                            <div class="input-group push-down-10">
                                <span class="input-group-addon"><span class="fa fa-search"></span></span>
                                <input name="search" type="text" class="form-control" placeholder="Search by username,name, or email .....">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <!-- end Search -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <form method="post" class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/manager.png')}}" /><strong>List of End Users</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-actions">
                                <thead>
                                <tr>
                                    <th width="50">ID</th>
                                    <th>Name</th>
                                    <th>Package</th>
                                    <th>Email</th>
                                    <th>Register Date</th>
                                    <th>actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users->get('users') as $user)
                                    <tr id="trow_{{$user->id}}">
                                        <td class="text-center">{{$user->id}}</td>
                                        <td><strong>{{$user->getFullName()}}</strong></td>
                                        @if(!is_null($user->subscription) && !is_null($user->subscription->subscribable) && !is_null($user->subscription->subscribable->name))
                                            <td>
                                                <a href="{{URL::route('manager.package.getEdit',$user->subscription->subscribable->id)}}">
                                                    {{$user->subscription->subscribable->name}}
                                                </a>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->created_at}}</td>
                                        <td>
                                            <button type="button" onclick="location.href = '{{URL::route('manager.user.getEdit',$user->id)}}' " class="btn btn-default btn-rounded btn-sm"><span class="fa fa-pencil"></span></button>
                                            <button type="button" onClick="delete_row('trow_{{$user->id}}',{{$user->id}});" class="btn btn-danger btn-rounded btn-sm" onClick="delete_row('trow_1');"><span class="fa fa-times"></span></button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        {!! $users->get('paginator')->render() !!}
                    </div>

                    <div class="panel-footer">
                    </div>
                </div>
            </form>

        </div>

    </div>


    <!-- MESSAGE BOX-->
    <div class="message-box animated fadeIn" data-sound="alert" id="mb-remove-row">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-times"></span> Remove <strong>Wifi Manager</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to remove this user?</p>
                    <p>Press Yes if you sure.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <button class="btn btn-success btn-lg mb-control-yes">Yes</button>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function delete_row(row,user_id){

            var box = $("#mb-remove-row");
            box.addClass("open");

            box.find(".mb-control-yes").on("click",function(){
                box.removeClass("open");


                $.ajax({
                    method: "POST",
                    url: "{{URL::route('manager.user.postDelete')}}",
                    data: { user_id: user_id }
                })
                        .success(function( msg ) {
                            //console.log(msg);
                            if(msg == "OK"){
                                $("#"+row).hide("slow",function(){
                                    $(this).remove();
                                });
                            }
                            else{
                                alert("Error, Failed to remove user!");
                            }

                        })
                        .fail(function(msg){
                            alert("Error, Failed to remove user!");
                            console.log(msg);
                        });

            });

        }
    </script>


@stop