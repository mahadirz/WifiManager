@extends('manager.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/custom.css')}}"/>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>User Account</li>
    <li class="active">Update Profile</li>
@stop

@section('pageContentWrap')

    @if(Session::has('errors'))
        @foreach (Session::get('errors')->all() as $message)
            <div class="row">
                <div class="col-md-12" style="padding: 15px;">
                    <div class="alert alert-danger" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        {{$message}}
                    </div>
                </div>
            </div>
        @endforeach
    @endif

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">

                {!! Form::open(['id'=>'jvalidate','method'=>'post','class'=>'form-horizontal']) !!}
                <input type="hidden" name="update_profile" value="1">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <input id="_country"  type="hidden" value="{{!is_null($user['profile'])? $user['profile']['country']:"Malaysia"}}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img src="{{URL::asset('img/icons/manager.png')}}" /><strong> Update Profile</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"> Username</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa"></span></span>
                                    <input value="{{$user['username']}}" readonly type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>First Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input value="{{$user['first_name']}}" name="first_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Last Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input value="{{$user['last_name']}}" name="last_name" type="text" class="form-control"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span> Email Address</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                    <input value="{{$user['email']}}" id="email" name="email" type="email" class="form-control"/>
                                </div>
                                <div id="email_loading" style="margin-top: 5px;display: None" >
                                    <span class="glyphicon glyphiconX glyphicon-refresh spinning"></span> Checking...
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Package</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="package_id" name="package_id" class="form-control select">
                                    @if(isset($packages))
                                        @foreach($packages as $package)
                                            <option value="{{$package['id']}}">{{$package['name']}}</option>
                                        @endforeach
                                    @endif
                                        <option value="0">Please Select</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Phone No.</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-phone"></span></span>
                                    <input value="{{!is_null($user['profile'])? $user['profile']['land_phone']:""}}" name="land_phone" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Mobile Phone. No.</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-mobile"></span></span>
                                    <input value="{{!is_null($user['profile'])? $user['profile']['mobile_phone']:""}}" name="mobile_phone" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Country</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="country" name="country" class="form-control select" data-live-search="true">

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">State</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input value="{{!is_null($user['profile'])? $user['profile']['state']:""}}" name="state" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">City</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input value="{{!is_null($user['profile'])? $user['profile']['city']:""}}" name="city" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Zip Code</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input value="{{!is_null($user['profile'])? $user['profile']['zip_code']:""}}" name="zip_code" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Street Address</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa "></span></span>
                                    <input value="{{!is_null($user['profile'])? $user['profile']['street1']:""}}" name="street1" type="text" class="form-control"/>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                    <div class="panel-footer">
                        <button class="btn btn-warning pull-right">Update Profile</button>
                    </div>
                </div>
                </div>
            {!! Form::close() !!}

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <form id="jvalidate2" method="post"  class="form-horizontal">
                <input name="_token" type="hidden" value="{{csrf_token()}}">
                <input type="hidden" name="update_password" value="1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/security-icon.gif')}}" /><strong> Change Password</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>New Password</label>
                            <div id="pwd-container" class="col-md-6 col-xs-12">
                                <div id="passwordGroup" class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                    <input id="password" name="password" type="password" class="form-control"/>
                                </div>
                                {{--<span class="help-block">Guest Password</span>--}}
                                <div class="col-sm-6 col-sm-offset-0" style="padding-top: 5px;;margin-left:-10px;" >
                                    <div class="pwstrength_viewport_progress"></div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Retype Password</label>
                            <div id="pwd-container" class="col-md-6 col-xs-12">
                                <div id="passwordConfirmationGroup" class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                    <input id="password2" name="password2" type="password" class="form-control"/>
                                </div>
                            </div>
                        </div>

                    <div class="panel-footer">
                        <button class="btn btn-warning pull-right">Update Password</button>
                    </div>
                </div>
                </div>
            </form>

        </div>

    </div>

    @if(count($accountInfo)>0)
        <div class="row">
        <div class="col-md-12">

            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Account</h3>
                        <span>Information</span>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">

                    </ul>
                </div>
                <div class="panel-body panel-body-table">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="20%"></th>
                                <th width="60%"></th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td><strong>Total Data Transfer</strong></td>
                                <td>{{$accountInfo['data']['transfer']}}</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><strong>Total Data Uploaded</strong></td>
                                <td>{{$accountInfo['data']['upload']}}</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><strong>Total Data Downloaded</strong></td>
                                <td>{{$accountInfo['data']['download']}}</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><strong>Account Status</strong></td>
                                <td><span class="label {{$accountInfo['status']['label']}}">{{$accountInfo['status']['text']}}</span></td>
                                <td>
                                    <form method="post">
                                        <input name="_token" type="hidden" value="{{csrf_token()}}">
                                        @if($accountInfo['status']['text'] == "active")
                                            <input type="hidden" name="suspend_account" value="1">
                                            <button type="submit" class="btn btn-danger pull-left btn-sm">Suspend Account</button>
                                        @else
                                            <input type="hidden" name="activate_account" value="1">
                                            <button type="submit" class="btn btn-primary pull-left btn-sm">Activate Account</button>
                                        @endif
                                    </form>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Wi-Fi Connection Status</strong></td>
                                <td><span class="label {{$accountInfo['connection_status']['label']}}">{{$accountInfo['connection_status']['text']}}</span></td>
                                <td>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Daily Connection Hour</strong></td>
                                <td>{{$accountInfo['daily_connection_hour']}} @if($accountInfo['daily_data_limit_enabled'] ==0)<span class="label label-danger">Disabled</span>@endif</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><strong>Daily Data Quota</strong></td>
                                <td>{{$accountInfo['daily_data_usage']['used']}}/{{$accountInfo['daily_data_usage']['limit']}}</td>
                                <td>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar {{$accountInfo['daily_data_usage']['progressBar_label']}}" role="progressbar"
                                             aria-valuenow="{{$accountInfo['daily_data_usage']['progressBar_percentage']}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$accountInfo['daily_data_usage']['progressBar_percentage']}}%;">{{$accountInfo['daily_data_usage']['progressBar_percentage']}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Daily Hours Quota</strong></td>
                                <td>{{$accountInfo['daily_hours_usage']['used']}}/{{$accountInfo['daily_hours_usage']['limit']}} hours</td>
                                <td>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar {{$accountInfo['daily_hours_usage']['progressBar_label']}}" role="progressbar"
                                             aria-valuenow="{{$accountInfo['daily_hours_usage']['progressBar_percentage']}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$accountInfo['daily_hours_usage']['progressBar_percentage']}}%;">{{$accountInfo['daily_hours_usage']['progressBar_percentage']}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Connection Limit</strong></td>
                                <td>{{$accountInfo['connection_limit']['used']}}/{{$accountInfo['connection_limit']['limit']}}</td>
                                <td>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar {{$accountInfo['connection_limit']['progressBar_label']}}" role="progressbar"
                                             aria-valuenow="{{$accountInfo['connection_limit']['progressBar_percentage']}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$accountInfo['connection_limit']['progressBar_percentage']}}%;">{{$accountInfo['connection_limit']['progressBar_percentage']}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- END PROJECTS BLOCK -->

        </div>
    </div>
    @endif


    <br><br><br>



@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>

    <script type="application/javascript" src="{{URL::asset('js/countries.js')}}"></script>

    <script type="text/javascript">
        function dynamicSort(property) {
            var sortOrder = 1;
            if(property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a,b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        countries.country.sort(dynamicSort("countryName"));
        for(var i=0;i < countries.country.length;i++){
            var c = countries.country[i].countryName;
            if(c == $('#_country').val())
                $('#country').append('<option selected value="'+c+'">'+c+'</option>');
            else
                $('#country').append('<option value="'+c+'">'+c+'</option>');
        }


        //set selected package
        if(myApp.package == undefined){
            console.log("ID:");
            console.log(myApp);
            $('#package_id').val("0");
        }
        else{
            console.log("ID second:");
            console.log(myApp);
            $('#package_id').val(myApp.package.subscribable_id);
        }

    </script>

    <script type="text/javascript" src="{{URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pwstrength-bootstrap-1.2.7.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                showVerdictsInsideProgressBar: true,
                viewports: {
                    progress: ".pwstrength_viewport_progress"
                }
            };
            $('#password').pwstrength(options);

        });
    </script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#jvalidate2").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 200
                },
                'password2': {
                    required: true,
                    equalTo: "#password"
                }
            }
        });

        var jvalidate = $("#jvalidate").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                username: {
                    required: true,
                    minlength: 5,
                    maxlength: 48,
                    remote: {
                        url: "{{URL::route('ajax.postUsername')}}",
                        type: "post",
                        beforeSend: function( xhr ) {
                            $('#username_loading').show();
                        },
                        complete: function(data){
                            $('#username_loading').hide();
                        },
                        data: {
                            username: function() {
                                return $( "#username" ).val();
                            }
                        }
                    }
                },

                first_name: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                last_name: {
                    required: true,
                    minlength: 3,
                    maxlength: 100
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "{{URL::route('ajax.postEmail')}}",
                        type: "post",
                        beforeSend: function( xhr ) {
                            $('#email_loading').show();
                        },
                        complete: function(data){
                            $('#email_loading').hide();
                            console.log(data);
                        },
                        data: {
                            email: function() {
                                return $("#email").val();
                            },
                            user_id:myApp.user_id
                        }
                    }
                }
            },

            messages: {
                username: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            }
        });

    </script>

    <!-- END SCRIPTS -->


@stop