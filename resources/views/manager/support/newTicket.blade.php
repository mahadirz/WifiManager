@extends('manager.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/overlayLoading.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/fileinput.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/custom.css')}}"/>
    <style>
        .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
        .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden;cursor:pointer; }
        .autocomplete-selected { background: #F0F0F0; }
        .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
        .autocomplete-group { padding: 2px 5px; }
        .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }

    </style>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li><a href="#">Support</a></li>
    <li class="active">Create New Ticket</li>

    <!-- START CONTENT FRAME -->
    <div class="content-frame">
        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">
            <div class="page-title">
                <h2><span class="fa fa-pencil"></span> Open a New Ticket</h2>
            </div>

        </div>
        <!-- END CONTENT FRAME TOP -->


        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-center">
            <div class="block">
                    {!! Form::open(['method'=>'POST','class'=>'form-horizontal','role'=>'form']) !!}
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="button" onclick="window.location.href = '{{URL::route('manager.support.getTickets','all')}}'" class="btn btn-default"><span class="fa fa-arrow-left"></span> Back</button>
                            </div>
                            <div class="pull-left">
                                <button class="btn btn-danger"><span class="fa fa-envelope"></span> Open Ticket</button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">To:</label>
                        <div class="col-md-9">
                            <input type="text"  class="form-control" id="receiver" placeholder="Type username,email, or name"/>
                            {!! Form::hidden('user_id_receiver') !!}
                            <div style="display: none" id="tags_3_tagsinput_tagsinput" class="tagsinput"
                                 style="width: 100%; min-height: auto; height: auto;"><span class="tag"><span id="tagValue"></span><a
                                            href="#" onclick="removeTag()" title="Removing tag">x</a></span>

                                <div id="tags_3_tagsinput_addTag">
                                    <input readonly id="tags_3_tagsinput_tag" value=""  data-default="add a tag"
                                           style="color: rgb(102, 102, 102); width: 80px;">
                                </div>
                                <div class="tags_clear"></div>
                            </div>
                            <div id="receiver_loading" style="margin-top: 5px;display: None" >
                                <span class="glyphicon glyphiconX glyphicon-refresh spinning"></span> searching...
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">Subject:</label>
                        <div class="col-md-10">
                            {!! Form::text('title',null,['placeholder'=>"what's this ticket is about",'class'=>'form-control','required'=>'required']) !!}
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-12">
                                    <textarea name="body" class="summernote_email">
                                        <p><br><br></p>
                                        {{is_null($user->profile)?"":$user->profile->signature}}
                                    </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="button" onclick="window.location.href = '{{URL::route('manager.support.getTickets','all')}}'" class="btn btn-default"><span class="fa fa-arrow-left"></span> Back</button>
                            </div>
                            <div class="pull-left">
                                <button class="btn btn-danger"><span class="fa fa-envelope"></span> Open Ticket</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>
        <!-- END CONTENT FRAME BODY -->
    </div>
    <!-- END CONTENT FRAME -->
@stop


@section('thisPageJs')


    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/summernote/summernote.js')}}"></script>

{{--    <script type="text/javascript" src="{{ URL::asset('js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>--}}
{{--    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-tagsinput.min.js')}}"></script>--}}

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.autocomplete.min.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->

@stop

@section('customJs')
    <script type="text/javascript">

        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var admin_id = '{{App\Models\Role::where('name','administrator')->first()->users->first()->id}}';
            var admin_fullname = '{{App\Models\Role::where('name','administrator')->first()->users->first()->getFullName()}}';
            addTag(admin_id,admin_fullname);

            $('#receiver').autocomplete({
                lookup: function (keyword, done) {
                    // Do ajax call or lookup locally, when done,
                    // call the callback and pass your results:
                    var date = new Date().getTime();
                    console.log('send ajax:'+date);
                    $.ajax({
                        url: '{{URL::route('ajax.postReceiverAutoComplete')}}',
                        'method': 'POST',
                        'beforeSend': function () {
                            $('#receiver_loading').show();
                        },
                        'data': {keyword:keyword}
                    }).done(function (data) {
                        date2 = new Date().getTime();
                        console.log('received ajax:'+date);
                        console.log(date2-date+" ms");
                        console.log(data);
                        done(data);
                    })
                            .fail(function (data) {
                                console.log('Ajax failed');
                                console.log(data);
                            })
                            .always(function () {
                                $('#receiver_loading').hide();
                            });

                },
                onSelect: function (suggestion) {
//                    console.log(suggestion.value);
//                    $('input[name=user_id_receiver]').val(suggestion.data);
//                    $('#receiver').hide();
//                    $('#tags_3_tagsinput_tagsinput').show();
//                    $('#tagValue').html(suggestion.value+'&nbsp;&nbsp;');
                    addTag(suggestion.data,suggestion.value);
                }
            });


        });


        function addTag(user_id,$name)
        {
            $('input[name=user_id_receiver]').val(user_id);
            $('#receiver').hide();
            $('#tags_3_tagsinput_tagsinput').show();
            $('#tagValue').html($name+'&nbsp;&nbsp;')
        }

        function removeTag()
        {
            console.log('removeTag');
            $('input[name=user_id_receiver]').val('');
            $('#tags_3_tagsinput_tagsinput').hide();
            $('#receiver').val('').show();
        }



    </script>
@stop
