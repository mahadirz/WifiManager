@extends('manager.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/overlayLoading.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/fileinput.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/custom.css')}}"/>
    <style>
        .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
        .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden;cursor:pointer; }
        .autocomplete-selected { background: #F0F0F0; }
        .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
        .autocomplete-group { padding: 2px 5px; }
        .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }

    </style>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li><a href="#">Support</a></li>
    <li class="active">Mail Blast</li>

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
        @endif

    <!-- START CONTENT FRAME -->
    <div class="content-frame">
        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">
            <div class="page-title">
                <h2><span class="fa fa-bullhorn"></span> Mail to all Users</h2>
            </div>

        </div>
        <!-- END CONTENT FRAME TOP -->

        <!-- START CONTENT FRAME LEFT -->
        <div class="content-frame-left">
            <div class="block">
                <div class="panel panel-success">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title">Variables</h3>
                    </div>
                    <div class="panel-body">
                        <p>You may use these <code>variables</code> in the email content</p>
                        <p><span style="background-color: #fff49a">Warning! Please use remove font style if you copy the variables</span></p>
                        <p><code>{username}</code> &nbsp;&nbsp;&nbsp;- Manager's username</p>
                        <p><code>{fullname}</code> &nbsp;&nbsp;&nbsp;- Manager's fullname</p>
                        <p><code>{firstname}</code> &nbsp;&nbsp;&nbsp;- Manager's first name</p>
                        <p><code>{lastname}</code> &nbsp;&nbsp;&nbsp;- Manager's last name</p>
                        <p><code>{email}</code> &nbsp;&nbsp;&nbsp;- Manager's email address</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CONTENT FRAME LEFT -->


        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-body">
            <div class="block">
                    {!! Form::open(['method'=>'POST','class'=>'form-horizontal','role'=>'form']) !!}
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <button class="btn btn-primary"><span class="fa fa-fighter-jet"></span> Start blasting..</button>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-2 control-label">Subject:</label>
                        <div class="col-md-10">
                            {!! Form::text('title',null,['placeholder'=>"Mail subject",'class'=>'form-control','required'=>'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label">cc:</label>
                        <div class="col-md-10">
                            {!! Form::text('cc',null,['placeholder'=>"Carbon Copy to email address",'class'=>'form-control']) !!}
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-12">
                                    <textarea name="body" id="summernote_email">
                                        <p><br><br></p>
                                        {{$user->getSignature()}}
                                    </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <button class="btn btn-primary"><span class="fa fa-fighter-jet"></span> Start blasting..</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>
        <!-- END CONTENT FRAME BODY -->
    </div>
    <!-- END CONTENT FRAME -->
@stop


@section('thisPageJs')


    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/summernote/summernote.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->

    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>

@stop

@section('customJs')
    <script type="text/javascript">

        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#summernote_email').summernote({height: 400, focus: true});

        });
    </script>
@stop
