<ul class="x-navigation">
    <li class="xn-logo">
        <a href="{{URL::route('user.dashboard.index')}}">Wi-Fi Management System</a>
        <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
        <div class="profile">
            <div class="profile-image">
                <img width="100px" height="100px" id="avatar" src="{{ \App\Models\User::find(Auth::id())->getProfilePhotoUrl()}}" alt="User Avatar"/>
            </div>
            <div class="profile-data">
                <div class="profile-data-name">{{Auth::user()->getFullName()}}</div>
                <div class="profile-data-title">Manager's Panel</div>
            </div>
            <div class="profile-controls">
                <a href="{{URL::route('manager.dashboard.getSettings')}}" class="profile-control-left"><span class="fa fa-gear"></span></a>
                <a href="{{URL::route('manager.support.getTickets','all')}}" class="profile-control-right"><span class="fa fa-envelope"></span></a>
            </div>
        </div>
    </li>

    <li class="xn-title">Navigation</li>

    <li class="active">
        <a href="{{URL::route('manager.dashboard.index')}}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
    </li>

    @if($vars['showBilling'])
        <li class="xn-openable">
            <a href="#"><span class="glyphicon glyphicon-list-alt"></span> Billing</a>
            <ul>
                <li>
                    <a href="{{URL::route('manager.billing.getSubscription')}}"><span class="fa fa-suitcase"></span> Subscription</a>
                </li>
                <li>
                    <a href="{{URL::route('manager.billing.getInvoices')}}"><span class="fa fa-credit-card"></span> Invoices</a>
                    @if($vars['invoiceCount'])
                        <div class="informer informer-warning">{{$vars['invoiceCount']}}</div>
                    @endif
                </li>
                {{--@if($vars['enable_user_billing'])--}}
                    {{--<li><a href="#"><span class="fa fa-money"></span> Payments</a> </li>--}}
                {{--@endif--}}
            </ul>
        </li>
    @endif

    @if($vars['showSupport'])
        <li class="xn-openable">
            <a href="#"><span class="fa fa-envelope"></span> Supports</a>
            <ul>
                @if($vars['enable_user_support'])
                    <li>
                        <a href="{{URL::route('manager.support.getMailBlast')}}"><span class="fa fa-inbox"></span> Email Blast</a></li>
                    <li>
                @endif
                <li>
                    <a id="menu_all_tickets" href="{{URL::route('manager.support.getTickets','all')}}"><span class="fa fa-comments-o"></span> All Tickets</a>
                </li>
                <li>
                    <a id="menu_opened_tickets" href="{{URL::route('manager.support.getTickets','opened')}}"><span class="fa fa-comment-o"></span> Opened Tickets</a>
                    @if($vars['openTicketCount'])
                        <div class="informer informer-warning">{{$vars['openTicketCount']}}</div>
                    @endif
                </li>
                <li><a href="{{URL::route('manager.support.getTickets','closed')}}"><span class="fa fa-comment"></span> Closed Tickets</a></li>
                <li><a href="{{URL::route('manager.support.getNewTicket')}}"><span class="fa fa-pencil"></span> Create Ticket</a></li>
            </ul>
        </li>
    @endif

    @if($vars['enable_user_account'])
        <li class="xn-openable">
            <a href="#"><span class="fa fa-users"></span> <span class="xn-text">User Account</span></a>
            <ul>
                <li><a href="{{URL::route('manager.package.getNew')}}"><span class="fa fa-plus-circle"></span> Add Package</a></li>
                <li ><a id="view-packages" href="{{URL::route('manager.package.getList')}}"><span class="fa fa-tasks"></span> View Packages</a></li>
                <li><a href="{{URL::route('manager.user.getRegister')}}"><span class="fa fa-magic"></span> Register User</a></li>
                <li><a id="view-user" href="{{URL::route('manager.user.view')}}"><span class="fa fa-user"></span> View Users</a></li>
            </ul>
        </li>
    @endif



</ul>