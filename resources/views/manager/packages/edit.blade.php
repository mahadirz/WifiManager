@extends('manager.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">User Account</li>
@stop

@section('pageContentWrap')

    @if(Session::has('errors'))
        @foreach (Session::get('errors')->all() as $message)
            <div class="row">
                <div class="col-md-12" style="padding: 15px;">
                    <div class="alert alert-danger" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        {{$message}}
                    </div>
                </div>
            </div>
        @endforeach
    @endif


    @if(Session::has('data'))
    <div class="row">
        <div class="col-md-12" style="padding: 15px;">
            <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                {{Session::get('data')['msg']}}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            {!! Form::model($package,['class'=>'form-horizontal','method'=>'post']) !!}
            {{--<form method="post" class="form-horizontal">--}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <img style="padding-right: 5px" width="32px" height="32px" src="{{URL::asset('img/icons/Box-icon.png')}}" />
                            <strong>Edit package</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label"><span style="color: red">*</span>Package Name</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    {!! Form::text('name',null,['class'=>'form-control','required']) !!}
                                </div>
                                <span class="help-block">The Package name to identify the package</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Package Descriptions</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    {!! Form::text('descriptions',null,['class'=>'form-control']) !!}
                                </div>
                                <span class="help-block">Optional field to describe the package</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Data Quota Limit</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    {!! Form::text('data_limit',null,['class'=>'form-control','required']) !!}
                                </div>
                                <span class="help-block">Maximum data transfer in MegaBytes (0 for unlimited)</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Maximum Connections</label>
                            <div class="col-md-6 col-xs-12">
                                    <select id="max_connection_limit" name="max_connection_limit" class="form-control select">
                                    </select>
                                <span class="help-block">Maximum instance of connection the user can made (example if 2 user can connect
                                from mobile phone and computer at the same time)</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Maximum Guest Connections</label>
                            <div class="col-md-6 col-xs-12">
                                <select id="guest_connection_max" name="guest_connection_max" class="form-control select">
                                </select>
                                <span class="help-block">Maximum instance of connection the guest of each user can made</span>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Enable User Daily Limit</label>
                            <div class="col-md-6 col-xs-12">
                                <label class="switch">
                                    {!! Form::checkbox('daily_limit_enabled') !!}
                                    <span></span>
                                </label>
                                <span class="help-block">Enable support page in user panel </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Data Quota Limit</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    {!! Form::text('daily_data_limit',null,['class'=>'form-control','required']) !!}
                                </div>
                                <span class="help-block">Maximum daily data transfer in MegaBytes (0 for unlimited)</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Hours Limit</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-bars"></span></span>
                                    {!! Form::text('daily_hours_max',null,['class'=>'form-control','required']) !!}
                                </div>
                                <span class="help-block">Maximum daily connection hours (0 for unlimited)</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Time From</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <div class="input-group bootstrap-timepicker">
                                        {!! Form::text('daily_time_from',null,['class'=>'form-control timepickerText','required']) !!}
                                        <span class="input-group-addon timepicker"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <span class="help-block">The user can only connect to Access Point within this time</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Time To</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <div class="input-group bootstrap-timepicker">
                                        {!! Form::text('daily_time_to',null,['class'=>'form-control timepickerText','required']) !!}
                                        <span class="input-group-addon timepicker"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <span class="help-block">The user can only connect to Access Point within this time</span>
                            </div>
                        </div>

                    <div class="panel-footer">
                        <button type="button" onclick="location.href = '{{URL::route('manager.package.getList')}}'" class="btn btn-default pull-left">Back</button>
                            <button class="btn btn-info pull-right">Save</button>
                    </div>
                </div>

                {!! Form::close() !!}

        </div>

    </div>
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-timepicker.min.js')}}"></script>

    <script type="application/javascript" src="{{URL::asset('js/countries.js')}}"></script>

    <script type="text/javascript">
        var maxConnection = {{$maxConnection}};
        var currentSelector = '';

        function setInputSelect(selector,selected,max){
            //console.log('setInputSelect: '+selected+" max:"+max);
            for(i=0;i<=max;i++) {
                a = $(selector);
                append = $("<option></option>")
                        .attr("value", i)
                        .text(i);
                if (i == selected) {
                    append.attr('selected', 'true');
                }
                a.append(append);
            }
        }

        setInputSelect('#max_connection_limit',{{$package->max_connection_limit}},maxConnection);
        setInputSelect('#guest_connection_max',{{$package->guest_connection_max}},maxConnection);


        $(document).ready(function () {
            $('.timepicker').timepicker().on('changeTime.timepicker', function(e) {
                $(this).parent().children('.timepickerText').val(e.time.value);
            }).on('show.timepicker', function(e) {
                $(this).parent().children('.timepickerText').val(e.time.value);
            });

            $('#max_connection_limit').change(function(){
                if(currentSelector == ""){
                    currentVal = $(this).val();
                    c = maxConnection - currentVal;
                    currentSelector = 'max_connection_limit';
                    otherval = $("#guest_connection_max").val();
                    $("#guest_connection_max").html('');
                    setInputSelect('#guest_connection_max',otherval,c);
                    $("#guest_connection_max").selectpicker('refresh');
                    currentSelector = '';
                }

            });

            $('#guest_connection_max').change(function(){
                if(currentSelector == ""){
                    currentVal = $(this).val();
                    c = maxConnection - currentVal;
                    currentSelector = 'guest_connection_max';
                    otherval = $("#max_connection_limit").val();
                    $("#max_connection_limit").html('');
                    setInputSelect('#max_connection_limit',otherval,c);
                    $("#max_connection_limit").selectpicker('refresh');
                    currentSelector = '';
                }

            });


        });


    </script>

    <script type="application/javascript" src="{{URL::asset('js/custom.js')}}"></script>


@stop