@extends('manager.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Billing</li>
    <li class="active">Invoice</li>
@stop

@section('pageContentWrap')

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/csv.png')}}" /><strong>Invoice</strong></h3>
                    <ul class="panel-controls">
                        <li style="margin-top:5px;margin-right:30px">
                            @if($billing->getInvoice()->status == "unpaid")
                                <span style="padding:5px;color:white;background-color: red">UNPAID</span>
                            @else
                                <span style="padding:5px;color:white;background-color: green">PAID</span>
                            @endif
                        </li>
                    </ul>
                </div>

                <div class="panel-body">

                    <div class="block">
                        <div class="col-md-8 col-md-offset-4">
                            <h3 >Please select your payment method</h3>
                        {!! Form::open(['method'=>'post','id'=>'form_subscribe','class'=>'form-inline']) !!}
                        <div class="form-group">
                            <div class="col-md-3">
                                <select id="payment_method" name="payment_method" class="form-control">
                                    <option value="paypal">Paypal / Credit Card</option>
                                    <option value="manual">Bank Transfer / Cash</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <select name="cycle" class="form-control">
                                    @if($billing->getInvoice()->packageable->monthly_price > 0)
                                        <option value="monthly_price">Monthly ({{$billing->getCurrency().$billing->getInvoice()->packageable->monthly_price}})</option>
                                    @endif
                                    @if($billing->getInvoice()->packageable->semi_annually_price > 0)
                                        <option value="semi_annually_price">Semi Annually ({{$billing->getCurrency().$billing->getInvoice()->packageable->semi_annually_price}})</option>
                                    @endif
                                    @if($billing->getInvoice()->packageable->annually_price > 0)
                                        <option value="annually_price">Annually ({{$billing->getCurrency().$billing->getInvoice()->packageable->annually_price}})</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                                <button id="button_pay" type="submit" class="btn btn-danger">Pay</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                            </div>
                    </div>




                    <!-- PayPal Logo -->
                    <table border="0" cellpadding="10" cellspacing="0" align="center">
                        <tr>
                            <td align="center"></td>
                        </tr>
                        <tr  id="row_paypal">
                            <td align="center">
                                <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_SbyPP_mc_vs_dc_ae.jpg"
                                            border="0" alt="PayPal Acceptance Mark"></td>
                        </tr>
                        <tr style="display: none" id="row_manual">
                            <td align="center">
                                {!! config('services.manual.instructions') !!}
                            </td>
                        </tr>
                    </table>
                    <!-- PayPal Logo -->

                </div>

                <div class="panel-footer">
                </div>
            </div>

        </div>

    </div>

    <!-- MESSAGE BOX-->
    <div id="confirmBox">
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function () {

            $('#payment_method').change(function(){
                payment_method = $(this).children(":selected").val();
                console.log(payment_method);
                if(payment_method == "manual"){
                    $("#button_pay").hide();
                    $("#row_paypal").hide();
                    $("#row_manual").show();
                }
                else{
                    $("#button_pay").show();
                    $("#row_paypal").show();
                    $("#row_manual").hide();
                }
            })

        });

    </script>


@stop