@extends('manager.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Billing</li>
    <li class="active">Subscription</li>
@stop

@section('pageContentWrap')

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/ppt.png')}}" /><strong>Subscription</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>


                <div class="panel-body">
                    @if(!is_null($subscription))
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                            <tr>
                                <th>Package Name</th>
                                <th>Package Descriptions</th>
                                <th>Status</th>
                                <th>Expiry Date</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <td>{{$subscription->subscribable->name}}</td>
                            <td>{{$subscription->subscribable->descriptions}}</td>
                            <td>
                                @if($subscription->active == false)
                                    <span style="background-color:red;color:white;padding:4px">cancelled</span>
                                @elseif( \Carbon\Carbon::now() > Carbon\Carbon::parse($subscription->ends_at))
                                    <span style="background-color:darkred;color:white;padding:4px">expired</span>
                                @else
                                    <span style="background-color:darkgreen;color:white;padding:4px">active</span>
                                @endif
                            </td>
                            <td>{{$subscription->ends_at}}</td>



                            <td>
                                {!! Form::open(['method'=>'post','id'=>'form_subscription']) !!}
                                    <input type="hidden" value="1" name="subscription">
                                    <input type="hidden" value="cancel" name="action">
                                    @if($subscription->active == true)
                                        <button name="cancel" value="1" class="btn btn-danger"> Cancel</button>
                                    @endif
                                        <button name="renew" value="1" class="btn btn-default"> Renew</button>
                                {!! Form::close() !!}
                            </td>


                            </tbody>
                        </table>

                    </div>
                    @endif

                    @if( is_null($subscription) || \Carbon\Carbon::now() > Carbon\Carbon::parse($subscription->ends_at) || $subscription->subscribable->isFree())

                        <h3>Subscribe to package</h3>
                        {!! Form::open(['method'=>'post','id'=>'form_subscribe']) !!}
                        <input type="hidden" name="subscribe" value="1">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Package</label>
                            <div class="col-md-3">
                                <select name="package_id" class="form-control">
                                    <option selected disabled>---- Please select ----</option>
                                    @foreach($packages as $package)
                                        <option value="{{$package->id}}">{{$package->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <button name="subscribe" value="1" type="submit" class="btn btn-info">Subscribe</button>
                            </div>
                        </div>
                        <div class="form-group">

                        </div>
                        {!! Form::close() !!}
                    @endif

                </div>

                <div class="panel-footer">
                </div>
            </div>


        </div>

    </div>


    <!-- MESSAGE BOX-->
    <div id="confirmBox">
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



        $(document).ready(function () {
            $('#form_subscription').submit(function(event){
                var btn = $(this).find("button:focus" );
                var btnName = btn.attr('name');
                if(btnName == "renew"){
                    confirmBox("Confirmation","Are you sure to renew this package?","#confirmBox",null,null,function(){
                        $('#form_subscription').find('input[name=action]').val('renew');
                        document.getElementById("form_subscription").submit()
                    });
                }
                else if(btnName == "cancel"){
                    confirmBox("Confirmation","Are you sure to cancel this package?","#confirmBox",null,null,function(){
                        $('#form_subscription').find('input[name=action]').val('cancel');
                        document.getElementById("form_subscription").submit()
                    });
                }
                event.preventDefault();
            });

            $('#form_subscribe').submit(function(event){
                confirmBox("Are you sure?","Any existing subscription will be overridden","#confirmBox","danger","times-circle",function(){
                    $('#form_subscribe').find('input[name=subscribe]').val(1);
                    document.getElementById("form_subscribe").submit()
                });
                event.preventDefault();
            });


        });

    </script>


@stop