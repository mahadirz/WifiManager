@extends('manager.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li>Billing</li>
    <li class="active">Invoices</li>
@stop

@section('pageContentWrap')

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif


    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><img width="32px" height="32px" src="{{URL::asset('img/icons/invoice.png')}}" /><strong>Invoices</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-actions">
                            <thead>
                            <tr>
                                <th width="10">ID</th>
                                <th>Package Name</th>
                                <th>Package Descriptions</th>
                                <th>Amount Paid</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($invoices as $invoice)
                                <tr>
                                    <td>{{$invoice->id}}</td>
                                    <td>{{$invoice->packageable->name}}</td>
                                    <td>{{$invoice->packageable->descriptions}}</td>
                                    <td>
                                        @if($invoice->paid_amount > 0)
                                            {{$billing->getCurrency().$invoice->paid_amount}}
                                        @else
                                            ---
                                        @endif
                                    </td>
                                    <td>{{$invoice->created_at}}</td>
                                    <td>
                                        <span style="background-color:
                                        @if($invoice->status == "unpaid")
                                                red;
                                        @elseif($invoice->status == "paid")
                                                green;
                                        @else
                                                #BCC1C8;
                                        @endif
                                                color:white;padding: 4px;">
                                            {{$invoice->status}}
                                        </span>

                                    </td>
                                    <td>
                                        {!! Form::open(['method'=>'post','id'=>'form_invoice','class'=>'form_invoice_'.$invoice->id]) !!}
                                        <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
                                        @if($invoice->status == "unpaid")
                                            <button type="button" onclick="window.location.href = '{{URL::route('manager.billing.getInvoice',$invoice->id)}}'" class="btn btn-info"><i class="fa fa-file-text-o"></i> View</button>
                                            <button name="cancel" value="1" class="btn btn-danger"><i class="fa fa-times-circle"></i> Cancel</button>
                                        @endif
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        @if(count($invoices)>0)
                            {!!$invoices->render()!!}
                        @endif
                    </div>
                </div>

                <div class="panel-footer">
                </div>
            </div>

        </div>

    </div>

    <!-- MESSAGE BOX-->
    <div id="confirmBox">
    </div>


@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


    <script type="text/javascript" >

        //ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ready(function () {

            var formClass;

            $('#form_invoice').submit(function (event) {
                formClass = $(this).attr('class');
                console.log(formClass);
                confirmBox("Confirmation","Are you sure to cancel the invoice?","#confirmBox",null,null,function(){
                    //console.log(formInstance.find('input[name=invoice_id]').val());
                    console.log(formClass);
                    $('.'+formClass).unbind('submit').submit();
                });
                event.preventDefault();
            });

        });

    </script>


@stop