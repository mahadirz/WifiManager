@extends('manager.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/overlayLoading.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/fileinput.css')}}"/>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Settings</li>
@stop

@section('pageContentWrap')

    <div style="display: none" class="loading"></div>

    <div id="confirmBox"></div>

    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    <strong>{{Session::get('data')['title']}} </strong>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> Update Profile</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                    {!! Form::model($user,array('route' => 'manager.dashboard.postSettings','class'=>'form-horizontal','id'=>'jvalidate', 'method' => 'post')) !!}
                    {!! !Form::token() !!}
                    <input type="hidden" name="update_profile" value="1">

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label"> First Name</label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-align-justify"></span></span>
                                {!! Form::text('first_name',null,array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label"> Last Name</label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-align-justify"></span></span>
                                {!! Form::text('last_name',null,array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>


                    <div class="panel-footer">
                        <button class="btn btn-warning pull-left">Update Profile</button>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}

        </div>

    </div>

    <div id="notification" style="margin-bottom: 20px;" class="row">
    </div>

    <div class="col-md-8">

        <div class="panel panel-default">


            <div class="panel-body">
                <h3><span class="fa fa-mail-forward"></span> Update Profile Picture</h3>
                <p>Images file must not exceeds <code>5MB</code> </p>
                     <div class="form-group">
                        <div class="col-md-12">
                            <label>File</label>
                            {!! Form::file('photo',array('id'=>'file-uploader','class'=>"file-loading",'accept'=>"image/*",'data-preview-file-type'=>"image/*",'name'=>'photo')) !!}
                        </div>
                    </div>

            </div>
        </div>

    </div>

    <div class="col-md-4">

        <div class="panel panel-colorful">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-key"></span><strong> Login Security</strong></h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                </ul>
            </div>

            <div class="panel-body">

                <div style="display: none" id="google2faurl">
                    <p>Scan this QR from Google Authenticator Mobile App</p>
                    <img id="google2faimage" src="">
                    <!-- START INLINE FORM SAMPLE -->
                    <div class="block">
                        <h4>Verify the code from your App</h4>
                        <form  class="form-inline" role="form">
                            <div class="form-group">
                                <label class="sr-only">Code</label>
                                <input name="google2facode" type="text" class="form-control" placeholder="Enter the Code"/>
                            </div>
                            <button id="verify_code" type="button" class="btn btn-danger">Submit</button>
                        </form>
                    </div>
                    <!-- END INLINE FORM SAMPLE -->
                </div>

                <div style="display: none" id="backupcodes">
                </div>

                @if(!is_null($user->google2fa) && $user->google2fa->enabled)
                    <button id="disable2fa" class="btn btn-default"> <i class="fa fa-flag-o"></i> &nbsp;Disable Two Factors Authentication</button>
                    <button style="display: none" id="enable2fa" class="btn btn-danger"> <i class="fa fa-bolt"></i> &nbsp;Enable Two Factors Authentication</button>
                @else
                    <button id="enable2fa" class="btn btn-danger"> <i class="fa fa-bolt"></i> &nbsp;Enable Two Factors Authentication</button>
                @endif

            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> Update Email</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a data-placement="left" class="popover-dismiss"
                               data-content="For security reasons you must verify your current password before updating email address,
                               please use reset password to change your password if you have forgotten it"
                               href="#" ><span class="fa fa-info"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                    {!! Form::model($user,array('route' => 'manager.dashboard.postSettings','class'=>'form-horizontal','id'=>'jvalidate2', 'method' => 'post')) !!}
                    {!! Form::token() !!}
                    {!! Form::hidden('update_email',1) !!}

                    <p>
                        For security reasons you must verify your current password before updating email address
                    </p>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label"> Current Password</label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-lock"></span></span>
                                {!! Form::password('password',array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label"> Email Address</label>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="fa fa-envelope"></span></span>
                                {!! Form::email('email',null,array('class' => 'form-control')) !!}
                            </div>
                            <div id="email_loading" style="margin-top: 5px;display: None" >
                                <span class="glyphicon glyphiconX glyphicon-refresh spinning"></span> Checking...
                            </div>
                        </div>
                    </div>



                    <div class="panel-footer">
                        <button class="btn btn-danger pull-left">Update Email</button>
                    </div>
                </div>
            </div>
            </form>

            {!! Form::close() !!}

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong> Update Signature</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                    {!! Form::model($user->profile,array('route' => 'manager.dashboard.postSettings','class'=>'form-horizontal', 'method' => 'post')) !!}
                    {!! Form::token() !!}
                    {!! Form::hidden('update_signature',1) !!}


                    <div class="form-group">
                        <div class="col-md-12">
                            {!! Form::textarea('signature',null,['class'=>'summernote_signature']) !!}
                        </div>
                    </div>


                    <div class="panel-footer">
                        <button class="btn btn-danger pull-left">Update signature</button>
                    </div>
                </div>
            </div>
            </form>

            {!! Form::close() !!}

        </div>

    </div>


@stop

@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>


    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>


@stop

@section('customJs')

    <script type="text/javascript" src="{{URL::asset('js/custom.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/pwstrength-bootstrap-1.2.7.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            var options = {};
            options.ui = {
                container: "#pwd-container",
                showVerdictsInsideProgressBar: true,
                viewports: {
                    progress: ".pwstrength_viewport_progress"
                }
            };
            $('#password').pwstrength(options);

        });
    </script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>


    <script type='text/javascript' src="{{ URL::asset('js/plugins/validationengine/jquery.validationEngine.js')}}"></script>

    <script type='text/javascript' src="{{ URL::asset('js/plugins/jquery-validation/jquery.validate.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/dropzone/dropzone.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/fileinput/canvas-to-blob.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/fileinput/fileinput.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/summernote/summernote.js')}}"></script>
    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': myApp.csrf_token
            }
        });

        $("#jvalidate3").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 200
                },
                'password2': {
                    required: true,
                    equalTo: "#password"
                }
            }
        });

        $("#jvalidate2").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "{{URL::route('ajax.postEmail')}}",
                        type: "post",
                        beforeSend: function( xhr ) {
                            $('#email_loading').show();
                        },
                        complete: function(data){
                            $('#email_loading').hide();
                            console.log(data);
                        },
                        data: {
                            email: function() {
                                return $("input[name=email]").val();
                            },
                            user_id:myApp.user_id
                        }
                    }
                }
            }
        });

        var jvalidate = $("#jvalidate").validate({
            ignore: [],
            errorPlacement: function(error, element) {
                //append error message below input-group
                error.insertAfter( element.parent('div'));
            },
            rules: {
                first_name: {
                    required: true,
                    minlength: 5,
                    maxlength: 100
                },
                last_name: {
                    required: true,
                    minlength: 5,
                    maxlength: 100
                }
            },

            messages: {
                email: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            }
        });

    </script>

    <script>


            $("#file-uploader").fileinput({
                showUpload: true,
                showCaption: true,
                allowedFileTypes:['image'],
                allowedPreviewTypes:['image'],
                resizeImage:true,
                maxImageWidth: 200,
                maxImageHeight: 200,
                resizePreference: 'width',
                resizeDefaultImageType:'image/png',
                browseClass: "btn btn-info",
                uploadUrl:"{{URL::route('ajax.postUploadProfile')}}",
                maxFileSize:5000,
                initialPreview: [
                    "<img src='{{ \App\Models\User::find(Auth::id())->getProfilePhotoUrl()}}' class='file-preview-image' alt='Desert' title='profile photo'>"
                ]
            }).on('filecleared', function(event) {
                console.log("filecleared");
            }).on('fileloaded', function(event, file, previewId, index, reader) {
                console.log("fileloaded");
            }).on('filereset', function(event) {
                console.log("filereset");
            }).on('fileuploaded', function(event, data, previewId, index) {
                var form = data.form, files = data.files, extra = data.extra,
                        response = data.response, reader = data.reader;
                console.log('File uploaded triggered');
                console.log(response);
                $('#avatar').attr('src',response.photo_path+"?timestamp=" + new Date().getTime());
                createNotification('success','Profile photo updated!','#notification');
            });

            $('#disable2fa').on('click',function(){
                confirmBox('<strong>Are you sure?</strong>',
                        'Please confirm your action to disable 2 factors authentication<p>Click Yes to proceeds</p>',
                        '#confirmBox',null,null,function(){
                            //yes clicked
                            $.ajax({
                                url: '{{URL::route('ajax.postDisable2Fa')}}',
                                'method': 'POST',
                                'beforeSend': function () {
                                    $('.loading').show();
                                },
                                'data': {}
                            }).done(function (data) {
                                if(data.success){
                                    $('#disable2fa').hide();
                                    $('#enable2fa').show();
                                }
                                else{
                                    messageBox('Error',
                                            'Internal error, please contact administrator',
                                            '#confirmBox','danger','times');
                                }

                            })
                                    .fail(function () {
                                        alert("error");
                                    })
                                    .always(function (data) {
                                        $('.loading').hide();
                                        console.log(data);
                                    });
                        });

            });

            $('#enable2fa').on('click',function(){
                $.ajax({
                    method:'POST',
                    url:'{{URL::route('ajax.postGenerate2FaQRCodeUrl')}}',
                    beforeSend:function(){
                        $('.loading').show();
                    }
                }).always(function(data){
                    console.log(data);
                    $('.loading').hide();
                }).done(function(data){
                    $('#google2faurl').show();
                    $('#google2faimage').attr('src',data.image_url);
                    $('#enable2fa').hide();
                });
            });


            function verify2FACode()
            {
                var code = $('input[name=google2facode]').val();
                $.ajax({
                    url: '{{URL::route('ajax.postVerify2FaQRCode')}}',
                    'method': 'POST',
                    'beforeSend': function () {
                        $('.loading').show();
                    },
                    'data': {code:code}
                }).done(function (data) {
                    c = $('#backupcodes');
                    c.show();
                    if(data.valid){
                        $('#google2faurl').hide();
                        c.html('<h4>Please save these backup codes in case your mobile phone is not with you</h4>' +
                                '<h2>'+data.backupcode+'</h2><h2>'+data.backupcode2+'</h2>');
                    }
                    else{
                        c.html('<div class="alert alert-danger" role="alert">'+
                                '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>'+
                                '<strong>Error!</strong> Invalid code.</div>');
                        $('input[name=google2facode]').val('');
                    }

                })
                        .fail(function () {
                            alert("error");
                        })
                        .always(function (data) {
                            console.log(data);
                            $('.loading').hide();
                        });
            }

            $('input[name=google2facode]').keypress(function (e) {
                if (e.which == 13) {
                    //enter key
                    verify2FACode();
                    return false;
                }
            });

        $('#verify_code').on('click',function() {
            verify2FACode();
        });

            $(".summernote_signature").summernote({height: 200, focus: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

    </script>



    <!-- END SCRIPTS -->

@stop
