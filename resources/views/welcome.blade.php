<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>Home and Enterprise Wifi Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- END META SECTION -->

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front-end/css/revolution-slider/extralayers.css')}}" media="screen" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front-end/css/revolution-slider/settings.css')}}" media="screen" />

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('front-end/css/styles.css')}}" media="screen" />

</head>
<body>
<!-- page container -->
<div class="page-container">

    <!-- page header -->
    <div class="page-header">

        <!-- page header holder -->
        <div class="page-header-holder">

            <!-- page logo -->
            <div class="logo">
                <a href="{{URL::route('home.index')}}">Wifi Management System</a>
            </div>
            <!-- ./page logo -->



            <!-- nav mobile bars -->
            <div class="navigation-toggle">
                <div class="navigation-toggle-button"><span class="fa fa-bars"></span></div>
            </div>
            <!-- ./nav mobile bars -->

            <!-- navigation -->
            <ul class="navigation">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="{{URL::route('login.index')}}">Login</a>
                </li>
                <li>
                    <a href="http://www.madet.my">Blog</a>
                </li>
                <li>
                    <a href="#">Screenshots</a>
                </li>
            </ul>
            <!-- ./navigation -->


        </div>
        <!-- ./page header holder -->

    </div>
    <!-- ./page header -->


</div>
<!-- ./page container -->

<!-- page scripts -->
<script type="text/javascript" src="{{ URL::asset('front-end/js/plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('front-end/js/plugins/bootstrap/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('front-end/js/plugins/mixitup/jquery.mixitup.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('front-end/js/plugins/appear/jquery.appear.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('front-end/js/plugins/revolution-slider/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('front-end/js/plugins/revolution-slider/jquery.themepunch.revolution.min.js')}}"></script>

<script type="text/javascript" src="{{ URL::asset('front-end/js/actions.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('front-end/js/slider.js')}}"></script>
<!-- ./page scripts -->
</body>
</html>






