@extends('user.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Statistics</li>
@stop

@section('pageContentWrap')
    <div class="row">
        <div class="col-md-12">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Data Usage Chart</h3>
                </div>
                <div class="panel-body">
                    <div id="morris-line-usage-statistics" style="height: 300px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <!-- START LINE CHART -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Hours Usage Chart</h3>
                </div>
                <div class="panel-body">
                    <div id="morris-line-hour-usage-statistics" style="height: 300px;"></div>
                </div>
            </div>
            <!-- END LINE CHART -->

        </div>
    </div>

@stop

@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/morris.min.js')}}"></script>


    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>



@stop

@section('customJs')
    <script type="text/javascript">
        var morrisCharts = function() {

            Morris.Line({
                element: 'morris-line-usage-statistics',
                data: [
                    {!! $usageStats !!}
                ],
                xkey: 'x',
                ykeys: ['a', 'b','c'],
                labels: ['Downloads (MB)', 'Uploads (MB)','Total (MB)'],
                resize: true
            });

            Morris.Line({
                element: 'morris-line-hour-usage-statistics',
                data: [

                    {!! $hourStats !!}
                ],
                xkey: 'x',
                ykeys: 'a',
                labels: 'Hours',
                resize: true
            });

        }();
    </script>
@stop

