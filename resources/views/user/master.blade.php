<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>Homes and Enterprises Wi-Fi Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="icon" href="{{ URL::asset('favicon.ico')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/theme-default.css')}}"/>
    <!-- EOF CSS INCLUDE -->
    @yield('customHeader')
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        @include('user.partials.navigation')
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        @include('user.partials.verticalNavigation')
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            @yield('breadcrumb')
        </ul>
        <!-- END BREADCRUMB -->

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap">
            @yield('pageContentWrap')
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
            <div class="mb-content">
                <p>Are you sure you want to log out?</p>
                <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="{{URL::route('login.logout')}}" class="btn btn-success btn-lg">Yes</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->


<!-- START SCRIPTS -->
<!-- START PLUGINS -->

<script type="text/javascript">
    var publicPath = '{{URL::asset('')}}';
</script>

<script type="text/javascript" src="{{ URL::asset('js/plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/plugins/jquery/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/plugins/socket.io-client/socket.io.js')}}"></script>
<!-- END PLUGINS -->

<!-- socket init -->
<script type="text/javascript">
    var socket  = io('{{config('app.socket_url')}}');
    var user_id = '{{Auth::id()}}';
</script>

<script type="text/javascript">
    //open active menu
    $('li.xn-openable').each(function(){
        var parentXn = $(this);
        $(this).children('ul').each(function(){
            $(this).children('li').each(function(){
                //console.log(parentXn);
                //get only URL
                var myRegexp = /([\S\s][^?]{0,})(\?.+)?/i;
                var match = myRegexp.exec(document.location);
                var url = match[1];
                var url_menu = $(this).children('a').attr('href');
                if(match[1][match[1].length-1] == "/"){
                    url = match[1].substring(0,match[1].length-1);
                }
                //console.log(match[1]+'=='+ $(this).children('a').attr('href'));

                        @if(isset($menu_id_open))
                            var menu_id_open = '{{$menu_id_open}}';
                if($(this).children('a').attr('id') == menu_id_open){
                    $(this).addClass('active');
                    parentXn.addClass('active');
                }
                @else
                if(document.location == url_menu || url == url_menu)
                {
                    $(this).addClass('active');
                    parentXn.addClass('active');
                }
                @endif
            })
        })
    });
</script>

<!-- START THIS PAGE PLUGINS-->
@yield('thisPageJs')
<!-- END THIS PAGE PLUGINS-->

<!-- custom js -->
@yield('customJs')
<!-- end custom js -->

<!-- START TEMPLATE -->


<script type="text/javascript" src="{{ URL::asset('js/plugins.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('js/actions.js')}}"></script>

<script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function retrieveUnreadMessages()
    {
        $.ajax({
            url: '{{URL::route('ajax.postMessages')}}',
            'method': 'POST',
            'beforeSend': function () {
            },
            'data': {}
        }).done(function (data) {
            console.log(data.messages.length);
            if(data.count>0){
                $("#new_messages_count1").html(data.count);
                $("#new_messages_count2").html(data.count+" new").show();

                //set the text
                var divData = '';
                for(i=0;i<data.messages.length;i++){
                    messageObject = data.messages[i];
                    htmldata = '<a href="'+messageObject.ticket_url+'" class="list-group-item">';
                    if(messageObject.ticket_status == "open"){
                        htmldata += '<div class="list-group-status status-online"></div>';
                    }
                    else{
                        htmldata += '<div class="list-group-status status-away"></div>';
                    }
                    htmldata += '<img src="'+messageObject.profile_url+'" class="pull-left" alt="Profile Photo"/> <span class="contacts-title">'
                            +messageObject.full_name+'</span><p>'+messageObject.title+'</p></a>';
                    divData += htmldata;

                }

                //the height 1 message 50px
                height = 60 * data.count;
                $("#new_messages_contents").html(divData).attr('style','height: '+height+'px;');


            }

        })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    //
                });
    }

    $(document).ready(function () {

        //for first loading..
        retrieveUnreadMessages();

        //listen for new message created event
        socket.on('notification-channel:MessageWasCreated', function(data){
            console.log('notification-channel:MessageWasCreated =>');
            console.log(data);
            if(user_id == data.recipient_id){
                //the broadcast event is for me
                //fetch the new message
                console.log('The notification is for me');
                retrieveUnreadMessages();
            }
        });

    });



</script>


<!-- END TEMPLATE -->
<!-- END SCRIPTS -->
</body>
</html>
