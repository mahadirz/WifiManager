@extends('user.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/overlayLoading.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/fileinput.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/custom.css')}}"/>
    <style>
        .autocomplete-suggestions { border: 1px solid #999; background: #FFF; overflow: auto; }
        .autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden;cursor:pointer; }
        .autocomplete-selected { background: #F0F0F0; }
        .autocomplete-suggestions strong { font-weight: normal; color: #3399FF; }
        .autocomplete-group { padding: 2px 5px; }
        .autocomplete-group strong { display: block; border-bottom: 1px solid #000; }

    </style>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li><a href="#">Support</a></li>
    <li class="active">Create New Ticket</li>

    <!-- START CONTENT FRAME -->
    <div class="content-frame">
        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">
            <div class="page-title">
                <h2><span class="fa fa-pencil"></span> Open a New Ticket</h2>
            </div>

        </div>
        <!-- END CONTENT FRAME TOP -->


        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-center">
            <div class="block">
                    {!! Form::open(['method'=>'POST','class'=>'form-horizontal','role'=>'form']) !!}
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="button" onclick="window.location.href = '{{URL::route('user.support.getTickets','all')}}'" class="btn btn-default"><span class="fa fa-arrow-left"></span> Back</button>
                            </div>
                            <div class="pull-left">
                                <button class="btn btn-danger"><span class="fa fa-envelope"></span> Open Ticket</button>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-1 control-label">Subject:</label>
                        <div class="col-md-10">
                            {!! Form::text('title',null,['placeholder'=>"what's this ticket is about",'class'=>'form-control','required'=>'required']) !!}
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-md-12">
                                    <textarea name="body" class="summernote_email">
                                        <p><br><br></p>
                                        {{is_null($user->profile)?"":$user->profile->signature}}
                                    </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="button" onclick="window.location.href = '{{URL::route('user.support.getTickets','all')}}'" class="btn btn-default"><span class="fa fa-arrow-left"></span> Back</button>
                            </div>
                            <div class="pull-left">
                                <button class="btn btn-danger"><span class="fa fa-envelope"></span> Open Ticket</button>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>
        <!-- END CONTENT FRAME BODY -->
    </div>
    <!-- END CONTENT FRAME -->
@stop


@section('thisPageJs')


    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/summernote/summernote.js')}}"></script>

{{--    <script type="text/javascript" src="{{ URL::asset('js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>--}}
{{--    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-tagsinput.min.js')}}"></script>--}}

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.autocomplete.min.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->

@stop

@section('customJs')
    <script type="text/javascript">

        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

    </script>
@stop
