@extends('user.master')

@section('customHeader')
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li><a href="#">Support</a></li>
    <li class="active">Create New Ticket</li>

    <!-- START CONTENT FRAME -->
    <div class="content-frame">
        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">
            <div class="page-title">
                <h2><span class="fa fa-pencil"></span> Open a New Ticket</h2>
            </div>

        </div>
        <!-- END CONTENT FRAME TOP -->


        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-center">
            <div class="block">

                <div class="col-md-12" style="padding: 15px;">
                    <div class="alert alert-success" id="alert">
                        <strong>Success! </strong>
                        Support ticket id #{{$ticket_id}} created
                    </div>
                </div>

            </div>

        </div>
        <!-- END CONTENT FRAME BODY -->
    </div>
    <!-- END CONTENT FRAME -->
@stop


@section('thisPageJs')
    <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

@stop

@section('customJs')
@stop
