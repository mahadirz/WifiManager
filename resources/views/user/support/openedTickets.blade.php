@extends('user.master')

@section('customHeader')
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/overlayLoading.css')}}"/>
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/custom.css')}}"/>
    <style>
        #mail-item:hover {
            background-color: #DCCFCF;
        }
    </style>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li><a href="#">Support</a></li>
    <li class="active">Opened Tickets</li>

    <!-- START CONTENT FRAME TOP -->
    <div style="margin-top:10px" class="content-frame-top">
        <div class="page-title">
            <h2><span class="fa fa-comments"></span> Opened Tickets</h2>
        </div>

    </div>
    <!-- END CONTENT FRAME TOP -->


            <!-- START CONTENT FRAME BODY -->
            <div class="content-frame-center">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="pull-right" style="width: 240px;">

                                {!! Form::open(['method'=>'POST','class'=>'form-inline','role'=>'form']) !!}
                                <div class="form-group">
                                    <div class="input-group">
                                        <input name="search_keyword" placeholder="Search subject" class="form-control" type="text" data-orientation="left"/>
                                    </div>
                                </div>
                                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="panel-body mail">

                        @foreach($tickets as $ticket)

                            @if(!is_string($ticket))
                                <div onclick="window.location.href ='{{URL::route('user.support.getViewTicket',$ticket->get('ticket_id'))}}'" id="mail-item" style="cursor: pointer" class="mail-item @if($ticket->get('read')==false)mail-unread @endif mail-info">
                                    <div class="mail-user">{{$ticket->get('name')}}</div>
                                    <div class="mail-text">{{$ticket->get('title')}}</div>
                                     <div class="mail-date">{{$ticket->get('created_at')}}</div>
                                </div>
                            @endif

                        @endforeach



                    </div>
                    <div class="panel-footer">
                        <ul class="pagination pagination-sm pull-left">
                            {!! $tickets->get('render')!!}
                        </ul>
                    </div>
                </div>

            </div>
            <!-- END CONTENT FRAME BODY -->
        </div>
        <!-- END CONTENT FRAME -->
    </div>
    <!-- END CONTENT FRAME -->
@stop


@section('thisPageJs')


    <!-- START THIS PAGE PLUGINS-->
    <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/summernote/summernote.js')}}"></script>

{{--    <script type="text/javascript" src="{{ URL::asset('js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>--}}
{{--    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-tagsinput.min.js')}}"></script>--}}

    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.autocomplete.min.js')}}"></script>
    <!-- END THIS PAGE PLUGINS-->

@stop

@section('customJs')
    <script type="text/javascript">

        $(document).ready(function () {


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        });
    </script>
@stop
