@extends('user.master')

@section('customHeader')
    <style type="text/css">
        .messages.messages-img .item .text {
            margin-left: 100px;
            position: relative;
        }
        .messages.messages-img .item.in .text {
            margin-left: 0px;
            margin-right: 100px;
        }
    </style>
@endsection

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li><a href="#">Support</a></li>
    <li class="active">View Ticket</li>

    <!-- START CONTENT FRAME -->
    <div class="content-frame">
        <!-- START CONTENT FRAME TOP -->
        <div class="content-frame-top">
            <div class="page-title">
                <h2><span class="fa fa-envelope-o"></span> #{{$ticketMessages->getTicket()->id}} {{$ticketMessages->getTicket()->title}}</h2>
            </div>

            <div  class="pull-right ">
                {!! Form::open(['method'=>'POST','class'=>'form-inline']) !!}
                {!! Form::hidden('close_ticket',true) !!}
                <button type="button" id="reply" class="btn btn-primary"><span class="fa fa-mail-reply-all"></span> Write a reply</button>
                <button id="close-ticket" class="btn btn-danger"><span class="glyphicon glyphicon-floppy-remove"></span> Close Ticket</button>
                {!! Form::close() !!}
            </div>


        </div>
        <!-- END CONTENT FRAME TOP -->


        <!-- START CONTENT FRAME BODY -->
        <div class="content-frame-center">


            <div class="messages messages-img">

                @foreach($ticketMessages->getMessage() as $messages)

                    @if($messages->user_id != Auth::id())
                        @if($messages->setRead()) @endif
                    @endif

                    @if($ticketMessages->getTicket()->user_id_sender === $messages->user_id)
                        <div class="item in">
                    @else
                        <div class="item in">
                    @endif
                            <div style="width: 100px;" class="image">
                                <a href="#" class="friend">
                                    <img src="{{$messages->user()->first()->getProfilePhotoUrl()}}">
                                    <span>{{$messages->user()->first()->getFullName()}}</span>
                                </a>
                            </div>

                            <div style="background-color: white" class="text">
                                <div class="heading">
                                    <a href="#"></a>
                                    <span class="date">{{$messages->created_at}}</span>
                                </div>
                                {!!$messages->body!!}
                            </div>
                        </div>

                @endforeach

            </div>

            {!! $ticketMessages->getMessage()->render()!!}

            <div id="reply-area" class="panel panel-default push-up-10">
                <div class="block">
                    {!! Form::open(['method'=>'POST','class'=>'form-horizontal','role'=>'form']) !!}

                    <div class="form-group">
                        <div class="col-md-12">
                                    <textarea name="body" class="ticket_reply_textarea">
                                        <p><br><br></p>
                                        {{$user->getSignature()}}
                                    </textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="pull-left">
                                <button class="btn btn-info"><span class="fa fa-mail-reply"></span> Reply</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <!-- END CONTENT FRAME BODY -->

    </div>
    <!-- END CONTENT FRAME -->
@stop


@section('thisPageJs')
    <script type='text/javascript' src="{{ URL::asset('js/plugins/icheck/icheck.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/summernote/summernote.js')}}"></script>

@stop

@section('customJs')
    <script type="text/javascript">
        /* END Lite summernote editor */

        /* Email summernote editor */
        if($(".ticket_reply_textarea").length > 0){

            $(".ticket_reply_textarea").summernote({height: 400, focus: false,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

        }
        /* END Email summernote editor */

        $("#reply").on("click",function(){
            console.log('focus');
            $('html, body').animate({
                scrollTop: $("#reply-area").offset().top
            }, 2000);
        });
    </script>
@stop
