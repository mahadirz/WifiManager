@extends('user.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Guest</li>
@stop

@section('pageContentWrap')

    <div class="row">
        <div class="col-md-12">
        <!-- START PANEL WITH CONTROL CLASSES -->
        <div class="panel panel-warning">
            <form>
            <div class="panel-heading">
                <h3 class="panel-title">Filter</h3>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">



                    <div class="row">

                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">Start Date</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input name="start_date" data-date-format="dd-mm-yyyy" type="text" class="form-control datepicker" value="{{$start_date}}">
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label">End Date</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input name="end_date" data-date-format="dd-mm-yyyy" type="text" class="form-control datepicker" value="{{$end_date}}">
                                </div>
                            </div>
                        </div>


                    </div>



                </div>


            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary pull-right">Apply</button>
            </div>
            </form>
        </div>
        <!-- END PANEL WITH CONTROL CLASSES -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- START SIMPLE DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">List of Guests</h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        <li><a href="#" class="panel-refresh-guest"><span class="fa fa-refresh"></span></a></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable_guest">
                        <thead>
                        <tr>
                            <th>MAC Address</th>
                            <th>Data Usage</th>
                            <th>Total Time</th>
                            <th>Start Time</th>
                            <th>End Time</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $d)
                                <tr>
                                    <td>{{$d['mac']}}</td>
                                    <td>{{$d['usage']}}</td>
                                    <td>{{$d['total_time']}}</td>
                                    <td>{{$d['start']}}</td>
                                    <td>{{$d['end']}}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END SIMPLE DATATABLE -->
        </div>
    </div>



@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>


    <script type='text/javascript' src='{{ URL::asset('js/plugins/datatables/jquery.dataTables.min.js')}}'></script>

    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>

    <script type="text/javascript">
        $.fn.datepicker.defaults.format = "dd-mm-yyyy";
        $('.datepicker').datepicker({
        });

        if($(".datatable_guest").length > 0){
            $(".datatable_guest").dataTable({"ordering": false, "info": false, "lengthChange": false,"searching": false});
            $(".datatable_guest").on('page.dt',function () {
                onresize(100);
            });
        }

        $(".panel-refresh-guest").on("click",function(){
            var panel = $(this).parents(".panel");
            panel_refresh(panel);
            var table = $(".datatable_guest").DataTable();

            //clear all rows
            table.clear().draw();

            $.ajax({
                url: $(location).attr('href'),
                'method': 'GET',
                'beforeSend': function () {
                },
                'data': {}
            }).done(function (data) {
                        console.log(data);
                        for(var i=0;i<data.length;i++){
                            table.row.add([
                                    data[i].mac,
                                    data[i].usage,
                                    data[i].total_time,
                                    data[i].start,
                                    data[i].end
                            ]).draw();
                        }
                    })
                    .fail(function (err) {
                        console.log('Error:'+err);
                    })
                    .always(function () {
                        panel_refresh(panel);
                    });

            $(this).parents(".dropdown").removeClass("open");
            return false;
        });



    </script>


@stop
