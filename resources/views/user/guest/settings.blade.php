@extends('user.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Guest</li>
@stop

@section('pageContentWrap')


    @if(Session::has('errors'))
        @foreach (Session::get('errors')->all() as $message)
            <div class="row">
                <div class="col-md-12" style="padding: 15px;">
                    <div class="alert alert-danger" id="alert">
                        <button type="button" class="close" data-dismiss="alert">x</button>
                        {{$message}}
                    </div>
                </div>
            </div>
        @endforeach
    @endif


    @if(Session::has('data'))
        <div class="row">
            <div class="col-md-12" style="padding: 15px;">
                <div class="alert alert-{{Session::get('data')['alert']}}" id="alert">
                    <button type="button" class="close" data-dismiss="alert">x</button>
                    {{Session::get('data')['msg']}}
                </div>
            </div>
        </div>
    @endif


    @if(!is_null($guestConstraint) && $guestConstraint->enabled)
        <div class="row">
        <div class="col-md-12">

            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Guest</h3>
                        <span>Information</span>
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">

                    </ul>
                </div>
                <div class="panel-body panel-body-table">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="20%"></th>
                                <th width="20%"></th>
                                <th width="60%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><strong>Daily Connection Hour</strong></td>
                                <td>{{$accountInfo['daily_connection_hour']}} @if($accountInfo['daily_data_limit_enabled'] ==0)<span class="label label-danger">Disabled</span>@endif</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><strong>Daily Data Quota</strong></td>
                                <td>{{$accountInfo['daily_data_usage']['used']}}/{{$accountInfo['daily_data_usage']['limit']}}</td>
                                <td>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar {{$accountInfo['daily_data_usage']['progressBar_label']}}" role="progressbar"
                                             aria-valuenow="{{$accountInfo['daily_data_usage']['progressBar_percentage']}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$accountInfo['daily_data_usage']['progressBar_percentage']}}%;">{{$accountInfo['daily_data_usage']['progressBar_percentage']}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Daily Hours Quota</strong></td>
                                <td>{{$accountInfo['daily_hours_usage']['used']}}/{{$accountInfo['daily_hours_usage']['limit']}} hours</td>
                                <td>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar {{$accountInfo['daily_hours_usage']['progressBar_label']}}" role="progressbar"
                                             aria-valuenow="{{$accountInfo['daily_hours_usage']['progressBar_percentage']}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$accountInfo['daily_hours_usage']['progressBar_percentage']}}%;">{{$accountInfo['daily_hours_usage']['progressBar_percentage']}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td><strong>Connection Limit</strong></td>
                                <td>{{$accountInfo['connection_limit']['used']}}/{{$accountInfo['connection_limit']['limit']}}</td>
                                <td>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar {{$accountInfo['connection_limit']['progressBar_label']}}" role="progressbar"
                                             aria-valuenow="{{$accountInfo['connection_limit']['progressBar_percentage']}}"
                                             aria-valuemin="0" aria-valuemax="100"
                                             style="width: {{$accountInfo['connection_limit']['progressBar_percentage']}}%;">{{$accountInfo['connection_limit']['progressBar_percentage']}}%
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- END PROJECTS BLOCK -->

        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['method'=>'post','class'=>'form-horizontal']) !!}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Guest</strong></h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Enable Guest Account</label>
                            <div class="col-md-6 col-xs-12">
                                <label class="switch">
                                    <input id="guest-account-enabled" type="checkbox" name="enabled" @if(!is_null($guestConstraint) && $guestConstraint->enabled) checked @endif value="1"/>
                                    <span></span>
                                </label>
                                <span class="help-block">Switch on/off the Guest Account</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Username</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-user"></span></span>
                                    <input value="{{Auth::user()->username}}-guest" readonly type="text" class="form-control"/>
                                </div>
                                <span class="help-block">Guest's username to connect to access point</span>
                            </div>
                        </div>

                        <div id="guest-passwords">
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Guest Password</label>
                                <div id="pwd-container" class="col-md-6 col-xs-12">
                                    <div id="passwordGuestGroup" class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                        <input  id="passwordGuest" name="password" type="password" class="form-control"/>
                                    </div>
                                    {{--<span class="help-block">Guest Password</span>--}}
                                    <div class="col-sm-6 col-sm-offset-0" style="padding-top: 5px;;margin-left:-10px;" >
                                        <div class="pwstrength_viewport_progress"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Re-type Password</label>
                                <div class="col-md-6 col-xs-12">
                                    <div id="passwordGuestConfirmationGroup" class="input-group">
                                        <span class="input-group-addon"><span class="fa fa-unlock-alt"></span></span>
                                        <input id="guestPassword_confirmation"  name="password_confirmation" type="password" class="form-control"/>
                                    </div>
                                    <span class="help-block">re-type Guest Password</span>
                                </div>
                            </div>
                        </div>



                    </div>
                    <div class="panel-footer">
                        <button name="save_guest" value="1" type="submit" id="guestSaveButton" class="btn btn-primary pull-right ">Save</button>
                    </div>
                </div>
            {!! Form::close() !!}

        </div>
    </div>

    @if(!is_null($guestConstraint) && $guestConstraint->enabled)
        <div class="row">
        <div class="col-md-12">

            {!! Form::model($guestConstraint,['method'=>'post','class'=>'form-horizontal']) !!}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Guest Account Constraints</strong></h3>
                    <ul class="panel-controls">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
                </div>

                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Total Guest Connection</label>
                        <div class="col-md-6 col-xs-12">

                            {!! Form::select('total_connection',$guest_connection_array,$guestConstraint->total_connection,['class'=>'form-control select']) !!}
                            <span class="help-block">Max number of Guest connect to access point at a time</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Enable Guest Daily Limit</label>
                        <div class="col-md-6 col-xs-12">
                            <label class="switch">
                                {!! Form::checkbox('daily_limit_enabled',1,null,['id'=>'checkbox-daily-limit-enabled']) !!}
                                <span></span>
                            </label>
                            <span class="help-block">Switch on/off Daily Limit</span>
                        </div>
                    </div>

                    <div id="guest-constraints-daily-enabled">

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Data Quota</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-external-link-square"></span></span>
                                    {!! Form::text('daily_data_quota',null,['class'=>'form-control']) !!}
                                </div>
                                <span class="help-block">Daily Data Quota in MegaBytes, put 0 for unlimited</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Hours Quota</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><span class="fa fa-external-link-square"></span></span>
                                    {!! Form::text('daily_hours_quota',null,['class'=>'form-control']) !!}
                                </div>
                                <span class="help-block">Daily Hours Quota, put 0 for unlimited</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Time From</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <div class="input-group bootstrap-timepicker">
                                        {!! Form::text('daily_time_from',null,['class'=>'form-control timepickerText']) !!}
                                        <span class="input-group-addon timepicker"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <span class="help-block">The Guest can only connect to Access Point within this time</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Daily Time To</label>
                            <div class="col-md-6 col-xs-12">
                                <div class="input-group">
                                    <div class="input-group bootstrap-timepicker ">
                                        {!! Form::text('daily_time_to',null,['class'=>'form-control timepickerText']) !!}
                                        <span class="input-group-addon timepicker"><span class="glyphicon glyphicon-time"></span></span>
                                    </div>
                                </div>
                                <span class="help-block">The Guest can only connect to Access Point within this time</span>
                            </div>
                        </div>

                    </div>




                </div>
                <div class="panel-footer">
                    <button name="update_constraint" value="1" type="submit" class="btn btn-primary pull-right">Update</button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
    @endif



@stop


@section('thisPageJs')

    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-timepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>


    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>


@stop

@section('customJs')

    <script type="text/javascript">
        $('.timepicker').timepicker().on('changeTime.timepicker', function(e) {
            $(this).parent().children('.timepickerText').val(e.time.value);
//            console.log('The time is ' + e.time.value);
//            console.log('The hour is ' + e.time.hours);
//            console.log('The minute is ' + e.time.minutes);
//            console.log('The meridian is ' + e.time.meridian);
        }).on('show.timepicker', function(e) {
            $(this).parent().children('.timepickerText').val(e.time.value);
        });

        var checkbox_daily_limit = $('#checkbox-daily-limit-enabled');
        var guest_constraint_daily_enabled = $('#guest-constraints-daily-enabled');
        if(checkbox_daily_limit.attr('checked') == undefined){
            guest_constraint_daily_enabled.hide();
        }

        checkbox_daily_limit.change(function() {
            console.log(checkbox_daily_limit.is(':checked'));
            if(checkbox_daily_limit.is(':checked')) {
                guest_constraint_daily_enabled.show();
                console.log('checked');
            }
            else{
                guest_constraint_daily_enabled.hide();
            }
        });

        var checkbox_guest_enabled = $('#guest-account-enabled');
        var guest_passwords = $('#guest-passwords');
        if(checkbox_guest_enabled.attr('checked') == undefined){
            guest_passwords.hide();
        }
        else{
            guest_passwords.show();
        }

        checkbox_guest_enabled.change(function() {
            console.log(checkbox_guest_enabled.is(':checked'));
            if(checkbox_guest_enabled.is(':checked')) {
                guest_passwords.show();
                console.log('checked');
            }
            else{
                guest_passwords.hide();
            }
        });




    </script>

@stop
