@extends('user.master')

@section('breadcrumb')
    <li><a href="#">Home</a></li>
    <li class="active">Dashboard</li>
@stop

@section('pageContentWrap')

        <!-- START WIDGETS -->
        <div class="row">


            @foreach($widgets as $widget)

                <div class="col-md-3">

                    <!-- START WIDGET SLIDER -->
                    <div class="widget widget-default widget-carousel">
                        <div class="owl-carousel" id="owl-example">
                            <div>
                                <div class="widget-title">{{$widget['title']}}</div>
                                <div class="widget-subtitle">{{$widget['within']}}</div>
                                <div class="widget-int">{{$widget['value']}}</div>
                            </div>
                        </div>
                        <div class="widget-controls">
                        </div>
                    </div>
                    <!-- END WIDGET SLIDER -->

                </div>

            @endforeach



            <div class="col-md-3">

                <!-- START WIDGET CLOCK -->
                <div class="widget widget-danger widget-padding-sm">
                    <div class="widget-big-int plugin-clock">00:00</div>
                    <div class="widget-subtitle plugin-date">Loading...</div>
                    <div class="widget-controls">
                    </div>

                </div>
                <!-- END WIDGET CLOCK -->

            </div>
        </div>
        <!-- END WIDGETS -->

        <div class="row">
            <div class="col-md-12">

                <!-- START PROJECTS BLOCK -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title-box">
                            <h3>Account</h3>
                            <span>Information</span>
                        </div>
                        <ul class="panel-controls" style="margin-top: 2px;">

                        </ul>
                    </div>
                    <div class="panel-body panel-body-table">

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th width="20%"></th>
                                    <th width="20%"></th>
                                    <th width="60%"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><strong>Wi-Fi Manager</strong></td>
                                    <td>{{$accountInfo['manager']}}</td>
                                    <td>
                                        {{--<div class="progress progress-small progress-striped active">--}}
                                            {{--<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%;">85%</div>--}}
                                        {{--</div>--}}
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Account Status</strong></td>
                                    <td><span class="label {{$accountInfo['status']['label']}}">{{$accountInfo['status']['text']}}</span></td>
                                    <td>

                                    </td>
                                </tr>

                                <tr>
                                    <td><strong>Wi-Fi Connection Status</strong></td>
                                    <td><span class="label {{$accountInfo['connection_status']['label']}}">{{$accountInfo['connection_status']['text']}}</span></td>
                                    <td>
                                    </td>
                                </tr>

                                <tr>
                                    <td><strong>Daily Connection Hour</strong></td>
                                    <td>{{$accountInfo['daily_connection_hour']}} @if($accountInfo['daily_data_limit_enabled'] ==0)<span class="label label-danger">Disabled</span>@endif</td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td><strong>Daily Data Quota</strong></td>
                                    <td>{{$accountInfo['daily_data_usage']['used']}}/{{$accountInfo['daily_data_usage']['limit']}}</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar {{$accountInfo['daily_data_usage']['progressBar_label']}}" role="progressbar"
                                                 aria-valuenow="{{$accountInfo['daily_data_usage']['progressBar_percentage']}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$accountInfo['daily_data_usage']['progressBar_percentage']}}%;">{{$accountInfo['daily_data_usage']['progressBar_percentage']}}%
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Daily Hours Quota</strong></td>
                                    <td>{{$accountInfo['daily_hours_usage']['used']}}/{{$accountInfo['daily_hours_usage']['limit']}} hours</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar {{$accountInfo['daily_hours_usage']['progressBar_label']}}" role="progressbar"
                                                 aria-valuenow="{{$accountInfo['daily_hours_usage']['progressBar_percentage']}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$accountInfo['daily_hours_usage']['progressBar_percentage']}}%;">{{$accountInfo['daily_hours_usage']['progressBar_percentage']}}%
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td><strong>Connection Limit</strong></td>
                                    <td>{{$accountInfo['connection_limit']['used']}}/{{$accountInfo['connection_limit']['limit']}}</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar {{$accountInfo['connection_limit']['progressBar_label']}}" role="progressbar"
                                                 aria-valuenow="{{$accountInfo['connection_limit']['progressBar_percentage']}}"
                                                 aria-valuemin="0" aria-valuemax="100"
                                                 style="width: {{$accountInfo['connection_limit']['progressBar_percentage']}}%;">{{$accountInfo['connection_limit']['progressBar_percentage']}}%
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- END PROJECTS BLOCK -->

            </div>
        </div>






@stop

@section('thisPageJs')
    <script type='text/javascript' src='{{ URL::asset('js/plugins/icheck/icheck.min.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/raphael-min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/morris/morris.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/rickshaw/d3.v3.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/rickshaw/rickshaw.min.js')}}"></script>
    <script type='text/javascript' src='{{ URL::asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}'></script>
    <script type='text/javascript' src='{{ URL::asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}'></script>
    <script type='text/javascript' src='{{ URL::asset('js/plugins/bootstrap/bootstrap-datepicker.js')}}'></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/owl/owl.carousel.min.js')}}"></script>

    <script type="text/javascript" src="{{ URL::asset('js/plugins/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins/daterangepicker/daterangepicker.js')}}"></script>

    {{--<script type="text/javascript" src="{{ URL::asset('js/demo_dashboard.js')}}"></script>--}}
@stop
