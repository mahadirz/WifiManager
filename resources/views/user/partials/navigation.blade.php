<ul class="x-navigation">
    <li class="xn-logo">
        <a href="{{URL::route('user.dashboard.index')}}">Wi-Fi Management System</a>
        <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
        <div class="profile">
            <div class="profile-image">
                <img width="100px" height="100px" id="avatar" src="{{ \App\Models\User::find(Auth::id())->getProfilePhotoUrl()}}" alt="User Avatar"/>
            </div>
            <div class="profile-data">
                <div class="profile-data-name">{{Auth::user()->getFullName()}}</div>
                <div class="profile-data-title">User's Panel</div>
            </div>
            <div class="profile-controls">
                <a href="{{URL::route('user.dashboard.getSettings')}}" class="profile-control-left"><span class="fa fa-gear"></span></a>
                @if($vars['enable_user_support'])
                    <a href="{{URL::route('user.support.getTickets','all')}}" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                @endif
            </div>
        </div>
    </li>

    <li class="xn-title">Navigation</li>

    <li class="active">
        <a href="{{URL::route('user.dashboard.index')}}"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>
    </li>



    @if($vars['enable_user_support'])
        <li class="xn-openable">
            <a href="#"><span class="fa fa-envelope"></span> Supports</a>
            <ul>
                <li>
                    <a id="menu_all_tickets" href="{{URL::route('user.support.getTickets','all')}}"><span class="fa fa-comments-o"></span> All Tickets</a>
                </li>
                <li>
                    <a id="menu_opened_tickets" href="{{URL::route('user.support.getTickets','opened')}}"><span class="fa fa-comment-o"></span> Opened Tickets</a>
                    @if($vars['openTicketCount'])
                        <div class="informer informer-warning">{{$vars['openTicketCount']}}</div>
                    @endif
                </li>
                <li><a href="{{URL::route('user.support.getTickets','closed')}}"><span class="fa fa-comment"></span> Closed Tickets</a></li>
                <li><a href="{{URL::route('user.support.getNewTicket')}}"><span class="fa fa-pencil"></span> Create Ticket</a></li>
            </ul>
        </li>
    @endif

    @if($vars['show_guest'])
        <li class="xn-openable">
            <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Guest Account</span></a>
            <ul>
                <li><a href="{{URL::route('user.guest.settings')}}"><span class="fa fa-gears"></span> Settings</a></li>
                <li><a href="{{URL::route('user.guest.view')}}"><span class="fa fa-user"></span> View Guest</a></li>
            </ul>
        </li>
    @endif

    @if($vars['show_guest'])
        <li><a href="{{URL::route('user.dashboard.getStatistics')}}"><span class="glyphicon glyphicon-stats"></span> Statistics</a></li>
    @endif



</ul>