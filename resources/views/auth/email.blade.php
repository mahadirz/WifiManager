<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>Password Reset - Homes and Enterprises Wi-Fi Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{ URL::asset('favicon.ico')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/theme-default.css')}}"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="login-container">

    @if (Session::has('message'))
        <div class="alert alert-{{Session::pull('alert')}} text-center">{{ Session::pull('message') }}</div>
    @endif

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div class="login-title"><strong>Reset Password</strong></div>
            <form action="{{URL::current()}}" class="form-horizontal" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <div class="col-md-12">
                        <input required type="text" name="username" class="form-control" placeholder="Username" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input required type="email" name="email" class="form-control" placeholder="Email" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <button onclick="location.href='{{URL::route('login.index')}}'" type="button"  class="btn btn-success btn-block">Back to Login</button>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-info btn-block">Email Password Reset</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; {{\Carbon\Carbon::now()->format("Y")}} Mahadir Network Enterprise
            </div>
            {{--<div class="pull-right">--}}
                {{--User's Panel--}}
            {{--</div>--}}
        </div>
    </div>

</div>

</body>
</html>