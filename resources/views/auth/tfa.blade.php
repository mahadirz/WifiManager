<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>2 Factor Auth - Homes and Enterprises Wi-Fi Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{ URL::asset('favicon.ico')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/theme-default.css')}}"/>
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="login-container">

    @if (Session::has('message'))
        <div class="alert alert-{{Session::pull('alert')}} text-center">{{ Session::pull('message') }}</div>
    @endif

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div align="center" class="login-title"><strong>Two-Factors Authentication</strong></div>
                {!! Form::open(array('class'=>'form-horizontal','method'=>'POST')) !!}
                <div class="form-group">

                    <div class="col-md-12">
                        {!! Form::text('token',null,array('pattern'=>'\d*','autofocus'=>'autofocus','class'=>'form-control','placeholder'=>'Token')) !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-12">
                        <button class="btn btn-info btn-block">Verify Token</button>
                    </div>
                </div>

            {!! Form::close() !!}
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; {{\Carbon\Carbon::now()->format("Y")}} Mahadir Network Enterprise
            </div>
        </div>
    </div>

</div>

</body>
</html>