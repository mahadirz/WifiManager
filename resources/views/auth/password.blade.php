<html>
<head>
    <title></title>
</head>
<body>
<div>
    Dear {{$username}},</div>
<div>
    &nbsp; &nbsp; &nbsp; &nbsp;</div>
<div>
    Someone had requested to reset your Wifi Management account password</div>
<div>
    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</div>
<div>
    If this was a mistake, just ignore this email and nothing will happen.</div>
<div>
    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</div>
<div>
    To reset your password, visit the following link:</div>
<div>
    &nbsp; &nbsp; &nbsp; &nbsp;</div>
<div>
    <a href="{{URL::route('login.getReset',$token) }}">{{URL::route('login.getReset',$token) }}</a></div>
<div>
    &nbsp; &nbsp; &nbsp; &nbsp;</div>
<div>
    Please take note that :</div>
<div>
    1. The above link is valid within 1 hour from the time of this e-mail and can only be used once.</div>
<div>
    2. If the above link is already expired (after 1 hour), you may request a new link.</div>
<div>
    3. Please do not reply to this e-mail since it&#39;s not monitored for incoming mail</div>
<div>
    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</div>
<div>
    Thank you.</div>
</body>
</html>
