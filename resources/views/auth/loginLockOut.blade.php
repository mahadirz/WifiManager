<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>User Login - Homes and Enterprises Wi-Fi Management System</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{ URL::asset('favicon.ico')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="{{ URL::asset('css/theme-default.css')}}"/>
    <!-- EOF CSS INCLUDE -->

    <style type="text/css">
        .counter {
            background: #2C2C2C;
            -moz-box-shadow:    inset 0 0 5px #000000;
            -webkit-box-shadow: inset 0 0 5px #000000;
            box-shadow:         inset 0 0 5px #000000;
            min-height: 150px;
            text-align: center;
        }

        .counter h3 {
            color: #E5E5E5;
            font-size: 14px;
            font-style: normal;
            font-variant: normal;
            font-weight: lighter;
            letter-spacing: 1px;
            padding-top: 20px;
            margin-bottom: 30px;
        }

        #countdown {
            color: #FFFFFF;
        }

        #countdown span {
            color: #E5E5E5;
            font-size: 26px;
            font-weight: normal;
            margin-left: 20px;
            margin-right: 20px;
            text-align: center;
        }
    </style>

</head>
<body>

<div class="login-container">

    <div class="alert alert-danger">Too many login attempts your IP address has been blocked!</div>

    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div class="counter">
                <h3>Please wait, before able to perform login:</h3>
                <div id="countdown">

                </div><!-- /#Countdown Div -->
            </div> <!-- /.Counter Div -->
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; {{\Carbon\Carbon::now()->format("Y")}} Mahadir Network Enterprise
            </div>
            {{--<div class="pull-right">--}}
                {{--User's Panel--}}
            {{--</div>--}}
        </div>
    </div>

</div>

<script type="text/javascript">
    // set the date we're counting down to
    var target_date = new Date('{{Session::get('lockDateTimeout',\Carbon\Carbon::now()->addMinute(1))}}').getTime();

    // variables for time units
    var days, hours, minutes, seconds;

    // get tag element
    var countdown = document.getElementById('countdown');

    // update the tag with id "countdown" every 1 second
    var timerId = setInterval(function () {

    // find the amount of "seconds" between now and target
    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;

    // do some time calculations
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    if(hours<=0 && minutes <=0 && seconds_left <= 0){
        hours = minutes = seconds = 0;
        clearInterval(timerId);
        window.location = '{{URL::route('login.index')}}';
    }

    if(hours<10)
        hours = '0'+hours.toString();

    if(minutes<10){
        minutes = "0"+ minutes.toString();
    }

    if(seconds<10)
        seconds = '0'+seconds.toString();

    // format countdown string + set tag value
    countdown.innerHTML = '<span class="minutes">'+hours+":"+
    minutes + ':' + seconds + '</span>';

    }, 1000);
</script>

</body>
</html>