var server = require('http').Server();

var io = require('socket.io')(server);

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('notification-channel');

redis.on('message',function(channel,message){
    message = JSON.parse(message);
    console.log("event Received");
    console.log(message);
    io.emit(channel + ':'+message.event,message.data);
});

//listen for client
server.listen(3000);

