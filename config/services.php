<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

    'paypal' => [
        'client_id' => env('Paypal_Client_ID'),
        'secret' => env('Paypal_Secret'),
        'mode' => env('Paypal_Mode','sandbox'),
        'endpoint' => ['sandbox'=>'https://api.sandbox.paypal.com','live'=>'https://api.paypal.com'],
        'connection_timeout' => 30,
        'log_enabled' => true,
        'log_filename' => storage_path('logs/paypal.log'),
        'log_level' =>'FINE',
    ],

    'manual' =>[
        'instructions' => '<p></p><strong>Bank in can be made to bank account</strong></p>
                                    <p>CIMB: 123213-4343-222</p>
                                    <p>Maybank: 433232-4333-2234</p>
                                <p>After payment is made, please send support about the payment details,
                                payment will be processed within 24 hours</p>'
    ],

];
