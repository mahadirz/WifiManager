<?php

use App\Models\Permission;
use App\Models\Profile;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \App\Models\User;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Model::unguard();

		$this->call('RadcheckTableSeeder');
		$this->command->info('Radcheck table seeded!');

		$this->call('NasTableSeeder');
		$this->command->info('Nas table seeded!');

		$this->call('TableUsers');
		$this->command->info('NasTableUsers seeded');
	}

}

class RadcheckTableSeeder extends Seeder {

	public function run()
	{
		DB::table('radcheck')->delete();

		DB::table('radcheck')->insert(
			['username' => 'mahadirz', 'attribute' => 'NT-Password',
			'op' => ':=','value' => Security::NTLMHash('zaq123')]
		);
	}

}

class NasTableSeeder extends Seeder {

	public function run()
	{
		DB::table('nas')->delete();

		DB::table('nas')->insert(
			['nasname' => '192.168.10.1', 'shortname' => 'myNAS',
				'type' => 'other','ports' => NULL,
			'secret'=>'mysecret','server'=>NULL,
			'community'=>NULL,'description'=>'RADIUS Client']
		);
	}

}

class TableUsers extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

        //create system user support
        $user = new User();
        $user->username = "system_user1";
        $user->firstName = "System";
        $user->email = "system@wifimanager.app";
        $user->save();

        //create system user support
        $user = new User();
        $user->username = "system_user2";
        $user->firstName = "Support";
        $user->email = "support@wifimanager.app";
        $user->save();

        //create system user billing
        $user = new User();
        $user->username = "system_user3";
        $user->firstName = "Billing";
        $user->email = "billing@wifimanager.app";
        $user->save();

        //create user level
        $user = new User();
        $user->username = "mahadirz";
        $user->firstName = "Mahadir";
        $user->lastName = "Ahmad";
        $user->email = "mahadir@madet.my";
        $user->password = bcrypt('zaq123');
        $user->save();
        $profile = new Profile();
        $profile->country = "Malaysia";
        $user->profile()->save($profile);

        //create admin
        $input = ['firstName'=>'Ali','lastName'=>'Ahmad','email'=>'admin@wifimanager.app'];
        $admin = User::create($input);
        $admin->username = "admin";
        $admin->password = bcrypt('zaq123');
        $admin->save();

        //create roles
        $adminRole = new Role();
        $adminRole->name         = 'administrator';
        $adminRole->display_name = 'System Administrator'; // optional
        $adminRole->description  = 'Highest privilege of user'; // optional
        $adminRole->save();

        $wifiManagerRole = new Role();
        $wifiManagerRole->name         = 'wifimanager';
        $wifiManagerRole->display_name = 'Wifi Manager'; // optional
        $wifiManagerRole->description  = 'Owner of NAS'; // optional
        $wifiManagerRole->save();

        $userRole = new Role();
        $userRole->name         = 'user';
        $userRole->display_name = 'End User'; // optional
        $userRole->description  = 'The user who can connect to NAS'; // optional
        $userRole->save();

        //attach role
        $user->attachRole($userRole);
        $admin->attachRole($adminRole);

        //create permissions
        $createPost = new Permission();
        $createPost->name         = 'Edit-Guest-Constraints';
        $createPost->display_name = 'Edit Guest Constraints';
        $createPost->description  = 'Ability to edit guest'; // optional
        $createPost->save();

	}

}
