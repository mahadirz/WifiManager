<?php

use Illuminate\Database\Seeder;
use \App\Models\Configuration;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ConfigurationsTableSeederTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('configurations')->delete();
        $input = [
            ['name'=>'currencyPrefix','value'=>'RM','descriptions'=>'prefix for currency'],
            ['name'=>'currencyPostfix','value'=>'MYR','descriptions'=>'postfix for currency'],
            ['name'=>'managerCanRegisterNas','value'=>'1','descriptions'=>'mode'],
            ['name'=>'monitorRadius','value'=>'1','descriptions'=>''],
            ['name'=>'monitorAutoStartRadius','value'=>'1','descriptions'=>''],
            ['name'=>'monitorDownSendEmail','value'=>'1','descriptions'=>''],
        ];
        Configuration::insert($input);
    }
}
