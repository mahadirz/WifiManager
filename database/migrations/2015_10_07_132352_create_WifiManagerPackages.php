<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWifiManagerPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('wifiManagerPackages', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('name');
            $table->string('descriptions');
            $table->integer('max_users')->unsigned()->default(5);
            $table->integer('max_connections_per_user')->unsigned()->default(2);
            $table->boolean('enable_user_support')->default(true);
            $table->boolean('enable_user_billing')->default(true);
            $table->integer('acctinterim')->unsigned()->default(10);
            $table->float('monthly_price')->default(0);
            $table->float('semi_annually_price')->default(0);
            $table->float('annually_price')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('wifiManagerPackages');
	}

}
