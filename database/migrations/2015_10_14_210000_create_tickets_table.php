<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('title',200);
            $table->integer('user_id_sender')->unsigned();
            $table->integer('user_id_receiver')->unsigned();
            $table->string('status',20)->default('open');
			$table->timestamps();

            $table->foreign('user_id_sender')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('user_id_receiver')
                ->references('id')->on('users')
                ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
