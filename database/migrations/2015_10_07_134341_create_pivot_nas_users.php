<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotNasUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nas_users', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned();
            $table->integer('nas_id')->unsigned();
            $table->string('mac_address')->default('00:11:22:33:44:57');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nas_users');
	}

}
