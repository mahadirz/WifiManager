<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPackages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('userPackages', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name')->unique();
            $table->integer('user_id')->unsigned(); //one manager has many packages
            $table->string('descriptions');
            $table->integer('data_limit')->default(0);
            $table->integer('max_connection_limit')->default(2);
            $table->integer('guest_connection_max')->default(2);
            $table->boolean('daily_limit_enabled')->default(false);
            $table->integer('daily_data_limit')->default(0);
            $table->integer('daily_hours_max')->default(0);
            $table->time('daily_time_from');
            $table->time('daily_time_to');
            $table->float('monthly_price')->default(0);
            $table->float('semi_annually_price')->default(0);
            $table->float('annually_price')->default(0);
			$table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('userPackages');
	}

}
