<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiusNas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nas', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('nasname',128);
            $table->string('shortname',32)->nullable();
            $table->string('type',30)->default('other');
            $table->integer('ports')->nullable();
            $table->string('secret',60)->default('secret');
            $table->string('server',64)->nullable();
            $table->string('community')->nullable();
            $table->string('description')->nullable()->default('RADIUS Client');
			$table->timestamps();

            //$table->primary('nasname');
            $table->index('nasname');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nas');
	}

}
