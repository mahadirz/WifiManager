<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiusRadacct extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('radacct', function(Blueprint $table)
		{
			$table->unsignedBigInteger('radacctid',true);
            $table->string('acctsessionid',64)->default('');
            $table->string('acctuniqueid',32)->default('');
            $table->string('username',64)->default('');
            $table->string('groupname',64)->default('');
            $table->string('realm',64)->nullable();
            $table->string('nasipaddress',15)->default('');
            $table->string('nasportid',15)->nullable();
            $table->string('nasporttype',32)->nullable();
            $table->dateTime('acctupdatetime')->nullable();
            $table->dateTime('acctstarttime')->nullable();
            $table->dateTime('acctstoptime')->nullable();
            $table->integer('acctinterval')->nullable();
            $table->integer('acctsessiontime')->nullable();
            $table->string('acctauthentic',32)->nullable();
            $table->string('connectinfo_start',50)->nullable();
            $table->string('connectinfo_stop',50)->nullable();
            $table->bigInteger('acctinputoctets')->nullable();
            $table->bigInteger('acctoutputoctets')->nullable();
            $table->string('calledstationid',50)->default('');
            $table->string('callingstationid',50)->default('');
            $table->string('acctterminatecause',32)->default('');
            $table->string('servicetype',32)->nullable();
            $table->string('framedprotocol',32)->nullable();
            $table->string('framedipaddress',15);
            $table->integer('acctstartdelay')->nullable();
            $table->integer('acctstopdelay')->nullable();
            $table->string('xascendsessionsvrkey',10)->nullable();

            //$table->primary('radacctid');
            $table->unique('acctuniqueid');
            $table->index('username');
            $table->index('framedipaddress');
            $table->index('acctsessionid');
            $table->index('acctsessiontime');
            $table->index('acctstarttime');
            $table->index('acctinterval');
            $table->index('acctstoptime');
            $table->index('nasipaddress');

            $table->engine = 'InnoDB';


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('radacct');
	}

}
