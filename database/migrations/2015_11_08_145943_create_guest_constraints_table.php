<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestConstraintsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('guest_constraints', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('total_connection');
            $table->boolean('enabled')->default(false);
            $table->string('username');
            $table->boolean('daily_limit_enabled')->default(false);
            $table->integer('daily_data_quota')->default(512);
            $table->float('daily_hours_quota')->default(5);
            $table->time('daily_time_from')->default('00:00:00');
            $table->time('daily_time_to')->default('23:59:00');
			$table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('guest_constraints');
	}

}
