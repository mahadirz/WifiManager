<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiusRadpostauth extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('radpostauth', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('username',64)->default('');
            $table->string('pass',64)->default('');
            $table->string('reply',32)->default('');
            $table->timestamp('authdate');

            //$table->primary('id');
            $table->engine = 'INNODB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('radpostauth');
	}

}
