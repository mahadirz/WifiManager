<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiusRadgroupreply extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('radgroupreply', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('groupname',64)->default('');
            $table->string('attribute',64)->default('');
            $table->char('op',2)->default('==');
            $table->string('value',253)->default('');
            $table->timestamps();

            //$table->primary('id');
            $table->index('groupname');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('radgroupreply');
	}

}
