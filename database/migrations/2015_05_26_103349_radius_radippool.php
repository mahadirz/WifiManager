<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiusRadippool extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('radippool', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('pool_name',30);
            $table->string('framedipaddress',15);
            $table->string('nasipaddress',15);
            $table->string('calledstationid',30);
            $table->string('callingstationid',30);
            $table->date('expiry_time')->nullable();
            $table->string('username',64);
            $table->string('pool_key',30);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('radippool');
	}

}
