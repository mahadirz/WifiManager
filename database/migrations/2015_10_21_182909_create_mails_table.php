<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mails', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('sender_user_id')->unsigned()->nullable();
            $table->integer('receiver_user_id')->unsigned()->nullable();
            $table->string('email_sender');
            $table->string('email_receiver');
            $table->string('subject');
            $table->string('cc');
            $table->text('body');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mails');
	}

}
