<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RadiusRadusergroup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('radusergroup', function(Blueprint $table)
		{
            $table->string('username',64)->default('');
            $table->string('groupname',64)->default('');
            $table->integer('priority')->default(1);
			$table->timestamps();

            $table->index('username');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('radusergroup');
	}

}
