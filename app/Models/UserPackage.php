<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model {

	protected $table = "userPackages";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'descriptions','data_limit','max_connection_limit','guest_connection_max',
        'daily_limit_enabled','daily_data_limit','daily_hours_max','daily_time_from','daily_time_to',
        'monthly_price','semi_annually_price','annually_price'
    ];

    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function subscription()
    {
        return $this->morphOne('App\Models\Subscription','subscribable');
    }

    /**
     * This package belong to which user (manager)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    /**
     * @return bool
     */
    public function isFree()
    {
        if($this->monthly_price == 0 && $this->semi_annually_price == 0 && $this->annually_price ==0)
        {
            return true;
        }
        return false;
    }

    /**
     * @param $value
     * @return string
     */
    public function getDailyTimeFromAttribute($value)
    {
        return Carbon::parse($value)->format('h:i A');
    }

    /**
     * @param $value
     */
    public function setDailyTimeFromAttribute($value)
    {
        $this->attributes['daily_time_from'] = Carbon::parse($value)->format('H:i:s');
    }

    /**
     * @param $value
     * @return string
     */
    public function getDailyTimeToAttribute($value)
    {
        return Carbon::parse($value)->format('h:i A');
    }

    /**
     * @param $value
     */
    public function setDailyTimeToAttribute($value)
    {
        $this->attributes['daily_time_to'] = Carbon::parse($value)->format('H:i:s');;
    }

}
