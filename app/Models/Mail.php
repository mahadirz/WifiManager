<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model {

	protected $table = "mails";

    protected $fillable = ['email_sender','email_receiver','subject','cc','body'];

}
