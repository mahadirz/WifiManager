<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Nas
 *
 * @property integer $id
 * @property string $nasname
 * @property string $shortname
 * @property string $type
 * @property integer $ports
 * @property string $secret
 * @property string $server
 * @property string $community
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereNasname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereShortname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas wherePorts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereSecret($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereServer($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereCommunity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Nas whereUpdatedAt($value)
 */
class Nas extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nasname','shortname','secret'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\Models\User', 'nas_users','nas_id', 'user_id');
    }

    /**
     * Getter
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(env('DATE_FORMAT','d/m/Y h:i A'));
    }
}
