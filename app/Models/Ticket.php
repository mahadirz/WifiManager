<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Ticket
 *
 * @property integer $id
 * @property string $title
 * @property integer $user_id_sender
 * @property integer $user_id_receiver
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\User $senderUser
 * @property-read \App\Models\User $ReceiverUser
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $message
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereUserIdSender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereUserIdReceiver($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Ticket whereUpdatedAt($value)
 */
class Ticket extends Model {

	protected $table = "tickets";

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function senderUser()
    {
        return $this->belongsTo('App\Models\User', 'user_id_sender','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ReceiverUser()
    {
        return $this->belongsTo('App\Models\User', 'user_id_receiver','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function message()
    {
        return $this->hasMany('App\Models\Message','ticket_id','id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        $parsed = Carbon::parse($value);
        if(Carbon::now()->format('dMY') == $parsed->format('dMY'))
            return $parsed->format('h:ia');
        else if(Carbon::now()->format('Y') == $parsed->format('Y'))
            return $parsed->format('d M  h:ia');
        else
        return $parsed->format('d M Y h:ia');
    }

}
