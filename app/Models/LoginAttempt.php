<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\LoginAttempt
 *
 * @property integer $id
 * @property string $ip
 * @property integer $attempts
 * @property string $username
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LoginAttempt whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LoginAttempt whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LoginAttempt whereAttempts($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LoginAttempt whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LoginAttempt whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LoginAttempt whereUpdatedAt($value)
 */
class LoginAttempt extends Model {

	protected $table = "login_attempts";

}
