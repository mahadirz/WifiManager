<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Message
 *
 * @property integer $id
 * @property integer $ticket_id
 * @property integer $user_id
 * @property string $body
 * @property boolean $read
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\Ticket $ticket
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereTicketId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereRead($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Message whereUpdatedAt($value)
 */
class Message extends Model {

	protected $table = "messages";

    protected $touches = ['ticket'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo('App\Models\Ticket','ticket_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    public function getCreatedAtAttribute($value)
    {
        $parsed = Carbon::parse($value);
        if(Carbon::now()->format('dMY') == $parsed->format('dMY'))
            return $parsed->format('h:ia');
        else if(Carbon::now()->format('Y') == $parsed->format('Y'))
            return $parsed->format('d M  h:ia');
        else
            return $parsed->format('d M Y h:ia');
    }

    /**
     * Set message to read
     * return $this
     */
    public function setRead()
    {
        $this->read = true;
        $this->save();
        return $this;
    }

}
