<?php namespace App\Models;

use Carbon\Carbon;
use Config;
use Crypt;
use DB;
use Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\Collection;
use Storage;
use URL;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * App\Models\User
 *
 * @property integer $id
 * @property string $firstName
 * @property string $lastName
 * @property string $username
 * @property string $street1
 * @property string $street2
 * @property string $country
 * @property string $state
 * @property string $zipCode
 * @property string $city
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.role')[] $roles
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereStreet1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereStreet2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereZipCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\User whereUpdatedAt($value)
 * @property-read \App\Models\Profile $profile
 * @property string $first_name
 * @property string $last_name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WifiManagerPackages[] $wifiManagerPackage
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Nas[] $nas
 * @property-read \App\Models\Google2fa $google2fa
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ticket[] $ticketCreated
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Ticket[] $ticketReceived
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Message[] $message
 */
class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	use EntrustUserTrait; // add this trait to your user model

    const PHOTO_LOCATION = "public/profiles/photos/";
    const PHOTO_EXT = ".png";

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name','last_name'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile','user_id','id');
    }

    /**
     * This is relation for manager own user package
     * and has more than one of it
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPackage()
    {
        return $this->hasMany('App\Models\UserPackage','user_id','id');
    }

    /**
     * This is relation for user can set constraint to guest
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function guestConstraint()
    {
        return $this->hasOne('App\Models\GuestConstraint','user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function google2fa()
    {
        return $this->hasOne('App\Models\Google2fa','user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticketCreated()
    {
        return $this->hasMany('App\Models\Ticket','user_id_sender','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ticketReceived()
    {
        return $this->hasMany('App\Models\Ticket','user_id_receiver','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function message()
    {
        return $this->hasMany('App\Models\Message','user_id','id');
    }

    /**
     * @param null $user_id
     * @param null $manager_id
     * @return $this
     */
    public function assignManager($user_id=null,$manager_id=null)
    {
        $user_id = is_null($user_id)?$this->id:$user_id;
        $manager_id = is_null($manager_id)?$this->id:$manager_id;
        if(is_null(DB::table('manager')->select('user_id')->where('user_id', $user_id)->first())){
            //insert
            DB::table('manager')->insert(
                ['user_id' => $user_id, 'manager_id' => $manager_id]
            );
        }
        else{
            //update
            DB::table('manager')
                ->where('user_id', $user_id)
                ->update(['manager_id' => $manager_id]);
        }
        return $this;

    }

    /**
     * Get the user that this manager manage
     * @param int $paginate
     * @param string $search
     * @return Collection
     */
    public function manage($paginate=10,$search=null)
    {
        if(is_null($search)){
            $paginated = DB::table('manager')->select('user_id')->where('manager_id', $this->id)->paginate($paginate);
        }
        else{
            $paginated = DB::table('manager')->select('user_id')->join('users','users.id','=','manager.user_id')
                ->where('manager_id', $this->id)->where(function ($q)
                use ($search) {
                    $q->where('first_name', 'like', '%' . $search . '%')
                        ->orWhere('email', 'like', '%' . $search . '%')
                        ->orWhere('username', 'like', '%' . $search . '%');

                })->paginate($paginate);
        }

        $collection = new Collection();
        $collection->put('paginator',$paginated);
        $userCollection = new Collection();
        foreach($paginated->items() as $users_id){
            $userCollection->push(User::find($users_id->user_id));
        }
        $collection->put('users',$userCollection);
        return $collection;
    }

    /**
     * Get the manager of current user
     * @return \Illuminate\Support\Collection|null|static
     */
    public function manager()
    {
        $manager_id = DB::table('manager')->where('user_id', $this->id)->pluck('manager_id');
        return $this->find($manager_id);
        /*
        return $this->belongsToMany('App\Models\User', 'user_wifiManagerPackages', 'user_id','wifiManagerPackages_id')
            ->withPivot('subscription_ends_at','subscription_active')->withTimestamps();
        */
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subscription()
    {
        return $this->hasOne('App\Models\Subscription','user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function invoice()
    {
        return $this->hasMany('App\Models\Invoice','user_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function nas()
    {
        return $this->belongsToMany('App\Models\Nas', 'nas_users', 'user_id','nas_id');
    }

    /**
     * Accessor
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(env('DATE_FORMAT','d/m/Y H:i:s'));
    }

    /**
     * Accessor
     * @param $value
     * @return string
     */
    public function getUpdatedAtAtAttribute($value)
    {
        return Carbon::parse($value)->format(env('DATE_FORMAT','d/m/Y H:i:s'));
    }

    /**
     * Generate sha256 hash of file name
     * @return string
     */
    public function getProfilePhotoId()
    {
        $id = $this->id;
        return hash_hmac('sha256',$id,Config::get('app.key'));
    }

    /**
     * @return string
     */
    public function getProfilePhotoUrl()
    {
        $photo_name = $this->getProfilePhotoId().User::PHOTO_EXT;
        if(Storage::disk('public')->exists('profiles/photos/'.$photo_name)){
            $photo_path = 'profiles/photos/'.$photo_name;
            return URL::asset($photo_path);
        }
        return URL::asset('profiles/default/default-avatar.png');
    }

    /**
     * @return mixed|string
     */
    public function getSignature()
    {
        if(!is_null($this->profile->signature))
            return $this->profile->signature;
        else
            return "";
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name. ' '.$this->last_name;
    }



}
