<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Profile
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $street1
 * @property string $street2
 * @property string $country
 * @property string $state
 * @property string $zipCode
 * @property string $city
 * @property string $mobilePhone
 * @property string $landPhone
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereStreet1($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereStreet2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereZipCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereMobilePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereLandPhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Profile whereUpdatedAt($value)
 * @property string $zip_code
 * @property string $mobile_phone
 * @property string $land_phone
 */
class Profile extends Model {


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['street1', 'country','state','zip_code','city','mobile_phone','land_phone'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

}
