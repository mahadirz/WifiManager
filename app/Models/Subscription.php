<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model {

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function subscribable()
    {
        return $this->morphTo();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    /**
     * Set fields that is carbon date
     * @return array
     */
    public function getDates()
    {
        return ['created_at','updated_at','ends_at'];
    }

    /**
     * @param $value
     * @return string
     */
    public function getEndsAtAttribute($value)
    {
        return Carbon::parse($value)->format('d M Y h:i a');
    }

}
