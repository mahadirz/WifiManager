<?php namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\WifiManagerPackages
 *
 * @property integer $id
 * @property string $name
 * @property string $descriptions
 * @property integer $maxUsers
 * @property integer $maxConnectionsPerUser
 * @property boolean $enableUserSupport
 * @property boolean $enableUserBilling
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $acctInterim
 * @property float $monthlyPrice
 * @property float $semiAnnuallyPrice
 * @property float $annuallyPrice
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereDescriptions($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereMaxUsers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereMaxConnectionsPerUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereEnableUserSupport($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereEnableUserBilling($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereAcctInterim($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereMonthlyPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereSemiAnnuallyPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\WifiManagerPackages whereAnnuallyPrice($value)
 * @property integer $max_users
 * @property integer $max_connections_per_user
 * @property boolean $enable_user_support
 * @property boolean $enable_user_billing
 * @property integer $acctinterim
 * @property float $monthly_price
 * @property float $semi_annually_price
 * @property float $annually_price
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $wifiManager
 */
class WifiManagerPackages extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wifiManagerPackages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'descriptions','max_users','max_connections_per_user','enable_user_support',
        'enable_user_billing','acctinterim','monthly_price','semi_annually_price','annually_price'
    ];

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * Accessor
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(env('DATE_FORMAT','d/m/Y H:i:s'));
    }

    /**
     * Accessor
     * @param $value
     * @return float
     */
    public function getMonthlyPriceAttribute($value)
    {
        return number_format($value,2);
    }

    /**
     * Accessor
     * @param $value
     * @return float
     */
    public function getSemiAnnuallyPriceAttribute($value)
    {
        return number_format($value,2);
    }

    /**
     * Accessor
     * @param $value
     * @return float
     */
    public function getAnnuallyPriceAttribute($value)
    {
        return number_format($value,2);
    }


    /**
     * Accessor
     * @param $value
     * @return string
     */
    public function getEnableUserSupportAttribute($value)
    {
        return $value==1?"checked":"";
    }

    /**
     * Accessor
     * @param $value
     * @return string
     */
    public function getEnableUserBillingAttribute($value)
    {
        return $value==1?"checked":"";
    }

    /**
     * Mutators
     * @param $value
     */
    public function setEnableUserSupportAttribute($value)
    {
        if($value)
            $this->attributes['enable_user_support'] = true ;
        else
            $this->attributes['enable_user_support'] = false ;
    }

    /**
     * Mutators
     * @param $value
     */
    public function setEnableUserBillingAttribute($value)
    {
        if($value)
            $this->attributes['enable_user_billing'] = true ;
        else
            $this->attributes['enable_user_billing'] = false ;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    public function wifiManager()
//    {
//        return $this->belongsToMany('App\Models\User', 'user_wifiManagerPackages', 'wifiManagerPackages_id', 'user_id');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function subscription()
    {
        return $this->morphOne('App\Models\Subscription','subscribable');
    }

    /**
     * @return bool
     */
    public function isFree()
    {
        if($this->monthly_price == 0 && $this->semi_annually_price == 0 && $this->annually_price ==0)
        {
            return true;
        }
        return false;
    }

}
