<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/3/2015
 * Time: 1:50 PM
 */

namespace App\Models;

use Eloquent;

/**
 * App\Models\Radcheck
 *
 * @property integer $id
 * @property string $username
 * @property string $attribute
 * @property string $op
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereAttribute($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereOp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Radcheck whereUpdatedAt($value)
 */
class Radcheck extends Eloquent{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'radcheck';

    protected $fillable = ['attribute','op','value','username'];

}