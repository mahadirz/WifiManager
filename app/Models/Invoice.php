<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function packageable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d M Y h:i a');
    }

    /**
     * @param $value
     * @return float
     */
    public function getPaidAmountAttribute($value)
    {
        return number_format($value,2);
    }

}
