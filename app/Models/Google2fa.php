<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Google2fa
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $secretkey
 * @property boolean $enabled
 * @property integer $backupcode
 * @property integer $backupcode2
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereSecretkey($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereEnabled($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereBackupcode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereBackupcode2($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Google2fa whereUpdatedAt($value)
 */
class Google2fa extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'google2fas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['secretkey','backupcode','backupcode2'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['secretkey', 'backupcode','backupcode2'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }


}
