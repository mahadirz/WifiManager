<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Configuration
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $descriptions
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereDescriptions($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereValue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Configuration whereUpdatedAt($value)
 */
class Configuration extends Model {

	protected $table = "configurations";

    protected $fillable = ['name','descriptions','value'];

    /**
     * @param array $keyValuePair
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function updateKeyValue(array $keyValuePair)
    {
        foreach($keyValuePair as $k=>$v)
        {
            $record = parent::query()->where('name',$k);
            $type = $record->get(['type'])->first();
            $value = $v;
            switch($type['type']){
                case "int":
                    $v =strtolower($v);
                    if($v=="true") $v=1;
                    if($v =="false") $v=0;
                    $value = intval($v);
                    break;
                case "float":
                    $value = floatval($v);
                    break;
                case "datetime":
                    $value = Carbon::parse($v);

            }
            $record->update(['value'=>$value]);
        }

        return parent::query();
    }

    /**
     * cast example ['check'=>1,'value'=>'checked','otherwise'=>'']
     * @param array $keys
     * @param null $cast
     * @return Collection
     */
    public static function getKeyValues(array $keys,$cast=null)
    {
        if(empty($keys))
            $collections = parent::query()->get();
        else
            $collections = parent::query()->whereIn('name', $keys)->get();
        $keypair = new Collection();
        $collections->each(function($items) use ($keypair,$cast){
            $value = $items['value'];
            if($items['type'] == "int")
                $value = intval($value);
            else if($items['type'] == "float")
                $value = floatval($value);
            else if($items['type'] == "datetime")
                $value = Carbon::parse($value);
            if(!is_null($cast) && $cast['check']===$value){
                if($cast['check']==$value)
                    $value = $cast['value'];
                else
                    $value = $cast['otherwise'];
            }
            $keypair->put($items['name'],$value);
        });
        return $keypair;
    }

}
