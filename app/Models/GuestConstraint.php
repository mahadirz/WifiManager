<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class GuestConstraint extends Model {

	protected $table = "guest_constraints";
    protected $fillable = ['total_connection','daily_limit_enabled','daily_data_quota','daily_hours_quota','daily_time_from','daily_time_to'];

    /**
     * @param $value
     * @return string
     */
    public function getDailyTimeFromAttribute($value)
    {
        return Carbon::parse($value)->format('h:i A');
    }

    /**
     * @param $value
     * @return string
     */
    public function getDailyTimeToAttribute($value)
    {
        return Carbon::parse($value)->format('h:i A');
    }

    /** mutator */

    /**
     * @param $value
     */
    public function setDailyTimeToAttribute($value)
    {
        $this->attributes['daily_time_to'] = Carbon::parse($value)->format('H:i:s');;
    }

    /**
     * @param $value
     */
    public function setDailyTimeFromAttribute($value)
    {
        $this->attributes['daily_time_from'] = Carbon::parse($value)->format('H:i:s');
    }


    /**
     * The constraint belong to which end user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id','user');
    }

}
