<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Storage;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
        'App\Console\Commands\MonitorRadius',
        'App\Console\Commands\TestEmailCommand',
        'App\Console\Commands\GenerateRadAcctDummy',
        'App\Console\Commands\CleanRadStaleSessions',
        'App\Console\Commands\DisconnectWirelessClient',
        'App\Console\Commands\QuotaCheck',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{

        $schedule->command('test:clean-radclean')
				 ->daily();

        //$schedule->command('monitor-radius')->everyFiveMinutes();
        $schedule->command('monitor-radius')->cron("* * * * *");

        //run every one minute
        $schedule->command('test:quota-check')->cron("* * * * *");

//        $schedule->call(function()
//        {
//            // Do some task...
//            if (Storage::exists('scheduler.log'))
//            {
//                Storage::append('scheduler.log', 'Appended Text '.\Carbon\Carbon::now());
//            }
//            else
//                Storage::put('scheduler.log', 'Put Text '.\Carbon\Carbon::now());
//
//        })->cron('* * * * *');
	}

}
