<?php namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class GenerateRadAcctDummy extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:radacct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Radius accounting dummy helpers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return string
     */
    public function genRandomWLZ($min,$max)
    {
        $val = rand($min,$max);
        $val = $val < 10 ? "0" . $val : "" . $val;
        return $val;
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $cmd =  $this->argument('cmd');
        $username =  $this->argument('username');
        if($cmd=="dummy"){
            for($i=28;$i>1;$i--){
                $acctsessiontime = rand(1,86400);
                $time = Carbon::now()->subDay($i)->format("Y-m-d ").$this->genRandomWLZ(0,23).":".$this->genRandomWLZ(0,59).":".$this->genRandomWLZ(0,59);
                $acctstarttime = Carbon::parse($time)->subDay($i)->format("Y-m-d H:i:s");
                $acctupdatetime = Carbon::parse($time)->subDay($i)->addSecond($acctsessiontime)->format("Y-m-d H:i:s");
                DB::table('radacct')->insert(
                    [
                        'acctsessionid' => uniqid(),
                        'acctuniqueid' => uniqid(),
                        'username' => $username,
                        'groupname' => "",
                        'realm' => "",
                        'nasipaddress' => "10.0.2.2",
                        'nasportid' => 1,
                        'nasporttype' => "Wireless-802.22",
                        'acctupdatetime' => $acctupdatetime,
                        'acctstarttime' => $acctstarttime,
                        'acctstoptime' => $acctupdatetime,
                        'acctinterval' => 60,
                        'acctsessiontime' => $acctsessiontime,
                        'acctauthentic' => "RADIUS",
                        'connectinfo_start' => "CONNECT 54Mbps 802.11g",
                        'connectinfo_stop' => "CONNECT 54Mbps 802.11g",
                        'acctinputoctets' => rand(100,100000000),
                        'acctoutputoctets' => rand(100,100000000),
                        'calledstationid' => "90-F6-52-BE-6A-69:Final Year Project WPA-EAP",
                        'callingstationid' => "44-00-10-57-2A-6A",
                        'acctterminatecause' => "User-Request",
                        'servicetype' => "",
                        'framedprotocol' => "",
                        'framedipaddress' => "",
                        'acctstartdelay' => "NULL",
                        'acctstopdelay' => "NULL",
                        'xascendsessionsvrkey' => "NULL",
                    ]
                );
            }
            echo "Dummy radacct data for $username has been generated!\n";
        }
        else if($cmd=="clear"){
            if($username=="all"){
                DB::table('radacct')->delete();
                DB::statement('ALTER TABLE  `radacct` AUTO_INCREMENT =1');
                echo "Table radacct cleared!\n";
            }
            else{
                DB::table('radacct')->where('username', $username)->delete();
                echo "Table radacct cleared for $username\n";
            }

        }


    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('cmd', InputArgument::REQUIRED, 'dummy'),
            array('username', InputArgument::REQUIRED, 'admin'),
        );
    }

}
