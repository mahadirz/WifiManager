<?php namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class CleanRadStaleSessions extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:clean-radclean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean Radaact stale sessions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $interval =  $this->argument('interval');
        if($interval){
            //clean the session when the difference between acctstarttime and acctupdatetime
            //less than interval
            $sql = "DELETE FROM radacct WHERE AcctStopTime IS NULL AND (ABS( TIME_TO_SEC( TIMEDIFF( acctupdatetime, AcctStartTime ) ) ) /60) < $interval ";
            $total = DB::delete($sql);
            echo $total." Session(s) with interval $interval has been cleared!\n\n";
            return;
        }
        //clean the session that have existed more than 24 hours
        $sql = "DELETE FROM radacct WHERE AcctStopTime IS NULL AND AcctStartTime + INTERVAL 24 Hour < NOW()";
        $total = DB::delete($sql);
        echo $total." Session(s) that existed more than 24 hours has been cleared!\n\n";
        return;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //the interval in minutes to clean from
            array('interval', InputArgument::OPTIONAL, 1),
        );
    }

}
