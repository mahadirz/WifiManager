<?php namespace App\Console\Commands;

use App\Commands\Emailer;
use Illuminate\Console\Command;

use App\Models\Configuration;
use App\Models\Role;
use App\Models\User;
use Bus;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use SSH;

class MonitorRadius extends Command implements SelfHandling {


    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'monitor-radius';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for RADIUS service';

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		//get config
        $configs =$this->getConfig();
        //only if monitor is enabled
        if($configs->get('monitorRadius')){
            if($this->isRadiusServiceRunning()){
                //update the radiusStopped to false
                Configuration::updateKeyValue(["radiusStopped"=>0]);
                var_dump("radius ok");
            }
            else{
                //only if auto start enabled
                if($configs->get('monitorAutoStartRadius')){
                    $this->forceStartRADIUSService();
                    echo "Restarted\n";
                }
                else{
                    //only when email notification enabled and send email has not been sent yet
                    if($configs->get('monitorDownSendEmail') && !$configs->get('radiusStopped')){
                        //send email if send email enabled
                        $sender = User::whereUsername('system_user2')->first();
                        $receiver = Role::with('users')->where('name','administrator')->first()->users()->first();
                        $emailer = new Emailer($sender,
                            $receiver,
                            'RADIUS Service Notification',
                            ''
                        );
                        $emailer->view = 'emails.radius';
                        Bus::dispatch($emailer);
                        //update the radiusStopped to true
                        Configuration::updateKeyValue(["radiusStopped"=>1]);
                        echo "Email dispatched!\n";
                    }
                }
            }
        }
        //update last check
        Configuration::updateKeyValue(['monitorLastCheck' => Carbon::now()]);
	}


    /**
     * return boolean
     */
    protected function isRadiusServiceRunning()
    {
        $output = "";
        SSH::run(['ps -ef |grep -v grep |grep -cw freeradius'], function($line)
        use (&$output)
        {
            $output = trim($line);
        });

        if(intval($output)>0){
            var_dump("RADIUS OK");
            return true;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getConfig()
    {
        $conf = Configuration::getKeyValues(['monitorDownSendEmail','monitorAutoStartRadius','monitorRadius','radiusStopped']);
        return $conf ;
    }

    protected function forceStartRADIUSService()
    {
        $cmd = ['service freeradius stop',
            'mkdir -p /var/run/radiusd/',
            'touch  /var/run/radiusd/radiusd.pid',
            'service freeradius start'];
        $output = "";
        SSH::run($cmd, function($line)
        use (&$output)
        {
            $output = trim($line);
        });
    }



}
