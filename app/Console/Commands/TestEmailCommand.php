<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TestEmailCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending email via command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $from =  $this->argument('from');
        $to =  $this->argument('to');
        $subject =  $this->argument('subject');
        $content =  ['body'=>$this->argument('content')];


        Mail::send(['html' => 'emails.general'],$content, function($message)
        use ($from,$to,$subject)
        {
            $message->from($from);
            $message->subject($subject);

            $message->to($to);

        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('from', InputArgument::REQUIRED, 'admin@wifimanager.app'),
            array('to', InputArgument::REQUIRED, 'user@wifimanager.app'),
            array('subject', InputArgument::REQUIRED, 'Mail by Console'),
            array('content', InputArgument::REQUIRED, 'Hello from Admin'),
        );
    }

}
