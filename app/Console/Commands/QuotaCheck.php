<?php namespace App\Console\Commands;

use App\Models\GuestConstraint;
use App\Models\User;
use DB;
use Event;
use Illuminate\Console\Command;
use SSH;
use Symfony\Component\Console\Input\InputArgument;
use Radius;

class QuotaCheck extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:quota-check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Usage quota';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //select all active connections
        $sql = "SELECT * FROM radacct WHERE AcctStopTime IS NULL";
        $result = DB::select($sql);

        foreach($result as $r){
            //attempt to get the user details
            $user = User::where('username',$r->username)->first();
            if(!is_null($user)){
                //user exist
                $package = is_null($user->subscription)?null:$user->subscription->subscribable;
                if(!is_null($package)){
                    if($package->daily_limit_enabled && $package->daily_data_limit!=0){
                        $limit_bytes = intval($package->daily_data_limit) * 1048576;
                        $current_usage_bytes = Radius::getDailyDataUsage($user->username);
                        if($current_usage_bytes > $limit_bytes){
                            //user exceeds the quota
                            echo "Quota for user ".$user->username." (".Radius::Bytes_converter($current_usage_bytes).") exceeds ".$package->daily_data_limit."MB \n";
                            //dispatch event
                            Event::fire(new \App\Events\UserHasBeenBlockedToNas($user->username));
                        }
                        else{
                            echo "Quota for user ".$user->username." ".Radius::Bytes_converter($current_usage_bytes)."/".$package->daily_data_limit."MB \n";
                        }
                    }
                    else{
                        echo "Daily limit disabled or unlimited quota\n";
                    }
                }

            }
            else{
                //maybe guest
                $username_owner = str_replace("-guest","",$r->username);
                $user = User::where("username",$username_owner)->first();
                if(!is_null($user)){
                    $package = $user->guestConstraint;
                    if(!is_null($package)){
                        if($package->daily_limit_enabled && $package->daily_data_quota!=0){
                            $limit_bytes = intval($package->daily_data_quota) * 1048576;
                            $current_usage_bytes = Radius::getDailyDataUsage($user->username."-guest");
                            if($current_usage_bytes > $limit_bytes){
                                //user exceeds the quota
                                echo "Quota for user ".$user->username."-guest (".Radius::Bytes_converter($current_usage_bytes).") exceeds ".$package->daily_data_quota."MB \n";
                                //dispatch event
                                Event::fire(new \App\Events\UserHasBeenBlockedToNas($user->username."-guest"));
                            }
                            else{
                                echo "Quota for user ".$user->username."-guest ".Radius::Bytes_converter($current_usage_bytes)."/".$package->daily_data_quota."MB \n";
                            }
                        }
                        else{
                            echo "Daily limit disabled or unlimited quota\n";
                        }
                    }
                }
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
        );
    }

}
