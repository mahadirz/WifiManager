<?php namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;
use SSH;
use Symfony\Component\Console\Input\InputArgument;
use Radius;

class DisconnectWirelessClient extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'test:disconnect-client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Disconnect wireless client';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $mac_address =  $this->argument('mac_address');
        //convert the mac to colon
        $mac_address =  Radius::convertMacAddress($mac_address,":");
        $output = "";
        $cmd = "hostapd_cli -i wlan0 -p /var/run/hostapd-phy0 deauthenticate $mac_address";
        echo "Command:".$cmd."\n";
        SSH::into('router')->run([$cmd], function($line)
        use (&$output)
        {
            $output = trim($line);
        });
        echo $output."\n";
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            //the interval in minutes to clean from
            array('mac_address', InputArgument::REQUIRED, 1),
        );
    }

}
