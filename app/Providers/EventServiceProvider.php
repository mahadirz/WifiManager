<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
        'App\Events\UserPasswordWasChanged' => [
            'App\Handlers\Events\UpdateRadCheck',
        ],
        'App\Events\UserWasRegistered' => [
            'App\Handlers\Events\RegisterRadCheck',
        ],
        'App\Events\MessageWasCreated' => [
            'App\Handlers\Events\NotifyReceiver',
        ],
        'App\Events\UserHasBeenBlockedToNas' => [
            'App\Handlers\Events\DisconnectWirelessClient',
        ],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
