<?php namespace App\Providers;

use Carbon\Carbon;
use JavaScript;
use Support;
use App\Models\Ticket;
use Auth;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->composeLeftNavigation();
        $this->composeManagerNavigation();
        $this->composeUserNavigation();
    }

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

    public function composeLeftNavigation()
    {
        view()->composer('admin.partials.navigation', function ($view)
        {
            $openTicketCount = Support::setSender(Auth::user())->GetOpenedTicketsCount();
            $view->with('openTicketCount', $openTicketCount);
        });
    }

    /**
     * Generate variables for left navigation of manager
     */
    public function composeManagerNavigation()
    {
        view()->composer(['manager.partials.navigation','manager.partials.verticalNavigation'], function ($view)
        {
            $vars = [
                'showSupport'=>true,
                'showBilling'=>true,
                'showUsers'=>true,
                'openTicketCount' => 0,
                'enable_user_support' => false,
                'enable_user_billing' => false,
                'enable_user_account' => false
            ];
            $subscription = Auth::user()->subscription()->first();
            if(!is_null($subscription)){
                if(Carbon::parse($subscription->ends_at) > Carbon::now()){
                    if($subscription->subscribable->enable_user_support){
                        $vars['enable_user_support'] = true;
                    }
                    if($subscription->subscribable->enable_user_billing){
                        $vars['enable_user_billing'] = true;
                    }
                    $vars['enable_user_account'] = true;
                }
                $vars['openTicketCount'] = Support::setSender(Auth::user())->GetOpenedTicketsCount();
            }
            //get invoice count
            $vars['invoiceCount'] = Auth::user()->invoice->where('status','unpaid')->count();

            $view->with('vars', $vars);
        });

    }

    /**
     * Generate variables for left navigation of user
     */
    public function composeUserNavigation()
    {
        view()->composer(['user.partials.navigation','user.partials.verticalNavigation'], function ($view)
        {
            $vars = [
                'openTicketCount' => 0,
                'enable_user_support' => false,
                'show_guest' => false,
                'show_stats' => false,
            ];

            //check is user is subscribe to package
            $package = is_null(Auth::user()->subscription)?null:Auth::user()->subscription->subscribable;
            if(!is_null($package)){
                $vars['show_guest'] = true;
                $vars['show_stats'] = true;
                $manager_subscription = Auth::user()->manager()->subscription;
                $manager_subscribable = is_null($manager_subscription)?null:$manager_subscription->subscribable;
                if(!is_null($manager_subscription)){
                    if(Carbon::parse($manager_subscription->ends_at) > Carbon::now()){
                        if($manager_subscribable->enable_user_support){
                            $vars['enable_user_support'] = true;
                        }
                    }
                    $vars['openTicketCount'] = Support::setSender(Auth::user())->GetOpenedTicketsCount();
                }
            }

            $view->with('vars', $vars);
        });
    }

}
