<?php namespace App\Events;

use App\Events\Event;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class UserWasRegistered extends Event {

	use SerializesModels;

    /**
     * @var User
     */
    protected $user;
    /**
     * The plain password
     * @var string
     */
    protected $password;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param $password
     */
    public function __construct(User $user,$password)
    {
        $this->user = $user;
        $this->password = $password;
    }


}
