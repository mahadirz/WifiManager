<?php namespace App\Events;

use App\Events\Event;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class MessageWasCreated extends Event {

	use SerializesModels;

    /**
     * @var int
     */
    public $receiver_id;
    /**
     * @var int
     */
    public $sender_id;

    /**
     * Create a new event instance.
     *
     * @param User $receiver_id
     * @param User $sender_id
     */
	public function __construct($receiver_id,$sender_id)
	{
		$this->receiver_id = $receiver_id;
        $this->sender_id = $sender_id;
	}

}
