<?php namespace App\Events;

use App\Events\Event;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class UserHasBeenBlockedToNas extends Event {

	use SerializesModels;

    /**
     * @var String
     */
    protected $username;

    /**
     * @return String
     */
    public function getUserName()
    {
        return $this->username;
    }


    /**
     * Create a new event instance.
     *
     * @param $username
     */
    public function __construct($username)
    {
        $this->username = $username;
    }


}
