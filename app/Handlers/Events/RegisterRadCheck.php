<?php namespace App\Handlers\Events;

use App\Events\UserWasRegistered;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Radius;

class RegisterRadCheck {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserWasRegistered  $event
	 * @return void
	 */
	public function handle(UserWasRegistered $event)
	{
		//maybe send email to greet
        $user = $event->getUser();
        if($user->hasRole('user')){
            //since the end user only can connect to NAS
            //so add radcheck password
            $radius = Radius::setUsername($user->username);
            $radius->setPassword($event->getPassword());
            $radius->addCheck();
        }
	}

}
