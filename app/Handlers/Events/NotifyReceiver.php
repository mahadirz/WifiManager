<?php namespace App\Handlers\Events;

use App\Events\MessageWasCreated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Redis;

class NotifyReceiver implements ShouldBeQueued{

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  MessageWasCreated  $event
	 * @return void
	 */
	public function handle(MessageWasCreated $event)
	{
        $data = [
            'event' => 'MessageWasCreated',
            'data'=> [
                'recipient_id' =>$event->receiver_id,
                'sender_id' => $event->sender_id,
            ],
        ];
        Redis::publish('notification-channel',json_encode($data));
	}

}
