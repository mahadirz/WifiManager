<?php namespace App\Handlers\Events;

use App\Events\UserPasswordWasChanged;

use Radius;

class UpdateRadCheck  {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserPasswordWasChanged  $event
	 * @return void
	 */
	public function handle(UserPasswordWasChanged $event)
	{
		$user = $event->getUser();
        if($user->hasRole('user')){
            //since the end user only can connect to NAS
            //so change the password
            $radius = Radius::setUsername($user->username);
            $radius->setPassword($event->getPassword());
            $radius->addCheck();
        }
	}

}
