<?php namespace App\Handlers\Events;

use App\Events\UserHasBeenBlockedToNas;

use Artisan;
use DB;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class DisconnectWirelessClient {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  UserHasBeenBlockedToNas  $event
	 * @return void
	 */
	public function handle(UserHasBeenBlockedToNas $event)
	{
		//select all radacct sessions that active and belong to the username
        $username = $event->getUserName();
        $result = DB::select("SELECT callingstationid FROM `radacct` WHERE acctstoptime IS NULL AND username = '$username'");
        foreach($result as $r){
            Artisan::call("test:disconnect-client",['mac_address'=>$r->callingstationid]);
        }

	}

}
