<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 9/1/2015
 * Time: 10:12 PM
 */

namespace App\Libraries\Providers;


use Illuminate\Support\ServiceProvider;

class SecurityServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('security', function () {
            return $this->app->make('App\Libraries\Security');
        });
    }
}