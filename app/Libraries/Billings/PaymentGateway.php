<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 7:11 PM
 */

namespace App\Libraries\Billings;

use App\Libraries\Billings\Contracts\Payment;
use GetSetGo\SetterGetter;
use View;
use Illuminate\Http\Request;

class PaymentGateway implements Payment
{
    use SetterGetter;

    /**
     *
     */
    public function __construct()
    {
        View::addNamespace('gateway', __DIR__.'/views');
    }

    /**
     * Prepare params before client make payment
     * @return mixed
     */
    public function prepare()
    {
        // TODO: Implement prepare() method.
    }

    /**
     * Initiate payment
     * @return mixed
     */
    public function pay()
    {
        // TODO: Implement pay() method.
    }

    /**
     * Once payment has been made by client
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    public function done(Request $request)
    {
        // TODO: Implement done() method.
    }
}