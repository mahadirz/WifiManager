<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 2:33 AM
 */

namespace App\Libraries\Billings;


use App\Libraries\Billings\Contracts\Billable;
use App\Models\Configuration;
use App\Models\Invoice;
use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use GetSetGo\SetterGetter;

class Billing implements Billable
{
    //setter trait
    use SetterGetter;

    /**
     * @var \App\Models\User
     */
    protected $user;

    /**
     * Last used invoice
     * @var Invoice
     */
    protected $invoice;

    /**
     * @var Subscription
     */
    protected $subscription;

    /**
     * @var $this
     */
    protected $instance;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->instance = $this;
    }

    /**
     * @param string $notes
     * @return $this
     */
    private function cancelActiveInvoices($notes="")
    {
        foreach($this->user->invoice()->where('status','unpaid')->get() as $invoice){
            $invoice->status = "cancelled";
            $invoice->notes = $notes;
            $invoice->save();
        }
        return $this;
    }

    /**
     * Subscribe or package
     * @param $subscribable_id
     * @param $subscribable_type
     * @return $this
     */
    public function subscribe($subscribable_id,$subscribable_type,$ends_at=null)
    {
        $sub = Subscription::where('user_id',$this->user->id)->first();
        if(is_null($sub)){
            $sub = new Subscription();
            $sub->user_id = $this->user->id;
        }
        $sub->subscribable_id = $subscribable_id;
        $sub->subscribable_type = $subscribable_type;
        $sub->active = true;
        $sub->ends_at = is_null($ends_at)?Carbon::now():$ends_at;
        $sub->save();
        //find all active invoice and cancel it
        $this->cancelActiveInvoices("new subscription created");
        $this->subscription = $sub;
        return $this;
    }

    /**
     * Unsubscribe current subscription
     * @return $this
     */
    public function unSubscribe()
    {
        $sub = Subscription::where('user_id',$this->user->id)->first();
        if(is_null($sub)){
            $sub = new Subscription();
            $sub->user_id = $this->user->id;
        }
        $sub->active = false;
        $sub->save();
        $this->subscription = $sub;
        return $this;
    }

    /**
     * Renew current subscription
     * @return $this
     */
    public function renew()
    {
        $sub = Subscription::where('user_id',$this->user->id)->first();
        if(is_null($sub)){
            $sub = new Subscription();
            $sub->user_id = $this->user->id;
        }
        $sub->active = true;
        $sub->save();
        $this->subscription = $sub;
        $this->invoice();
        return $this;
    }

    /**
     * Cancel invoice by id or latest one
     * @param integer $id
     */
    public function cancelInvoice($id=null)
    {
        if(is_null($id)){
            //find latest invoice
            $this->invoice = Invoice::latest()->first();
        }
        else{
            $this->invoice = Invoice::find($id);
        }

        $this->invoice->status = 'cancelled';
        $this->invoice->save();

    }

    /**
     * @param string $type
     * @param string $cycle
     * @param $returnURL
     * @param $cancelURL
     * @return mixed|void
     * @throws \Exception
     */
    public function initPaymentGateway($type="paypal",$cycle="monthly_price",$returnURL,$cancelURL)
    {
        if(is_null($this->invoice))
            throw new \Exception("Please set invoice");
        switch($type){
            case "paypal":
                $gateway = new PaypalGateway();
                $params = [
                    'amount'=>$this->invoice->packageable->$cycle,
                    'currency'=>$this->getCurrency(),
                    'descriptions'=>$this->invoice->packageable->name." - ".$this->invoice->packageable->descriptions,
                    'returnURL'=>$returnURL,
                    'cancelURL'=>$cancelURL,
                    'invoiceId'=>$this->invoice->id];
                return $gateway->prepare($params);
                break;
            case "manual":
                break;
        }
    }

    /**
     * Verify the payment made by user through respective gateway
     * @param \Illuminate\Http\Request $request
     * @param string $type
     * @throws \Exception
     */
    public function verifyGatewayPayment(\Illuminate\Http\Request $request,$type="paypal")
    {
        switch($type){
            case "paypal":
                $gateway = new PaypalGateway();
                $gateway->done($request);
                if($gateway->isPaymentApproved()){
                    //payment success
                    if($gateway->getPaymentCurrency() == $this->getCurrency()){
                        //same currency
                        //pay the invoice
                        $this->payInvoice($gateway->getPaymentAmountTotal(),$gateway->getPaymentId(),$gateway->getPaymentInvoiceId());
                    }
                    else
                        throw new \Exception("Payment currency mismatch: paypal said currency=>".$gateway->getPaymentCurrency());
                }
                else
                    throw new \Exception("Payment was failed");
                break;
            case "manual":
                //the creator of the invoice that approve it
                //so less checking
                $gateway = new ManualGateway();
                $gateway->done($request);
                $this->payInvoice($gateway->getAmount(),$gateway->getNotes(),$gateway->getInvoiceId());
                break;
        }
    }

    /**
     * @param string $amount
     * @param string $notes
     * @param integer $id
     * @return $this
     */
    public function payInvoice($amount,$notes,$id=null)
    {
        if(!is_null($id)){
            $this->invoice = Invoice::find($id);
        }
        if($this->invoice->status == "incomplete"){
            //update the current payment with the paid amount
            $amount += $this->invoice->paid_amount;
        }
        //if already paid dont pay again
        if($this->invoice->status != "paid"){
            $this->invoice->paid_amount = $amount;
            $newEndsDate = $this->newSubscriptionEndAt($amount);
            //the incomplete example payed not in full or not same price as
            //the package price
            $this->invoice->status = $newEndsDate == false ? "incomplete" : "paid";
            $this->invoice->notes = $notes;
            $this->invoice->save();
            //set the subscription ends date
            $this->setSubscriptionEndDate($newEndsDate);
        }
        return $this;
    }

    /**
     * Get new subscription ends date from amount paid,
     * if payment incomplete return false
     * @param $amount
     * @return Carbon|false
     */
    public function newSubscriptionEndAt($amount)
    {
        if(is_null($this->subscription))
            $this->subscription = $this->subscription();
        //if the subscription ends_at still not expired, use it
        if(Carbon::parse($this->subscription->ends_at)>Carbon::now())
            $ends_at = Carbon::parse($this->subscription->ends_at);
        else
            $ends_at = Carbon::now();
        $package = $this->subscription->subscribable;
        if($package->isFree()){
            //use annual
            $ends_at->addYear(1);
        }
        else if($package->monthly_price == $amount){
            $ends_at->addMonth();
        }
        else if($package->semi_annually_price == $amount){
            //6 months
            $ends_at->addMonth(6);
        }
        else if($package->annually_price == $amount){
            $ends_at->addYear(1);
        }
        else{
            return false;
        }
        return $ends_at;
    }

    /**
     * Generate invoice
     * @return $this
     */
    public function invoice()
    {
        if($this->subscription->active){
            //only create invoice if active
            $invoice = new Invoice();
            $invoice->user_id = $this->user->id;
            $invoice->packageable_id = $this->subscription->subscribable_id;
            $invoice->packageable_type = $this->subscription->subscribable_type;
            $invoice->status = "unpaid";
            $invoice->save();
            $this->invoice = $invoice;
            //if the package price is free then pay the invoice
            if($invoice->packageable->isFree())
                $this->payInvoice(0.00,"Free package");
        }
        return $this;
    }

    /**
     * Get the name that should be shown on the entity's invoices.
     *
     * @return string
     */
    public function getBillableName()
    {
        return $this->user->getFullName();
    }

    /**
     * Get subscription attached to user
     * @return Subscription|null
     */
    public function subscription()
    {
        return $this->user->subscription;
    }

    /**
     * Find invoice by primary key
     * @param $id
     * @return $this
     */
    public function findInvoice($id)
    {
        $this->invoice = Invoice::find($id);
        return $this;
    }

    /**
     * Get an array of the entity's invoices.
     *
     * @param  array $parameters
     * @return array
     */
    public function invoices($parameters = [])
    {
        if(isset($parameters['paginate']))
            $paginate = $parameters['paginate'];
        else
            $paginate = 10;
        $invoices = $this->user->invoice()->latest()->paginate($paginate);
        return $invoices;
    }


    /**
     * Determine if the entity has an active subscription.
     *
     * @return bool
     */
    public function subscribed()
    {
        // TODO: Implement subscribed() method.
    }

    /**
     * Determine if the entity's plan has expired.
     *
     * @return bool
     */
    public function expired()
    {
        // TODO: Implement expired() method.
    }

    /**
     * Determine if the entity is on the given plan.
     *
     * @param  string $plan
     * @return bool
     */
    public function onPlan($plan)
    {
        // TODO: Implement onPlan() method.
    }

    /**
     * Get the subscription end date for the entity.
     *
     * @return \App\Libraries\Billings\Contracts\Carbon
     */
    public function getSubscriptionEndDate()
    {
        // TODO: Implement getSubscriptionEndDate() method.
    }

    /**
     * Set the subscription end date for the entity.
     *
     * @param  Carbon|null $date
     * @return void
     */
    public function setSubscriptionEndDate($date)
    {
        if(!is_null($this->subscription)){
            $this->subscription->ends_at = $date;
            $this->subscription->save();
        }
    }

    /**
     * Get the Stripe supported currency used by the entity.
     *
     * @return string
     */
    public function getCurrency()
    {
        return Configuration::getKeyValues(['currencyPostfix'])->get('currencyPostfix');
    }

    /**
     * Format the given currency for display, without the currency symbol.
     *
     * @param  int $amount
     * @return mixed
     */
    public function formatCurrency($amount)
    {
        // TODO: Implement formatCurrency() method.
    }

    /**
     * Add the currency symbol to a given amount.
     *
     * @param  string $amount
     * @return string
     */
    public function addCurrencySymbol($amount)
    {
        // TODO: Implement addCurrencySymbol() method.
    }
}