<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 6:59 AM
 */

namespace App\Libraries\Billings;

use Illuminate\Support\Facades\Facade;

class BillingFacade extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Libraries\Billings\Contracts\Billable';
    }
}