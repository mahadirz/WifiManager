<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 7:12 PM
 */

namespace App\Libraries\Billings;


use Illuminate\Http\Request;

class ManualGateway extends PaymentGateway
{
    /**
     * @var float
     */
    protected $amount;


    /**
     * ID to identify this transaction
     * @var string
     */
    protected $invoiceId;

    /**
     * @var string
     */
    protected $notes;

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    public function prepare()
    {

    }

    /**
     * @param Request $request
     * @return $this
     */
    public function done(Request $request)
    {
        $this->amount = floatval($request->get('amount'));
        $this->notes = $request->get('notes');
        $this->invoiceId = $request->get('invoice_id');
        return $this;
    }
}