<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 2:35 AM
 */

namespace App\Libraries\Billings\Contracts;


interface Billable
{
    /**
     * Get the name that should be shown on the entity's invoices.
     *
     * @return string
     */
    public function getBillableName();


    /**
     * Get subscription attached to user
     * @return mixed
     */
    public function subscription();

    /**
     * @param $subscribable_id
     * @param $subscribable_type
     * @return $this
     */
    public function subscribe($subscribable_id,$subscribable_type,$ends_at=null);

    /**
     * Unsubscribe current subscription
     * @return $this
     */
    public function unSubscribe();

    /**
     * Renew current subscription
     * @return $this
     */
    public function renew();

    /**
     * Cancel invoice by id or latest one
     * @param integer $id
     */
    public function cancelInvoice($id=null);



    /**
     * Generate invoice
     * @return $this
     */
    public function invoice();

    /**
     * Find invoice by primary key
     * @param $id
     * @return $this
     */
    public function findInvoice($id);

    /**
     * Get an array of the entity's invoices.
     *
     * @param  array $parameters
     * @return array
     */
    public function invoices($parameters = []);


    /**
     * Determine if the entity has an active subscription.
     *
     * @return bool
     */
    public function subscribed();

    /**
     * Determine if the entity's plan has expired.
     *
     * @return bool
     */
    public function expired();

    /**
     * Determine if the entity is on the given plan.
     *
     * @param  string $plan
     * @return bool
     */
    public function onPlan($plan);


    /**
     * Get the subscription end date for the entity.
     *
     * @return Carbon
     */
    public function getSubscriptionEndDate();

    /**
     * Set the subscription end date for the entity.
     *
     * @param  \DateTime|null $date
     * @return void
     */
    public function setSubscriptionEndDate($date);

    /**
     * Get the Stripe supported currency used by the entity.
     *
     * @return string
     */
    public function getCurrency();


    /**
     * Format the given currency for display, without the currency symbol.
     *
     * @param  int $amount
     * @return mixed
     */
    public function formatCurrency($amount);

    /**
     * Add the currency symbol to a given amount.
     *
     * @param  string $amount
     * @return string
     */
    public function addCurrencySymbol($amount);

}