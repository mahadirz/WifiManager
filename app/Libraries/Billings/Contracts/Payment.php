<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 6:50 PM
 */

namespace App\Libraries\Billings\Contracts;

use Illuminate\Http\Request;

interface Payment
{
    /**
     * Prepare params before client make payment
     * @return mixed
     */
    public function prepare();

    /**
     * Initiate payment
     * @return mixed
     */
    public function pay();

    /**
     * Once payment has been made by client
     * @return mixed
     */
    public function done(Request $request);
}