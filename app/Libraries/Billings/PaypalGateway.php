<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 7:11 PM
 */

namespace App\Libraries\Billings;

use Carbon\Carbon;
use PayPal;
use PayPal\Api\Payment;
use Redirect;
use Request;
use Storage;
use View;

class PaypalGateway extends PaymentGateway
{
    private $_apiContext;
    /**
     * @var float
     */
    protected $amount;

    /**
     * ID to identify this transaction
     * @var string
     */
    protected $invoiceId;

    /**
     * @var string
     */
    protected $currency = "USD";

    /**
     * @var string
     */
    protected $returnURL;

    /**
     * @var string
     */
    protected $descriptions;

    /**
     * @var string
     */
    protected $cancelURL;

    /**
     * Paypal checkout URL
     * @var string
     */
    protected $checkOutURL;

    /**
     * @var Payment
     */
    protected $payPalPayment;

    public function __construct()
    {
        parent::__construct();
        $this->_apiContext = PayPal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => config('services.paypal.mode'),
            'service.EndPoint' => config('services.paypal.endpoint')[config('services.paypal.mode')],
            'http.ConnectionTimeOut' => config('services.paypal.connection_timeout'),
            'log.LogEnabled' => config('services.paypal.log_enabled'),
            'log.FileName' => config('services.paypal.log_filename'),
            'log.LogLevel' => config('services.paypal.log_level')
        ));

    }

    /**
     * Return html data
     * @param array $params
     * @return mixed|void
     */
    public function prepare(array $params = [])
    {
        //prepare what is the required params before pay
        $this->amount = $params['amount'];
        $this->currency = $params['currency'];
        $this->descriptions = $params['descriptions'];
        $this->returnURL = $params['returnURL'];
        $this->cancelURL = $params['cancelURL'];
        $this->invoiceId = $params['invoiceId'];
        $this->checkOutURL = $this->setCheckout();
        //return View::make('gateway::paypalPayButton')->with('url',$this->checkOutURL)->render();
        return $this;
    }

    /**
     * Redirect to checkout URL
     * @return \Illuminate\Http\RedirectResponse
     */
    public function pay()
    {
        return Redirect::to( $this->checkOutURL )->send();
    }


    /**
     * @return mixed
     */
    protected function setCheckout()
    {
        $payer = PayPal::Payer();
        $payer->setPaymentMethod('paypal');

        $amount = PayPal:: Amount();
        $amount->setCurrency($this->currency);
        $amount->setTotal($this->amount);

        $transaction = PayPal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription($this->descriptions);
        $transaction->setInvoiceNumber($this->invoiceId);

        $redirectUrls = PayPal:: RedirectUrls();
        $redirectUrls->setReturnUrl($this->returnURL);
        $redirectUrls->setCancelUrl($this->cancelURL);

        $payment = PayPal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        $response = $payment->create($this->_apiContext);
        //$redirectUrl = $response->links[1]->href;
        $redirectUrl = $response->getApprovalLink()."&useraction=commit";
        return $redirectUrl;
    }

    /**
     * @param $paymentId
     * @return mixed
     */
    public function getPaymentDetails($paymentId,$lookupLocal=true)
    {
        if($lookupLocal){
            if(Storage::disk('local')->exists('PayPal/'.$paymentId.'.txt')){
                return unserialize(Storage::disk('local')->get('PayPal/'.$paymentId.'.txt'));
            }
        }
        return PayPal::Payment()->get($paymentId, $this->_apiContext);
    }

    public function logPayment($paymentId)
    {
        Storage::disk('local')->put('PayPal/'.$paymentId.'.txt',
            serialize($this->getPaymentDetails($paymentId)));
    }

    /**
     * @return string
     */
    public function getPaymentId()
    {
        if(!is_null($this->payPalPayment)){
            return $this->payPalPayment->getId();
        }
        return "";
    }

    /**
     * @return bool
     */
    public function isPaymentApproved()
    {
        if(!is_null($this->payPalPayment)){
            if($this->payPalPayment->getstate()=="approved")
                return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getPaymentAmountTotal()
    {
        if(!is_null($this->payPalPayment)){
            return $this->payPalPayment->getTransactions()[0]->getAmount()->getTotal();
        }
        return "";
    }

    /**
     * @return string
     */
    public function getPaymentCurrency()
    {
        if(!is_null($this->payPalPayment)){
            return $this->payPalPayment->getTransactions()[0]->getAmount()->getCurrency();
        }
        return "";
    }

    /**
     * @return string
     */
    public function getPaymentInvoiceId()
    {
        if(!is_null($this->payPalPayment)){
            return $this->payPalPayment->getTransactions()[0]->getInvoiceNumber();
        }
        return "";
    }

    /**
     * @return null|Carbon
     */
    public function getPaymentCreateTime()
    {
        if(!is_null($this->payPalPayment)){
            return Carbon::parse($this->payPalPayment->getCreateTime())->setTimezone(config('app.timezone'));
        }
        return null;
    }

    /**
     * @return null|Carbon
     */
    public function getPaymentUpdateTime()
    {
        if(!is_null($this->payPalPayment)){
            return Carbon::parse($this->payPalPayment->getUpdateTime())->setTimezone(config('app.timezone'));
        }
        return null;
    }

    /**
     * Execute when payment has been made
     * @param \Illuminate\Http\Request $request
     * @return $this|mixed
     */
    public function done(\Illuminate\Http\Request $request)
    {
        $id = $request->get('paymentId');
        $token = $request->get('token');
        $payer_id = $request->get('PayerID');

        $payment = PayPal::getById($id, $this->_apiContext);

        $paymentExecution = PayPal::PaymentExecution();

        $paymentExecution->setPayerId($payer_id);

        try {
            $payment->execute($paymentExecution, $this->_apiContext);
        } catch (\Exception $e) {
            //the problem might be due to the payment already executed
            //so just catch
        }


        //at this point the transaction has succeeded
        //retrieve payment details
        $this->payPalPayment = $this->getPaymentDetails($request->get('paymentId'));
        //log payment to file
        $this->logPayment($request->get('paymentId'));
        return $this;
    }
}