<?php namespace App\Libraries\Billings;

use App\Models\User;
use Auth;
use Illuminate\Support\ServiceProvider;

class BillingServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Libraries\Billings\Contracts\Billable',function() {
            $user = Auth::user();
            if(is_null($user)){
                $user = new User();
            }
            return new Billing($user);
        }
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('App\Libraries\Billings\Contracts\Billable');
    }

}
