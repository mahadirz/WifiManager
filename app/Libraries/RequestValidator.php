<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/9/2015
 * Time: 11:34 PM
 */

namespace App\Libraries;


use DB;
use Validator;

class RequestValidator
{

    /**
     * @var \Illuminate\Validation\Validator
     */
    protected static $_validator;


    public function __construct()
    {
        //register custom rule
        //usage: banned_domain:table,column
        Validator::extend('banned_domain', function($attribute, $value, $parameters)
        {
            if($value)
            {
                $table = array_key_exists(0,$parameters)? $parameters[0]:"banned_domain";
                $column = array_key_exists(1,$parameters)? $parameters[1]:"domain";
                $domain = explode('@',$value);
                if(array_key_exists(1,$domain))
                    $domain = $domain[1];
                else
                    return false;
                $db = DB::table($table)->where($column,$domain)->first();
                if(!is_null($db))
                    return false;
            }
            return true;
        });
    }

    /**
     * @param $value
     * @param null $exceptId
     * @return \Illuminate\Validation\Validator
     */
    public function username($value,$exceptId=null)
    {
        if(is_null($exceptId))
            $unique = 'unique:users,username';
        else
            $unique = "unique:users,username,$exceptId,id";
        self::$_validator = Validator::make(
            ['username' => $value],
            ['username' => ['required', 'min:5','max:48','alpha_dash',$unique]]
        );
        return self::$_validator;
    }

    /**
     * @param $value
     * @param null $exceptId
     * @return \Illuminate\Validation\Validator
     */
    public function email($value,$exceptId=null)
    {
        if(is_null($exceptId))
            $unique = 'unique:users,email';
        else
            $unique = "unique:users,email,$exceptId,id";
        self::$_validator = Validator::make(
            ['email' => $value],
            ['email' => ['required', 'email',$unique,'banned_domain']],
            [
                'banned_domain' => 'The :attribute domain is banned to be used',
                'unique' => 'The :attribute is in use by other user'
            ]
        );
        return self::$_validator;
    }

    /**
     * Get last used validator instance
     * @return \Illuminate\Validation\Validator
     */
    public function getLastValidator()
    {
        return self::$_validator;
    }

}