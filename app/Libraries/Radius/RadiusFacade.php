<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/25/2015
 * Time: 6:59 AM
 */

namespace App\Libraries\Radius;

use Illuminate\Support\Facades\Facade;

class RadiusFacade extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Libraries\Radius\Radius';
    }
}