<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/4/2015
 * Time: 8:04 PM
 */

namespace App\Libraries\Radius;


/**
 * Class Attributes
 * @package App\Libraries\Radius
 */
class Attributes
{

    /**
     * NTLM Hashed password type
     */
    const NT_PASSWORD = 'NT-Password';

    /**
     *
     */
    const AUTH_TYPE = 'Auth-Type';

    /**
     * http://freeradius.org/rfc/rfc2869.html#Acct-Interim-Interval
     * Account Interim Update in seconds
     * op  =
     */
    const ACCT_INTERIM_INTERVAL = "Acct-Interim-Interval";

    /**
     *
     */
    const SESSION_TIMEOUT  = "Session-Timeout";

    /**
     * Example: April 08 2017 15:56:05
     */
    const EXPIRATION = "Expiration";
}