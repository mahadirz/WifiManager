<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/31/2015
 * Time: 4:34 AM
 */

namespace App\Libraries\Radius;


use App\Models\RadReply;
use App\Models\Radcheck;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Security;

class Radius
{
    protected $username;
    /**
     * Password hashed with nt
     * @var string
     */
    protected $ntPassword;
    /**
     * Plain password
     * @var string
     */
    protected $password;
    protected $user;

    /**
     * @param mixed $username
     * @return Radius
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @param User $user
     * @return Radius
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param $plainPassword
     * @return $this
     */
    public function setPassword($plainPassword)
    {
        $this->password = $plainPassword;
        $this->ntPassword = Security::NTLMHash($plainPassword);
        return $this;
    }

    /**
     * @param $username
     * @param $attributes
     * @param $op
     * @param $value
     * @param array $finder
     * @return $this
     */
    protected function setRadCheck($username,$attributes,$op,$value,$finder=null)
    {
        if(is_null($finder)){
            $finder = ['username'=>$username];
        }
        $radCheck = Radcheck::firstOrNew($finder);
        $radCheck->username = $username;
        $radCheck->attribute = $attributes;
        $radCheck->op = $op;
        $radCheck->value = $value;
        $radCheck->save();
        return $this;
    }

    /**
     * @param $username
     * @param $attributes
     * @param $op
     * @param $value
     * @param array $finder
     * @return $this
     */
    protected function setRadReply($username,$attributes,$op,$value,$finder=null)
    {
        if(is_null($finder)){
            $finder = ['username'=>$username];
        }
        $radCheck = RadReply::firstOrNew($finder);
        $radCheck->username = $username;
        $radCheck->attribute = $attributes;
        $radCheck->op = $op;
        $radCheck->value = $value;
        $radCheck->save();
        return $this;
    }

    /**
     * Add password authentication field
     */
    public function addCheck()
    {
        $finder = ['username'=>$this->username,'attribute'=>Attributes::NT_PASSWORD];
        $this->setRadCheck($this->username,Attributes::NT_PASSWORD,':=',$this->ntPassword,$finder);
        return $this;
    }

    /**
     * Delete associated radcheck for username
     * @throws \Exception
     */
    public function deleteCheck($finder=null)
    {
        if(is_null($finder)){
            $finder = ['username'=>$this->username];
        }
        try {
            $radCheck = Radcheck::firstOrFail($finder);
            $radCheck->delete();
        } catch (Exception $e) {
        }
        return $this;
    }

    /**
     * Delete all associated radreply for username
     * @return $this
     */
    public function deleteReply()
    {
        $radreply = RadReply::where('username',$this->username);
        $radreply->delete();
        return $this;
    }

    /**
     * @return $this
     */
    public function deleteAll()
    {
        $this->deleteCheck();
        $this->deleteReply();
        return $this;
    }

    /**
     * Suspend user
     * @return $this
     */
    public function suspendUser()
    {
        $finder = ['username'=>$this->username,'attribute'=>Attributes::EXPIRATION];
        $this->setRadCheck($this->username,Attributes::EXPIRATION,'==','February 10 1993 00:00:00',$finder);
        return $this;
    }

    /**
     * unSuspend user
     * @return $this
     */
    public function unSuspendUser()
    {
        $date = Carbon::now()->addYear(10)->format("F d Y H:i:s");
        $finder = ['username'=>$this->username,'attribute'=>Attributes::EXPIRATION];
        $this->setRadCheck($this->username,Attributes::EXPIRATION,'==',$date,$finder);
        return $this;
    }

    /**
     * The value is in seconds
     * @param $value
     * @return $this
     */
    public function setInterim($value=600)
    {
        $finder = ['username'=>$this->username,'attribute'=>Attributes::ACCT_INTERIM_INTERVAL];
        $this->setRadReply($this->username,Attributes::ACCT_INTERIM_INTERVAL,'=',$value,$finder);
        return $this;
    }

    /**
     * The value is in seconds
     * @param $value
     * @return $this
     */
    public function setSessionTimeout($value=3600)
    {
        $finder = ['username'=>$this->username,'attribute'=>Attributes::SESSION_TIMEOUT];
        $this->setRadReply($this->username,Attributes::SESSION_TIMEOUT,'=',$value,$finder);
        return $this;
    }

    /**
     * @param $input
     * @param $precisions
     * @return float|string
     */
    public function Bytes_converter($input,$precisions=2)
    {
        if($input >= 1074000000){
            $input = round($input/1074000000,$precisions);
            $input .= " GB";
        }
        else if($input >= 1048576){
            $input = round($input/1048576,$precisions);
            $input .= " MB";
        }
        else if($input >= 1024){
            $input = round($input/1024,$precisions);
            $input .= " KB";
        }
        else if($input >0)
            $input .= "B";
        else
            $input = "0 B";

        return $input;
    }

    /**
     * @param $username
     * @param int $within
     * @param int $conversion
     * @return array
     */
    public function generateUsageStats($username,$within=30,$conversion=1048576 )
    {
        $startDate = Carbon::now()->subDay($within);
        $sql = "SELECT SUM(AcctOutputOctets)/$conversion AS download,SUM(acctinputoctets)/$conversion AS upload,
                ((SUM(AcctOutputOctets)/$conversion)+(SUM(acctinputoctets)/$conversion)) as total,
                date_format(acctstarttime, '%Y-%m-%d') AS date FROM radacct WHERE username='$username'
                AND acctstarttime >= '$startDate'
                GROUP BY date_format(acctstarttime, '%Y-%m-%d')";
//        echo "<pre>";
//        print_r($sql);
//        echo "</pre>";
//        die();
        $result = DB::select($sql);
        return $result;
    }

    public function generateHourStats($username,$within=30,$conversion=1048576 )
    {
        $startDate = Carbon::now()->subDay($within);
        $sql = "SELECT  acctsessiontime as total,
                date_format(acctstarttime, '%Y-%m-%d') AS date FROM radacct WHERE username='$username'
                AND acctstarttime >= '$startDate'
                GROUP BY date_format(acctstarttime, '%Y-%m-%d')";
//        dd($sql);
        $result = DB::select($sql);
        return $result;
    }

    public function generateGuestConnectionLists($username,$start=null,$end=null)
    {
        if(is_null($start)){
            $start = Carbon::now()->subHour(24);
        }
        if(is_null($end)){
            $end = Carbon::now();
        }

        $sql = "SELECT acctupdatetime,acctstarttime,acctstoptime,callingstationid,acctinputoctets,acctoutputoctets,acctsessiontime FROM `radacct`
        WHERE username = '$username' AND
        acctstarttime between  '$start' AND  '$end' ORDER BY acctstarttime DESC ";
//        echo "<pre>";
//        print_r($sql);
//        echo "</pre>";
//        die();
        $result = DB::select($sql);
        return $result;
    }

    /**
     * @param $mac
     * @return string
     */
    public function convertMacAddress($mac,$format="-")
    {
        preg_match('/([0-9a-f]{2})[-:]?([0-9a-f]{2})[-:.]?([0-9a-f]{2})[-:]?([0-9a-f]{2})[-:.]?([0-9a-f]{2})[-:]?([0-9a-f]{2})/i',$mac,$matches);
        if(count($matches)>=7){
            return $matches[1].$format.$matches[2].$format.$matches[3].$format.$matches[4].$format.$matches[5].$format.$matches[6];
        }
        return $mac;
    }

    /**
     * Get the AcctOutputOctets by default in a month
     * Return as number of bytes uploaded
     * @param $username
     * @param string $type
     * @param null $start
     * @param null $end
     * @return int
     */
    public function getAcctInputOutputOctets($username,$type="both",$start=null,$end=null)
    {
        $acctQuery = "SUM(AcctOutputOctets) + SUM(acctinputoctets)";
        switch($type){
            case "download":
                $acctQuery = "SUM(acctinputoctets)";
                break;
            case "upload":
                $acctQuery = "SUM(AcctOutputOctets)";
                break;
        }
        if(is_null($start)){
            $start = Carbon::now()->subMonth(1);
        }
        if(is_null($end)){
            $end = Carbon::now();
        }

        $sql = "SELECT IFNULL($acctQuery,0) AS total FROM `radacct`
        WHERE username = '$username' AND
        acctstarttime between  '$start' AND  '$end'";
        $result = DB::select($sql);
        return is_array($result)? $result[0]->total:0;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getDailyDataUsage($username)
    {
        $start = Date('Y-m-d')." 00:00:00";
        $end = Date('Y-m-d')." 23:59:59";
        $usage = $this->getAcctInputOutputOctets($username,"both",$start,$end);
        return $usage;
    }

    /**
     * @param $username
     * @return mixed
     */
    public function getDailyHoursUsage($username)
    {
        $start = Date('Y-m-d')." 00:00:00";
        $end = Date('Y-m-d')." 23:59:59";
        $sql = "SELECT IFNULL(SUM(acctsessiontime),0) as total FROM radacct WHERE username = '$username'
        AND acctstarttime BETWEEN '$start' AND '$end'";
        $seconds =  DB::select($sql);
        $seconds = is_array($seconds)? $seconds[0]->total : 0;
        //convert to hour
        $hour = round($seconds/3600,2);
        return $hour;
    }

    /**
     * Get number of simultaneous connection to NAS
     * @param $username
     * @return mixed
     */
    public function getConnectionTotal($username)
    {
        $sql = "SELECT COUNT(*) as total FROM `radacct`WHERE username = '$username' AND acctstoptime IS NULL
        AND acctstarttime + interval 1 day > NOW()";
        $result = DB::select($sql);
        $result = is_array($result) ? $result[0]->total:0;
        return $result;
    }
}