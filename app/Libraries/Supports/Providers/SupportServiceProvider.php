<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 9/1/2015
 * Time: 10:12 PM
 */

namespace App\Libraries\Supports\Providers;


use Illuminate\Support\ServiceProvider;

class SupportServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('support', function () {
            return $this->app->make('App\Libraries\Supports\Support');
        });
    }
}