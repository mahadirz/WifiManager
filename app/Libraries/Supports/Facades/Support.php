<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 9/1/2015
 * Time: 10:40 PM
 */

namespace App\Libraries\Supports\Facades;


use Illuminate\Support\Facades\Facade;

class Support extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'support';
    }
}