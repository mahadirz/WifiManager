<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/14/2015
 * Time: 8:38 PM
 */

namespace App\Libraries\Supports\Contracts;


interface Ticket
{

    public function createNewTicket();

    public function openTicket();

    public function closeTicket();

    public function checkForNewTicket();

    public function viewTicket($id);

    public function loadTickets();
}