<?php
/**
 * Created by PhpStorm.
 * User: Mahadir
 * Date: 10/15/2015
 * Time: 1:46 AM
 */

namespace App\Libraries\Supports;


use App\Commands\Emailer;
use App\Events\MessageWasCreated;
use App\Models\Message;
use App\Models\Ticket;
use App\Models\User;
use Bus;
use Event;
use GetSetGo\SetterGetter;
use Illuminate\Database\Eloquent\Collection;

/**
 *
 * Class Support
 * @package App\Libraries\Supports
 * @method Support setSender(User $user)       Set sender user
 * @method Support setReceiver(User $user)     Set receiver user
 */
class Support
{
    //setter trait
    use SetterGetter;

    /**
     * @var \App\Models\Message
     */
    protected $message;
    protected $messages = array();
    /**
     * @var \App\Models\Ticket
     */
    protected $ticket;

    /**
     * @var Collection
     */
    protected $tickets;
    /**
     * @var Collection
     */
    protected $openedTickets;

    /**
     * @var \App\Models\User
     */
    protected $sender;
    /**
     * @var \App\Models\User
     */
    protected $receiver;

    /**
     * @var Integer
     */
    protected $messagePaginate = 5;
    protected $ticketPaginate = 20;

    /**
     * @var bool
     */
    protected $sendEmail = true;


    public function __construct()
    {
    }

    /**
     * Initialize Ticket Model from ticket_id
     * @param $ticket_id
     * @return $this
     */
    public function initTicket($ticket_id)
    {
        $this->ticket = Ticket::find($ticket_id);
        return $this;
    }

    /**
     * It is assumed that $sender and $receiver has
     * been initialized
     * @param $title
     * @return $this
     */
    public function createNewTicket($title)
    {
        //$userSender = User::find($sender_id);
        //$userReceiver = User::find($receiver_id);
        $this->ticket = new Ticket();
        $this->ticket->title = $title;
        $this->ticket->user_id_sender = $this->sender->id;
        $this->ticket->user_id_receiver = $this->receiver->id;
        $this->ticket->save();
        return $this;
    }

    /**
     * The $sender must already initialized
     * @param $body
     * @return $this
     * @throws \Exception
     */
    public function addMessage($body)
    {
        if(is_null($this->sender))
            throw new \Exception('Message owner unknown');
        $this->message = new Message();
        $this->message->body = $body;
        $this->message->user_id = $this->sender->id;
        $this->ticket->message()->save($this->message);

        $receiver = ($this->ticket->ReceiverUser->id == $this->sender->id)? $this->ticket->senderUser:$this->ticket->ReceiverUser;
        //fire MessageWasCreated event
        Event::fire(new MessageWasCreated(
            $receiver->id,
            $this->sender->id
        ));

        if($this->sendEmail){
            $subject = "[HEWMS Ticket ID ".$this->ticket->id."] ".$this->ticket->title;
            $sender = User::where('username','system_user2')->first();
            $emailer = new Emailer($sender,$receiver,$subject,$body);
            $emailer->view = 'emails.ticket';
            Bus::dispatch($emailer);
        }

        return $this;
    }


    /**
     * @return mixed
     */
    public function GetOpenedTicketsCount()
    {
        $count = Ticket::where('status', 'open')
            ->whereUserIdSenderOrUserIdReceiver($this->sender->id, $this->sender->id)
            ->get()->count();
        return $count;
    }

    /**
     * Retrieve all opened ticket
     * @return $this
     */
    public function GetOpenedTickets($search=null)
    {

        $tickets = Ticket::where(function($q){
            $q->where('status','open')
                ->where(function($q){
                    $q->whereUserIdSenderOrUserIdReceiver($this->sender->id, $this->sender->id);
                });
        });
        if(!is_null($search)){
            $tickets = $tickets->where('title','like',"%$search%");
        }
        $tickets = $tickets->latest()
            ->paginate($this->ticketPaginate);
        $this->tickets = $tickets;
        return $this;
    }

    /**
     * Retrieve all closed tickets
     * @return $this
     */
    public function GetClosedTickets($search=null)
    {
        $tickets = Ticket::where(function($q){
            $q->where('status','closed')
                ->where(function($q){
                    $q->whereUserIdSenderOrUserIdReceiver($this->sender->id, $this->sender->id);
                });
        });
        if(!is_null($search)){
            $tickets = $tickets->where('title','like',"%$search%");
        }
        $tickets = $tickets->latest()
            ->paginate($this->ticketPaginate);
        $this->tickets = $tickets;
        return $this;
    }


    /**
     * Set the status of ticket to open
     * @return $this
     */
    public function setTicketToOpen()
    {
        if(!is_null($this->ticket)){
            $this->ticket->status = 'open';
            $this->ticket->save();
        }
        return $this;
    }

    /**
     * Set the status ticket to closed
     * @return $this
     */
    public function setTicketToClosed()
    {
        if(!is_null($this->ticket)){
            $this->ticket->status = 'closed';
            $this->ticket->save();
            return true;
        }
        return false;
    }


    /**
     * Retrieve all tickets
     * @param null $search
     * @return $this
     */
    public function getAllTickets($search=null)
    {
        $tickets = $tickets = Ticket::where(function($q){
            $q->whereUserIdSenderOrUserIdReceiver($this->sender->id, $this->sender->id);
        });
        if(!is_null($search)){
            $tickets = $tickets->where('title','like',"%$search%");
        }
        $tickets = $tickets->latest()
            ->paginate($this->ticketPaginate);
        $this->tickets = $tickets;
        return $this;
    }

    public function getAllMessages($ticket_id)
    {
        $message = Message::find($ticket_id)->latest()->paginate(20);
        return $message;
    }

    /**
     * @param $user_id
     * @return Collection
     */
    public function displayInbox($user_id)
    {
        $tickets = $this->tickets;
        $collection = new Collection();
        $collection->put('render',$tickets->render());
        foreach($tickets as $ticket)
        {
            $message = $ticket->message()->where('user_id','<>',$user_id)->first();
            $read = true;
            if(!is_null($message)&&$message->read==false)
                $read = false;
            if($ticket->user_id_sender != $user_id){
                $name = $ticket->senderUser()->first()->getFullName();
            }
            else{
                $name = $ticket->ReceiverUser()->first()->getFullName();
            }
            $c = new Collection();
            $c->put('name',$name);
            $c->put('title',$ticket->title);
            $c->put('status',$ticket->status);
            $c->put('created_at',$ticket->created_at);
            $c->put('ticket_id',$ticket->id);
            $c->put('read',$read);

            $collection->push($c);
        }
        return $collection;
    }

    /**
     * @param $ticket_id
     * @return $this
     */
    public function retrieveTicketMessage($ticket_id)
    {
        $this->ticket = Ticket::find($ticket_id);
        $this->message = $this->ticket->message()->latest()->paginate($this->messagePaginate);
        return $this;
    }

    /**
     * @param $user_id
     * @param $ticket_id
     * @return bool
     */
    public function checkUserCanViewMessage($user_id,$ticket_id)
    {
        $ticket = Ticket::find($ticket_id);
        if($ticket->user_id_sender == $user_id ||$ticket->user_id_receiver==$user_id ){
            return true;
        }
        return false;
    }

    /**
     * Retrieve 4 all unread message
     * @param $user_id
     * @return $this
     */
    public function retrieveUnreadMessages($user_id)
    {
        //get message where not user_id as sender, unread and user_id is
        //in Ticket either as sender or receiver
        // Display all SQL executed in Eloquent
        $messages = Message::where('user_id','<>',$user_id)
            ->where('read',0)
            ->whereHas('ticket',function($q)
                use($user_id)
            {
                $q->where(function($q2) use($user_id) {
                    $q2->whereUserIdSenderOrUserIdReceiver($user_id,$user_id);
                });
            })
            ->orderBy('id','desc')->groupBy('ticket_id')->limit(4)->get();
        $this->messages = $messages;
        return $this;
    }




}