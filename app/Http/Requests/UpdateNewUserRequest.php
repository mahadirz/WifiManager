<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use DB;
use Validator;

class UpdateNewUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        $id = $this->route()->getParameter('id');
        //register custom rule
        //usage: banned_domain:table,column
        Validator::extend('banned_domain', function($attribute, $value, $parameters)
        {
            if($value)
            {
                $table = array_key_exists(0,$parameters)? $parameters[0]:"banned_domain";
                $column = array_key_exists(1,$parameters)? $parameters[1]:"domain";
                $domain = explode('@',$value);
                if(array_key_exists(1,$domain))
                    $domain = $domain[1];
                else
                    return false;
                $db = DB::table($table)->where($column,$domain)->first();
                if(!is_null($db))
                    return false;
            }
            return true;
        });

        if ($this->has('update_profile')) {
            return [
                'first_name' => ['required'],
                'last_name' => ['required'],
                'email' => ['required', 'email', 'unique:users,email,' . $id . ',id', 'banned_domain'],
            ];
        }
        else if($this->has('update_password')){
            return [
                'password' => ['required'],
                'password2' => ['same:password']
            ];
        }
        return [];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'banned_domain' => 'The :attribute domain is banned to be used'
        ];
    }

}
