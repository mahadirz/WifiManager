<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateGuestConstraint extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        if($this->request->has('update_constraint')){
            return [
                'total_connection' => ['required', 'integer'],
                'daily_data_quota' => ['required','integer'],
                'daily_hours_quota' => ['required','numeric'],
                'daily_time_from' => ['required','date_format:h:i A'],
                'daily_time_to' => ['required','date_format:h:i A']
            ];
        }
        return [];
	}

}
