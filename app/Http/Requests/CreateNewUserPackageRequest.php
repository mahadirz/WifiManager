<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateNewUserPackageRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'name' => ['required', 'min:5'],
            'data_limit' => ['required','integer'],
            'max_connection_limit' => ['required','integer'],
            'guest_connection_max' => ['required','integer'],
            'daily_hours_max' => ['required','numeric'],
            'daily_time_from' => ['required','date_format:h:i A'],
            'daily_time_to' => ['required','date_format:h:i A']
		];
	}

}
