<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use DB;
use Validator;

class RegisterNewUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        //register custom rule
        //usage: banned_domain:table,column
        Validator::extend('banned_domain', function($attribute, $value, $parameters)
        {
            if($value)
            {
                $table = array_key_exists(0,$parameters)? $parameters[0]:"banned_domain";
                $column = array_key_exists(1,$parameters)? $parameters[1]:"domain";
                $domain = explode('@',$value);
                if(array_key_exists(1,$domain))
                    $domain = $domain[1];
                else
                    return false;
                $db = DB::table($table)->where($column,$domain)->first();
                if(!is_null($db))
                    return false;
            }
            return true;
        });

		return [
            'username' => ['required', 'min:5','max:48','alpha_dash','unique:users,username'],
            'email' => ['required', 'email','unique:users,email','banned_domain'],
            'password' => ['required'],
            'password2' => ['same:password']

		];
	}

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'banned_domain' => 'The :attribute domain is banned to be used'
        ];
    }

}
