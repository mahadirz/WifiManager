<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use DB;
use Validator;

class RegisterNasRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nasname' => ['required', 'regex:/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}(\/\d{0,2})?$/','unique:nas,nasname'],
            'shortname' => ['required','unique:nas,shortname'],
            'secret' => ['required']

        ];
    }


}
