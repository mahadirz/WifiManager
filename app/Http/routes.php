<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as'=>'home.index','uses'=>'WelcomeController@index']);
Route::get('/event', function(){
    $response = Event::fire(new \App\Events\MessageWasCreated(
        App\Models\User::find(2),
        App\Models\User::find(37)
    ));
});
Route::get('/event', function(){
    $response = Event::fire(new \App\Events\MessageWasCreated(
        App\Models\User::find(2),
        App\Models\User::find(37)
    ));
});

//login and logout are shared for administrator, wifi manager and admin
Route::get('login', array('as' => 'login.index', 'uses' => 'LoginController@index'));
Route::post('login', array('as' => 'login.authenticate', 'uses' => 'LoginController@authenticate'));
Route::get('lockout', array('as' => 'login.lockOut', 'uses' => 'LoginController@lockOut'));

//2 factor auth
Route::get('login/tfa', array('as' => 'login.show2Fa', 'uses' => 'LoginController@show2Fa'));
Route::post('login/tfa', array('as' => 'login.postProcess2Fa', 'uses' => 'LoginController@postProcess2Fa'));

//password reset link request
Route::get('password/reset/', array('as' => 'login.getEmail', 'uses' => 'LoginController@getEmail'));
Route::post('password/reset/', array('as' => 'login.postEmail', 'uses' => 'LoginController@postEmail'));

//password reset form
Route::get('password/reset/{token}', array('as' => 'login.getReset', 'uses' => 'LoginController@getReset'));
Route::post('password/reset/{token}', array('as' => 'login.postReset', 'uses' => 'LoginController@postReset'));

Route::get('logout', array('as' => 'login.logout', 'uses' => 'LoginController@logout','middleware' => 'auth'));

Route::group(array('namespace' => 'User','middleware' => 'auth'), function()
{
    // Notice how, by nesting route groups, the namespace will automatically
    // be nested as well!

    Route::match(['get','post'],'user/register',array('as'=>'user.register.index','uses'=>'RegisterController@index'));

    //dashboards
    Route::get('user/dashboard', array('as' => 'user.dashboard.index', 'uses' => 'DashboardController@index'));
    Route::get('user/settings', array('as' => 'user.dashboard.getSettings', 'uses' => 'DashboardController@getSettings'));
    Route::post('user/settings', array('as' => 'user.dashboard.postSettings', 'uses' => 'DashboardController@postSettings'));
    Route::get('user/statistics', array('as' => 'user.dashboard.getStatistics', 'uses' => 'DashboardController@getStatistics'));

    //support
    Route::get('user/support/ticket/new', array('as' => 'user.support.getNewTicket', 'uses' => 'SupportController@getNewTicket'));
    Route::post('user/support/ticket/new', array('as' => 'manager.support.postNewTicket', 'uses' => 'SupportController@postNewTicket'));
    Route::get('user/support/ticket/{id}', array('as' => 'user.support.getTickets', 'uses' => 'SupportController@getTickets'));
    Route::post('user/support/ticket/{id}', array('as' => 'user.support.postAllTickets', 'uses' => 'SupportController@postTickets'));
    Route::get('user/support/ticket/view/{id}', array('as' => 'user.support.getViewTicket', 'uses' => 'SupportController@getViewTicket'));
    Route::post('user/support/ticket/view/{id}', array('as' => 'user.support.postViewTicket', 'uses' => 'SupportController@postViewTicket'));

    //guest
    Route::get('user/guest/view', array('as' => 'user.guest.view', 'uses' => 'GuestController@view'));
    Route::post('user/guest/settings', array('as' => 'user.guest.postSettings', 'uses' => 'GuestController@postSettings'));
    Route::get('user/guest/settings', array('as' => 'user.guest.settings', 'uses' => 'GuestController@settings'));

});

Route::group(array('namespace' => 'Manager','middleware' => 'auth'), function(){

    //index
    Route::get('manager/dashboard',array('as'=>'manager.dashboard.index','uses'=>'DashboardController@index'));

    //settings
    Route::get('manager/settings', array('as' => 'manager.dashboard.getSettings', 'uses' => 'DashboardController@getSettings'));
    Route::post('manager/settings', array('as' => 'manager.dashboard.postSettings', 'uses' => 'DashboardController@postSettings'));

    //billing
    Route::get('manager/billing/invoices', array('as' => 'manager.billing.getInvoices', 'uses' => 'BillingController@getInvoices'));
    Route::post('manager/billing/invoices', array('as' => 'manager.billing.postInvoices', 'uses' => 'BillingController@postInvoices'));
    Route::get('manager/billing/subscription', array('as' => 'manager.billing.getSubscription', 'uses' => 'BillingController@getSubscription'));
    Route::post('manager/billing/subscription', array('as' => 'manager.billing.postSubscription', 'uses' => 'BillingController@postSubscription'));
    Route::get('manager/billing/invoice/{id}', array('as' => 'manager.billing.getInvoice', 'uses' => 'BillingController@getInvoice'));
    Route::post('manager/billing/invoice/{id}', array('as' => 'manager.billing.postInvoice', 'uses' => 'BillingController@postInvoice'));

    //user account
    Route::get('manager/user/package/add', array('as' => 'manager.package.getNew', 'uses' => 'PackageController@getNew'));
    Route::post('manager/user/package/add', array('as' => 'manager.package.postNew', 'uses' => 'PackageController@postNew'));
    Route::post('manager/user/package/delete', array('as' => 'manager.package.postDelete', 'uses' => 'PackageController@postDelete'));
    Route::get('manager/user/packages', array('as' => 'manager.package.getList', 'uses' => 'PackageController@getList'));
    Route::get('manager/user/package/edit/{id}', array('as' => 'manager.package.getEdit', 'uses' => 'PackageController@getEdit'));
    Route::post('manager/user/package/edit/{id}', array('as' => 'manager.package.postEdit', 'uses' => 'PackageController@postEdit'));
    Route::get('manager/user/register', array('as' => 'manager.user.getRegister', 'uses' => 'UserController@getRegister'));
    Route::post('manager/user/register', array('as' => 'manager.user.postRegister', 'uses' => 'UserController@postRegister'));
    Route::get('manager/user/edit/{id}', array('as' => 'manager.user.getEdit', 'uses' => 'UserController@getEdit'));
    Route::get('manager/user/edit/{id}', array('as' => 'manager.user.getEditDummy', 'uses' => 'UserController@getEdit'));
    Route::post('manager/user/edit/{id}', array('as' => 'manager.user.postEdit', 'uses' => 'UserController@postEdit'));
    Route::post('manager/user/delete', array('as' => 'manager.user.postDelete', 'uses' => 'UserController@postDelete'));
    Route::get('manager/users', array('as' => 'manager.user.view', 'uses' => 'UserController@getView'));
    Route::post('manager/users', array('as' => 'manager.user.postView', 'uses' => 'UserController@postView'));

    //support
    Route::get('manager/support/ticket/new', array('as' => 'manager.support.getNewTicket', 'uses' => 'SupportController@getNewTicket'));
    Route::post('manager/support/ticket/new', array('as' => 'manager.support.postNewTicket', 'uses' => 'SupportController@postNewTicket'));
    Route::get('manager/support/ticket/{id}', array('as' => 'manager.support.getTickets', 'uses' => 'SupportController@getTickets'));
    Route::post('manager/support/ticket/{id}', array('as' => 'manager.support.postAllTickets', 'uses' => 'SupportController@postTickets'));
    Route::get('manager/support/ticket/view/{id}', array('as' => 'manager.support.getViewTicket', 'uses' => 'SupportController@getViewTicket'));
    Route::post('manager/support/ticket/view/{id}', array('as' => 'manager.support.postViewTicket', 'uses' => 'SupportController@postViewTicket'));
    Route::get('manager/support/mail', array('as' => 'manager.support.getMailBlast', 'uses' => 'SupportController@getMailBlast'));
    Route::post('manager/support/mail', array('as' => 'manager.support.postMailBlast', 'uses' => 'SupportController@postMailBlast'));
});

Route::group(array('namespace' => 'Admin','middleware' => 'auth'), function()
{
    Route::match(['get', 'post'],'admin/login', array('as' => 'admin.login.index', 'uses' => 'LoginController@index'));

    //dashboards
    Route::get('admin/dashboard', array('as' => 'admin.dashboard.index', 'uses' => 'DashboardController@index'));

    //settings
    Route::get('admin/settings', array('as' => 'admin.dashboard.getSettings', 'uses' => 'DashboardController@getSettings'));
    Route::post('admin/settings', array('as' => 'admin.dashboard.postSettings', 'uses' => 'DashboardController@postSettings'));

    //Billing
    Route::get('admin/billing/payments', array('as' => 'admin.billing.getPayments', 'uses' => 'BillingController@getPayments'));
    Route::post('admin/billing/payments', array('as' => 'admin.billing.postPayments', 'uses' => 'BillingController@postPayments'));


    //access point
    Route::get('admin/access-point/register', array('as' => 'admin.accessPoint.getRegister', 'uses' => 'AccessPointController@getRegister'));
    Route::post('admin/access-point/register', array('as' => 'admin.accessPoint.postRegister', 'uses' => 'AccessPointController@postRegister'));
    Route::get('admin/access-point', array('as' => 'admin.accessPoint.getList', 'uses' => 'AccessPointController@getList'));
    Route::post('admin/access-point/delete', array('as' => 'admin.accessPoint.postDelete', 'uses' => 'AccessPointController@postDelete'));
    Route::get('admin/access-point/update/{id}', array('as' => 'admin.accessPoint.getUpdate', 'uses' => 'AccessPointController@getUpdate'));
    Route::post('admin/access-point/update/{id}', array('as' => 'admin.accessPoint.postUpdate', 'uses' => 'AccessPointController@postUpdate'));
    Route::get('admin/access-point/radius', array('as' => 'admin.accessPoint.getRadius', 'uses' => 'AccessPointController@getRadius'));

    Route::post('admin/json/radius/status', array('as' => 'admin.accessPoint.postRadiusStatus', 'uses' => 'AccessPointController@postRadiusStatus'));
    Route::post('admin/json/radius/action', array('as' => 'admin.accessPoint.postRadiusAction', 'uses' => 'AccessPointController@postRadiusAction'));
    Route::post('admin/json/radius/monitor', array('as' => 'admin.accessPoint.postRadiusMonitor', 'uses' => 'AccessPointController@postRadiusMonitor'));

    //wi-fi manager
    Route::get('admin/package/new', array('as' => 'admin.package.getNew', 'uses' => 'PackageController@getNew'));
    Route::post('admin/package/new', array('as' => 'admin.package.postNew', 'uses' => 'PackageController@postNew'));
    Route::get('admin/package/edit/{id}', array('as' => 'admin.package.getEdit', 'uses' => 'PackageController@getEdit'));
    Route::post('admin/package/edit/{id}', array('as' => 'admin.package.postEdit', 'uses' => 'PackageController@postEdit'));
    Route::post('admin/package/delete', array('as' => 'admin.package.postDelete', 'uses' => 'PackageController@postDelete'));
    Route::get('admin/package/', array('as' => 'admin.package.getList', 'uses' => 'PackageController@getList'));

    Route::get('admin/manager', array('as' => 'admin.manager.getView', 'uses' => 'ManagerController@getView'));
    Route::get('admin/manager/register', array('as' => 'admin.manager.getRegister', 'uses' => 'ManagerController@getRegister'));
    Route::post('admin/manager/register', array('as' => 'admin.manager.postRegister', 'uses' => 'ManagerController@postRegister'));
    Route::post('admin/manager/delete', array('as' => 'admin.manager.postDelete', 'uses' => 'ManagerController@postDelete'));
    Route::get('admin/manager/edit/{id}', array('as' => 'admin.manager.getEdit', 'uses' => 'ManagerController@getEdit'));
    Route::post('admin/manager/edit/{id}', array('as' => 'admin.manager.postEdit', 'uses' => 'ManagerController@postEdit'));
    Route::get('admin/manager/edit/', array('as' => 'admin.manager.getEditDummy'));

    //support
    Route::get('admin/support/ticket/new', array('as' => 'admin.support.getNewTicket', 'uses' => 'SupportController@getNewTicket'));
    Route::post('admin/support/ticket/new', array('as' => 'admin.support.postNewTicket', 'uses' => 'SupportController@postNewTicket'));
    Route::get('admin/support/ticket/{id}', array('as' => 'admin.support.getTickets', 'uses' => 'SupportController@getTickets'));
    Route::post('admin/support/ticket/{id}', array('as' => 'admin.support.postAllTickets', 'uses' => 'SupportController@postTickets'));
    Route::get('admin/support/ticket/view/{id}', array('as' => 'admin.support.getViewTicket', 'uses' => 'SupportController@getViewTicket'));
    Route::post('admin/support/ticket/view/{id}', array('as' => 'admin.support.postViewTicket', 'uses' => 'SupportController@postViewTicket'));
    Route::get('admin/support/mail', array('as' => 'admin.support.getMailBlast', 'uses' => 'SupportController@getMailBlast'));
    Route::post('admin/support/mail', array('as' => 'admin.support.postMailBlast', 'uses' => 'SupportController@postMailBlast'));
});

Route::group(array('namespace' => 'Ajax','middleware' => 'auth'), function()
{
    Route::post('ajax/validate/username', array('as' => 'ajax.postUsername', 'uses' => 'validator@postUsername'));
    Route::post('ajax/validate/email', array('as' => 'ajax.postEmail', 'uses' => 'validator@postEmail'));
    Route::post('ajax/upload/profile', array('as' => 'ajax.postUploadProfile', 'uses' => 'uploader@postProfile'));
    Route::post('ajax/security/2fA/QRCode', array('as' => 'ajax.postGenerate2FaQRCodeUrl', 'uses' => 'Security@postGenerate2FaQRCodeUrl'));
    Route::post('ajax/security/2fA/verify', array('as' => 'ajax.postVerify2FaQRCode', 'uses' => 'Security@postVerify2FaQRCode'));
    Route::post('ajax/security/2fA/disable', array('as' => 'ajax.postDisable2Fa', 'uses' => 'Security@postDisable2Fa'));

    Route::post('ajax/autocomplete/sender', array('as' => 'ajax.postReceiverAutoComplete', 'uses' => 'AutoComplete@postReceiverAutoComplete'));
    Route::post('ajax/support/messages', array('as' => 'ajax.postMessages', 'uses' => 'Support@postMessages'));
});

// only user will that have roles can access certain routes
Entrust::routeNeedsRole('user/*', 'user',Redirect::route('login.index'));
Entrust::routeNeedsRole('admin/*', 'administrator',Redirect::route('login.index'));
Entrust::routeNeedsRole('manager/*', 'wifimanager',Redirect::route('login.index'));

Route::get('preventClose',function(){
    return '<script>window.onbeforeunload = function() { return "You are closing all tabs!"; }</script>';
});

Route::get('dummy/wifimanager',function(){
    $total = 1;
    if(\Request::has('total')){
        $total = \Request::get('total');
    }
    $a = \Laracasts\TestDummy\Factory::times($total)->create('App\Models\User');

    $manager = \App\Models\Role::whereName('wifimanager')->first();
    foreach($a as $user){
        $user->assignRole($manager);
    }

});
