<?php namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateNewUserPackageRequest;
use App\Models\UserPackage;
use App\Models\WifiManagerPackages;
use Auth;
use Carbon\Carbon;
use Request;
use Session;
use Validator;

class PackageController extends Controller {

    /**
     * @return int
     */
    private function getMaxConnection()
    {
        try{
            return Auth::user()->subscription->subscribable->max_connections_per_user;
        }
        catch(\Exception $e){
            return 0;
        }
    }

	public function getNew()
    {
        $maxConnection = $this->getMaxConnection();
        return view('manager.packages.package')->with('maxConnection',$maxConnection);
    }

    /**
     * @param CreateNewUserPackageRequest $input
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postNew(CreateNewUserPackageRequest $input)
    {
        $maxConnection = $this->getMaxConnection();

        if(($input->get('max_connection_limit')+$input->get('guest_connection_max'))>$maxConnection){
            //error, user trying to cheat
            Session::flash('data',['alert'=>'danger','msg'=>'Maximum user & connection limit exceed the subscription quota']);
            return redirect()->back();
        }

        if(!is_null(Auth::user()->userPackage()->where('name','Basic')->first())){
            //duplicate name
            Session::flash('data',['alert'=>'danger','msg'=>'The basic package already exist!']);
            return redirect()->back();
        }

        $request = $input->input();
        if(Request::has('daily_limit_enabled'))
            $request['daily_limit_enabled'] = true;
        else
            $request['daily_limit_enabled'] = false;
        //convert time to 24h
        $request ['daily_time_to'] = Carbon::parse($request['daily_time_to'])->format('H:i:s');
        $request ['daily_time_from'] = Carbon::parse($request['daily_time_from'])->format('H:i:s');

        try {
            Auth::user()->userPackage()->create($request);

        } catch (\Exception $e) {
            Session::flash('data',['alert'=>'danger','msg'=>'Error! '.$e->getMessage()]);
            return redirect()->back();
        }

        Session::flash('data',['alert'=>'success','msg'=>'New package created!']);
        return redirect()->back();
    }

    /**
     * @param $id
     * @return $this
     */
    public function getEdit($id)
    {
        $package = UserPackage::whereId($id)->first();
        $maxConnection = $this->getMaxConnection();
        return view('manager.packages.edit')
            ->with('package',$package)
            ->with('maxConnection',$maxConnection)
            ->with('menu_id_open','view-packages');
    }

    public function postEdit(CreateNewUserPackageRequest $input,$id)
    {
        $maxConnection = $this->getMaxConnection();

        if(($input->get('max_connection_limit')+$input->get('guest_connection_max'))>$maxConnection){
            //error, user trying to cheat
            Session::flash('data',['alert'=>'danger','msg'=>'Maximum user & connection limit exceed the subscription quota']);
            return redirect()->back();
        }

        $request = $input->input();
        if($input->has('daily_limit_enabled'))
            $request['daily_limit_enabled'] = true;
        else
            $request['daily_limit_enabled'] = false;
        //convert time to 24h
        $request ['daily_time_to'] = Carbon::parse($request['daily_time_to'])->format('H:i:s');
        $request ['daily_time_from'] = Carbon::parse($request['daily_time_from'])->format('H:i:s');

        try {
            $userpackage = UserPackage::find($id);
            $userpackage->update($request);
        } catch (\Exception $e) {
            Session::flash('data',['alert'=>'danger','msg'=>'Package with the same name already exist']);
            return redirect()->back();
        }

        Session::flash('data',['alert'=>'success','msg'=>'Package updated!']);
        return redirect()->back();
    }

    /**
     * List of all package under current user
     * @return $this
     */
    public function getList()
    {
        $packages = Auth::user()->UserPackage()->paginate(10);
        return view('manager.packages.view')->with('packages',$packages);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function postDelete()
    {
        $package_id= Request::get('package_id');
        /** @var WifiManagerPackages $package */
        $package = UserPackage::find($package_id);
        try{
            $package->delete();
            return "OK";
        }
        catch(\Exception $e){
            return $e->getMessage();
        }
    }

}
