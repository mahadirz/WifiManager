<?php namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Profile;
use App\Models\User;
use Auth;
use DB;
use Hash;
use Request;
use JavaScript;

class DashboardController extends Controller {

    public function __construct()
    {
        JavaScript::put([
            'user_id' => Auth::id(),
            'csrf_token'=>csrf_token()
        ]);
    }

    private function generateProgressBar($current,$limit)
    {
        if($limit ==0)
            $limit = 9999999999;
        $percentage = ($current/$limit) * 100;
        $output['progressBar_percentage'] = $percentage;
        if($percentage > 70)
            $output['progressBar_label'] = "progress-bar-danger";
        else if($percentage > 40)
            $output['progressBar_label'] = "progress-bar-warning";
        else
            $output['progressBar_label'] = "progress-bar-success";
        return $output;
    }

    /**
     * Dashboard home page
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();
        $sql = "SELECT count(*) as total FROM `manager` WHERE manager_id = ".$user->id;
        $total_registered_user = DB::select($sql);
        $total_registered_user = is_array($total_registered_user)?$total_registered_user[0]->total:0;

        $sql = "SELECT count(*) as total FROM `tickets` WHERE (user_id_sender = ".$user->id." OR user_id_receiver = ".$user->id.") AND status = 'open'";
        $total_opened_tickets = DB::select($sql);
        $total_opened_tickets = is_array($total_opened_tickets)?$total_opened_tickets[0]->total:0;

        $sql = "SELECT count(*) as total FROM `invoices` WHERE user_id=".$user->id." AND status='unpaid'";
        $total_unpaid_invoices= DB::select($sql);
        $total_unpaid_invoices = is_array($total_unpaid_invoices)?$total_unpaid_invoices[0]->total:0;
        $widgets = [
            ["count"=>$total_registered_user,"title"=>"REGISTERED USERS","subtitle"=>"under your management","icon"=>"fa-user"],
            ["count"=>$total_opened_tickets,"title"=>"OPENED TICKETS","subtitle"=>"In your supports","icon"=>"fa-envelope"],
            ["count"=>$total_unpaid_invoices,"title"=>"UNPAID ITEMS","subtitle"=>"In your invoices","icon"=>"fa-briefcase"],
        ];

        $sql = "SELECT up.acctinterim,max_users,suspended
            FROM `users` as m
            JOIN `subscriptions` as user_s ON user_s.user_id = m.id
            JOIN `wifiManagerPackages` as up ON up.id = user_s.subscribable_id
            WHERE m.id = '".$user->id."' LIMIT 1";
        $accounting = DB::select($sql)[0];


        $user_limit = array("used"=>$total_registered_user,"limit"=>$accounting->max_users);
        $tmp = $this->generateProgressBar($total_registered_user,$accounting->max_users);
        $user_limit = array_merge($user_limit,$tmp);

        if($accounting->suspended){
            $status['text'] = "Suspended";
            $status['label'] = "label-danger";
        }
        else{
            $status['text'] = "Active";
            $status['label'] = "label-success";
        }

        $accountInfo = [
            "user_limit" => $user_limit,
            "acct_interim" => $accounting->acctinterim,
            "status" => $status,
        ];
        return view('manager.index')->with("accountInfo",$accountInfo)->with("widgets",$widgets);
    }

    /**
     * @return $this
     */
    public function getSettings()
    {
        $user = User::find(Auth::id());
        return view('manager.settings')->with('user',$user);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSettings()
    {
        $user = User::find(Auth::id());
        if(Request::has('update_profile'))
        {
            $user->update(Request::all());
            $data = array('alert'=>'info','msg'=>'The Profile has been updated','title'=>'Success!');
        }
        else if(Request::has('update_email'))
        {
            if(Hash::check(Request::get('password'),$user->password) ){
                $user->email = Request::get('email');
                $user->save();
                $data = array('alert'=>'info','msg'=>'The email has been updated','title'=>'Success!');
            }
            else{
                $data = array('alert'=>'danger','msg'=>'Password incorrect!','title'=>'Error!');
            }

        }
        else if(Request::has('update_signature')){
            $profile = Profile::firstOrNew(['user_id'=>$user->id]);
            $profile->signature = Request::get('signature');
            $profile->user_id = $user->id;
            $profile->save();
            $data = array('alert'=>'info','msg'=>'The Signature has been updated','title'=>'Success!');
        }
        else
        {
            $data = array('alert'=>'danger','msg'=>'Error performing actions!','title'=>'Error!');
        }

        return redirect()->route('manager.dashboard.getSettings')->with('data',$data);
    }

}
