<?php namespace App\Http\Controllers\Manager;

use App\Events\UserPasswordWasChanged;
use App\Events\UserWasRegistered;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Subscription;
use DB;
use Radius;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Models\UserPackage;
use App\Models\WifiManagerPackages;
use Auth;
use Carbon\Carbon;
use Event;
use Illuminate\Http\Request;
use SSH;
use Session;
use JavaScript;
use Billing;

class UserController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        $user = Auth::user();
        $sql = "SELECT count(*) as total FROM `manager` WHERE manager_id = ".$user->id;
        $total_registered_user = DB::select($sql);
        $total_registered_user = is_array($total_registered_user)?$total_registered_user[0]->total:0;

        $sql = "SELECT up.acctinterim,max_users,suspended
            FROM `users` as m
            JOIN `subscriptions` as user_s ON user_s.user_id = m.id
            JOIN `wifiManagerPackages` as up ON up.id = user_s.subscribable_id
            WHERE m.id = '".$user->id."' LIMIT 1";
        $accounting = DB::select($sql)[0];
        $user_limit = array("used"=>$total_registered_user,"limit"=>$accounting->max_users);
        $packages = Auth::user()->UserPackage;
        return view('manager.user.register')->with('packages',$packages)->with("user_limit",$user_limit);
    }

    /**
     * @param Requests\RegisterNewUserRequest|Request $request
     * @return $this
     */
    public function postRegister(Requests\RegisterNewUserRequest $request)
    {
        //dd($request->all());
        $input = $request->all();
        $user = User::create($input);
        $user->username = $input['username'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);

        $user->profile()->create($input);
        $user->save();

        $user->assignManager(null,Auth::id());

        if(isset($input['package_id']) && intval($input['package_id'])){
            //since it is free automatically subscribe for a year
            Billing::setUser($user)->subscribe($input['package_id'],'App\Models\UserPackage',Carbon::now()->addYear());
        }

        //attach role
        $role = Role::whereName('user')->first();
        $user->attachRole($role);

        //fire event UserWasRegistered
        Event::fire(new UserWasRegistered($user,$input['password']));

        $data = array('alert'=>'info','msg'=>'The user has been registered!');
        return redirect()->route('manager.user.getRegister')->with('data',$data);

    }

    /**
     * List of all users
     * @return \Illuminate\View\View
     */
    public function getView()
    {
        $users = Auth::user()->manage();
        //dd($users->get('users')[0]->subscription->subscribable);
        return view('manager.user.view')->with('users',$users);
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function postView(Request $request)
    {
        $users = Auth::user()->manage(10,$request->get('search'));
        return view('manager.user.view')->with('users',$users);
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function postDelete(Request $request)
    {
        try{
            /** @var User $user */
            $user = User::find($request->input('user_id'));
            $user->delete();
            //clean up radcheck
            Radius::setUsername($user->username)->deleteAll();
            return "OK";
        }
        catch(\Exception $e){
            return "failed";
        }
    }

    private function generateProgressBar($current,$limit){
        if($limit ==0)
            $limit = 9999999999;
        $percentage = ($current/$limit) * 100;
        $output['progressBar_percentage'] = $percentage;
        if($percentage > 70)
            $output['progressBar_label'] = "progress-bar-danger";
        else if($percentage > 40)
            $output['progressBar_label'] = "progress-bar-warning";
        else
            $output['progressBar_label'] = "progress-bar-success";
        return $output;
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function getEdit(Request $request,$id)
    {
        $user = User::with(['subscription','profile'])->find($id);
        $packages = UserPackage::all();
        //dd($user->profile);
        JavaScript::put([
            'user_id' => $id,
            'package' => is_null($user->subscription)? 'undefined':$user->subscription->subscribable,
        ]);

        //get accounting information
        $sql = "SELECT u.id as user_id,CONCAT(m_u.first_name,\" \",m_u.last_name) as manager_name, manager_s.ends_at as manager_ends_at,
        manager_s.suspended as manager_suspended, user_s.suspended as user_suspended,
        manager_s.starts_at as manager_starts_at,user_s.starts_at as user_starts_at,
        user_s.ends_at as user_ends_at, up.data_limit,up.max_connection_limit,
        up.daily_limit_enabled,up.daily_data_limit,up.daily_hours_max,up.daily_time_from,
        up.daily_time_to
        FROM `manager` as m
        JOIN `users` as m_u ON m_u.id = m.manager_id
        JOIN `users` as u ON u.id = m.user_id
        JOIN `subscriptions` as manager_s ON manager_s.user_id = m.manager_id
        JOIN `subscriptions` as user_s ON user_s.user_id = m.user_id
        JOIN `userPackages` as up ON up.id = user_s.subscribable_id
        WHERE u.username = '$user->username' LIMIT 1";
        $accounting = DB::select($sql);
        $accountInfo = array();
        if(count($accounting) > 0){
            $accounting = $accounting[0];
            //Account Status	Active
            $status['text'] = "active";
            $status['label'] = "label-success";
            if($accounting->user_suspended == 1){
                $status['text'] = "Suspended";
                $status['label'] = "label-danger";
            }
            else if($accounting->manager_suspended == 1){
                $status['text'] = "Manager Suspended";
                $status['label'] = "label-warning";
            }

            //get current connection to NAS
            $simulConn = Radius::getConnectionTotal($user->username);
            if($simulConn>0){
                $connection_status['text'] = "Connected";
                $connection_status['label'] = "label-info";
            }
            else{
                $connection_status['text'] = "Disconnected";
                $connection_status['label'] = "label-primary";
            }

            //is daily limit enabled
            $daily_data_limit_enabled = $accounting->daily_limit_enabled;

            //calculate daily data quota
            $tmp = Radius::Bytes_converter(Radius::getDailyDataUsage($user->username));
            $dailyDataUsage['used'] = $tmp;
            if($daily_data_limit_enabled) {
                $tmp = $this->generateProgressBar(Radius::getDailyDataUsage($user->username),($accounting->daily_data_limit*1048576));
                if($accounting->daily_data_limit==0) {
                    $dailyDataUsage['limit'] = "&infin;";
                    $dailyDataUsage['progressBar_percentage'] = 10;
                    $dailyDataUsage['progressBar_label'] = "progress-bar-success";
                }
                else {
                    $dailyDataUsage['limit'] = Radius::Bytes_converter($accounting->daily_data_limit);
                    $dailyDataUsage = array_merge($dailyDataUsage,$tmp);
                }
            }
            else {
                $dailyDataUsage['progressBar_percentage'] = 10;
                $dailyDataUsage['progressBar_label'] = "progress-bar-success";
                $dailyDataUsage['limit'] = "&infin;";
            }

            //calculate daily hours quota
            $tmp = Radius::getDailyHoursUsage($user->username);
            $dailyHoursUsage['used'] = $tmp;
            if($daily_data_limit_enabled) {
                $tmp = $this->generateProgressBar(Radius::getDailyHoursUsage($user->username),($accounting->daily_hours_max));
                if($accounting->daily_data_limit==0) {
                    $dailyHoursUsage['limit'] = "&infin;";
                    $dailyHoursUsage['progressBar_percentage'] = 10;
                    $dailyHoursUsage['progressBar_label'] = "progress-bar-success";
                }
                else {
                    $dailyHoursUsage['limit'] = $accounting->daily_hours_max;
                    $dailyHoursUsage = array_merge($dailyHoursUsage,$tmp);
                }
            }
            else {
                $dailyHoursUsage['progressBar_percentage'] = 10;
                $dailyHoursUsage['progressBar_label'] = "progress-bar-success";
                $dailyHoursUsage['limit'] = "&infin;";
            }

            //connection limit
            $connection_limit['used'] = $simulConn;
            $connection_limit['limit'] = $accounting->max_connection_limit;
            $tmp = $this->generateProgressBar($simulConn,$accounting->max_connection_limit);
            $connection_limit = array_merge($connection_limit,$tmp);

            //daily connection hour
            $daily_connection_hour = Carbon::parse($accounting->daily_time_from)->format("h:i a")." - ".
                Carbon::parse($accounting->daily_time_to)->format("h:i a");

            $accountInfo = [
                "manager" => $accounting->manager_name,
                "status" => $status,
                "connection_status" => $connection_status,
                "daily_data_usage" => $dailyDataUsage,
                "daily_hours_usage" => $dailyHoursUsage,
                "connection_limit" => $connection_limit,
                "daily_connection_hour" => $daily_connection_hour,
                "daily_data_limit_enabled" => $daily_data_limit_enabled,
                "data" => array(
                    "transfer" =>Radius::Bytes_converter(Radius::getAcctInputOutputOctets($user->username)),
                    "upload" =>Radius::Bytes_converter(Radius::getAcctInputOutputOctets($user->username,"upload")),
                    "download" =>Radius::Bytes_converter(Radius::getAcctInputOutputOctets($user->username,"download")),
                )
            ];
        }


        return view('manager.user.edit')
            ->with('user',$user)
            ->with('packages',$packages)
            ->with('menu_id_open','view-user')
            ->with("accountInfo",$accountInfo);
    }

    public function postEdit(Requests\UpdateNewUserRequest $request,$id)
    {
        $input = $request->all();
        /** @var User $user */
        $user = User::find($id);
        if($request->has('update_profile'))
        {
            //dd($input);
            $user->update($input);
            $user->email = $input['email'];
            $profile = Profile::whereUserId($id)->first();
            $profile->update($input);
            $user->profile()->save($profile);
            $user->save();

            //only package_id > 0
            if(intval($input['package_id']))
                Billing::setUser($user)->subscribe($input['package_id'],'App\Models\UserPackage',Carbon::now()->addYear());
            else
                Billing::setUser($user)->unsubscribe();

            $data = array('alert'=>'info','msg'=>'The profile has been updated!','title'=>'Success!');
        }
        else if($request->has('update_password'))
        {
            $user->password = bcrypt($input['password']);
            $user->save();
            //fire UserPasswordWasChanged event
            Event::fire(new UserPasswordWasChanged($user,$input['password']));
            $data = array('alert'=>'info','msg'=>'The password has been updated!','title'=>'Success!');
        }
        else if($request->has('suspend_account'))
        {
            $sub = Subscription::where('user_id',$user->id)->first();
            $sub->suspended = 1;
            $sub->save();
            $data = array('alert'=>'info','msg'=>'User account has been suspended!','title'=>'Success!');
            //dispatch event
            Event::fire(new \App\Events\UserHasBeenBlockedToNas($user->username));
        }
        else if($request->has('activate_account'))
        {
            $sub = Subscription::where('user_id',$user->id)->first();
            $sub->suspended = 0;
            $sub->save();
            $data = array('alert'=>'info','msg'=>'User account has been activated!','title'=>'Success!');
        }
        else
        {
            $data = array('alert'=>'danger','msg'=>'Invalid form','title'=>'Error!');
        }

        return redirect()->route('manager.user.getEdit',$id)->with('data',$data);
    }


}
