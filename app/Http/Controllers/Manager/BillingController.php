<?php namespace App\Http\Controllers\Manager;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Libraries\Billings\PaymentGateway;
use App\Libraries\Billings\PaypalGateway;
use App\Models\Invoice;
use Billing;
use App\Models\Configuration;
use App\Models\WifiManagerPackages;
use Auth;
use Request;
use URL;

class BillingController extends Controller {

    public function getInvoices()
    {
        $invoices = Billing::invoices();
        return view('manager.billing.invoices')
            ->with('invoices',$invoices)
            ->with('billing',Billing::getInstance());
    }

    public function postInvoices()
    {
        if(Request::has('invoice_id')){
            $invoice = Invoice::find(Request::get('invoice_id'));
            $invoice->status = "cancelled";
            $invoice->save();
            $data = ['alert'=>'success','title'=>'Success','msg'=>'Invoice has been cancelled'];
            return redirect()->route('manager.billing.getInvoices')->with('data',$data);
        }
        return redirect()->route('manager.billing.getInvoices');
    }

    public function getSubscription()
    {
        $subscription = Billing::subscription();
        $packages = WifiManagerPackages::all();
        //dd($subscription->subscribable->name);
        return view('manager.billing.subscription')
            ->with('subscription',$subscription)
            ->with('packages',$packages);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSubscription()
    {
        if(Request::has('subscribe')){
            if(!Request::has('package_id')){
                $data = ['alert'=>'danger','title'=>'Error','msg'=>'Please select package'];
                return redirect()->route('manager.billing.getSubscription')->with('data',$data);
            }
            Billing::subscribe(Request::get('package_id'),'App\Models\WifiManagerPackages')->invoice();
            $data = ['alert'=>'success','title'=>'Success','msg'=>'Subscribed to new package, please check your invoice'];
            return redirect()->route('manager.billing.getSubscription')->with('data',$data);
        }
        elseif (Request::has('subscription')){
            //renew or cancel
            if(Request::get('action')=='renew'){
                //renew
                Billing::renew();
                $data = ['alert'=>'success','title'=>'Success','msg'=>'New invoice created!'];
                return redirect()->route('manager.billing.getSubscription')->with('data',$data);
            }
            else{
                //cancel
                Billing::unSubscribe();
                $data = ['alert'=>'success','title'=>'Success','msg'=>'Subscription has been cancelled, the new invoice will not be generated for renewal!'];
                return redirect()->route('manager.billing.getSubscription')->with('data',$data);
            }
        }

        return redirect()->route('manager.billing.getSubscription');
    }

    /**
     * @param $id
     * @return $this
     */
    public function getInvoice($id)
    {
        if(Request::has('success') && Request::get('success')=="true"){
            Billing::verifyGatewayPayment(Request::instance());
        }
        $billing = Billing::findInvoice($id);
        if($billing->getInvoice()->status=="paid")
            return redirect()->route('manager.billing.getInvoices');
        return view('manager.billing.invoice')->with('billing',$billing);
    }

    /**
     * Redirect to payment gateway
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postInvoice($id)
    {
        Billing::findInvoice($id);
        try {
            Billing::initPaymentGateway(Request::get('payment_method'),Request::get('cycle'),URL::current()."?success=true",URL::current()."?success=false")->pay();
        } catch (\Exception $e) {
            $data = ['alert'=>'danger','title'=>'Error','msg'=>$e->getMessage()];
            return redirect()->route('manager.billing.getInvoice')->with('data',$data);
        }
    }

}
