<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\User;
use Auth;
use DB;
use Form;
use Hash;
use JavaScript;
use PhpParser\Node\Expr\Cast\Object_;
use Request;
use SSH;


class DashboardController extends Controller
{

    /**
     * Constructor
     */
    public function __construct()
    {
        JavaScript::put([
            'user_id' => Auth::id(),
            'csrf_token'=>csrf_token()
        ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = Auth::user();

        $sql = "SELECT r.name,count(*) as total FROM `users` as u
        JOIN role_user as ru ON ru.user_id = u.id
        JOIN roles as r ON r.id = ru.role_id
        GROUP BY r.name";
        $statistic_users = DB::select($sql);

        $sql = "SELECT count(*) as total FROM `tickets` WHERE (user_id_sender = ".$user->id." OR user_id_receiver = ".$user->id.") AND status = 'open'";
        $total_opened_tickets = DB::select($sql);
        $total_opened_tickets = is_array($total_opened_tickets)?$total_opened_tickets[0]->total:0;

        $output = "";
        SSH::run(['ps -ef |grep -v grep |grep -cw freeradius'], function($line)
        use (&$output)
        {
            $output = $line.PHP_EOL;
        });

        $accountInfo = array();

        if(intval($output)>0){
            $accountInfo['radius_status']['text'] = 'Running';
            $accountInfo['radius_status']['label'] = 'label-success';
        }
        else{
            $accountInfo['radius_status']['text'] = 'Stopped';
            $accountInfo['radius_status']['label'] = 'label-danger';
        }

        $accountInfo['created_at'] = $user->created_at;
        $accountInfo['updated_at'] = $user->updated_at;

        $widgets = [
            ["count"=>$statistic_users[2]->total,"title"=>"REGISTERED WIFI MANAGER","subtitle"=>"under your management","icon"=>"fa-users"],
            ["count"=>$statistic_users[1]->total,"title"=>"REGISTERED USERS","subtitle"=>"under your management","icon"=>"fa-user"],
            ["count"=>$total_opened_tickets,"title"=>"OPENED TICKETS","subtitle"=>"In your supports","icon"=>"fa-envelope"],
        ];
        return view('admin.index')->with("accountInfo",$accountInfo)->with("widgets",$widgets);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function settings()
    {
        return view('admin.settings');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function statistics()
    {
        return view('admin.statistics');
    }

    /**
     * @return $this
     */
    public function getSettings()
    {
        $user = User::find(Auth::id());
        return view('admin.settings')->with('user',$user);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSettings()
    {
        $user = User::find(Auth::id());
        if(Request::has('update_profile'))
        {
            $user->update(Request::all());
            $data = array('alert'=>'info','msg'=>'The Profile has been updated','title'=>'Success!');
        }
        else if(Request::has('update_email'))
        {
            if(Hash::check(Request::get('password'),$user->password) ){
                $user->email = Request::get('email');
                $user->save();
                $data = array('alert'=>'info','msg'=>'The email has been updated','title'=>'Success!');
            }
            else{
                $data = array('alert'=>'danger','msg'=>'Password incorrect!','title'=>'Error!');
            }

        }
        else if(Request::has('update_signature')){
            $profile = Profile::firstOrNew(['user_id'=>$user->id]);
            $profile->signature = Request::get('signature');
            $profile->user_id = $user->id;
            $profile->save();
            $data = array('alert'=>'info','msg'=>'The Signature has been updated','title'=>'Success!');
        }
        else
        {
            $data = array('alert'=>'danger','msg'=>'Error performing actions!','title'=>'Error!');
        }

        return redirect()->route('admin.dashboard.getSettings')->with('data',$data);
    }

}
