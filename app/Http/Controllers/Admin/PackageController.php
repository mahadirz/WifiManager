<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\WifiManagerPackages;
use Illuminate\Http\Request;

class PackageController extends Controller {

	public function getNew()
    {
        return view('admin.packages.package')->with('title','Add New Package');
    }

    public function postNew(Request $request)
    {
        $input = $request->input();
        isset($input['enable_user_support'])? $input['enable_user_support'] = true : $input['enable_user_support'] = false;
        isset($input['enable_user_billing'])? $input['enable_user_billing'] = true : $input['enable_user_billing'] = false;
        $package = new WifiManagerPackages();
        $package::create($input);
        $data = array('alert'=>'info','msg'=>'The package has been created','title'=>'Success!');
        return view('admin.packages.package')
            ->with('data',$data)
            ->with('title','Add New Package');
    }

    /**
     * @param $id
     * @return $this
     */
    public function getEdit($id)
    {
        $package = WifiManagerPackages::whereId($id)->first();
        $input = $package->toArray();
        return view('admin.packages.package')
            ->with('title','Edit Package')
            ->with('input',$input);
    }

    public function postEdit(Request $request,$id)
    {
        $input = $request->input();
        isset($input['enable_user_support'])? $input['enable_user_support'] = true : $input['enable_user_support'] = false;
        isset($input['enable_user_billing'])? $input['enable_user_billing'] = true : $input['enable_user_billing'] = false;
        /** @var WifiManagerPackages $package */
        //dd($input);
        $package = WifiManagerPackages::find($id);
        $package->update($input);
        $data = array('alert'=>'info','msg'=>'The package has been updated','title'=>'Success!');
        return view('admin.packages.package')
            ->with('data',$data)
            ->with('title','Edit Package')
            ->with('input',$package->toArray());
    }

    /**
     * @return $this
     */
    public function getList()
    {
        $packages = WifiManagerPackages::paginate(10);
        return view('admin.packages.view')->with('packages',$packages);
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function postDelete(Request $request)
    {
        $package_id= $request->input('package_id');
        /** @var WifiManagerPackages $package */
        $package = WifiManagerPackages::find($package_id);
        try{
            $package->delete();
            return "OK";
        }
        catch(\Exception $e){
            return $e->getMessage();
        }

    }

}
