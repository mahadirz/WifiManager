<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Models\WifiManagerPackages;
use DB;
use Illuminate\Http\Request;
use SSH;
use Session;
use JavaScript;
use Billing;
use Radius;

class ManagerController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        $packages = WifiManagerPackages::all();
        //$data = array('alert'=>'info','msg'=>'The Manager has been registered!','title'=>'Success!');
        //Session::flash('data',$data);
        return view('admin.manager.register')->with('packages',$packages->toArray());
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function postRegister(Request $request)
    {
        //dd(intval($request->get('package_id')));
        //User::whereUsername($request->input('username'))->delete();
        $input = $request->all();
        $user = User::create($input);
        $user->username = $input['username'];
        $user->email = $input['email'];
        $user->password = bcrypt($input['password']);

        //$profile = Profile::create($input);
        $user->profile()->create($input);
        $user->save();

        //only package_id > 0
        if(intval($input['package_id']) )
            Billing::setUser($user)->subscribe($request->get('package_id'),'App\Models\WifiManagerPackages')->invoice();


        //attach role
        $role = Role::whereName('wifimanager')->first();
        $user->attachRole($role);

        $data = array('alert'=>'info','msg'=>'The Manager has been registered!','title'=>'Success!');
        return redirect()->route('admin.manager.getRegister')->with('data',$data);

    }

    /**
     * List of Wi-Fi Manager
     * @return \Illuminate\View\View
     */
    public function getView()
    {
        $managers = User::with(['subscription','roles'])
            ->whereHas('roles',function($q){
                $q->where('name','wifimanager');
            })
            ->paginate(10);
        $package = new WifiManagerPackages();
        return view('admin.manager.view')
            ->with('managers',$managers)
            ->with('package',$package);
    }

    /**
     * Delete manager
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function postDelete(Request $request)
    {
        try{
            $user = User::find($request->input('user_id'));
            //find the users under the manager
            $end_users = DB::table('manager')->where('manager_id',$user->id)->get();
            //delete all the users
            foreach($end_users as $u){
                $end_user = User::find($u->user_id);
                Radius::setUsername($end_user->username)->deleteAll();
                $end_user->delete();
            }
            $user->delete();
            return "OK";
        }
        catch(\Exception $e){
            return $e->getMessage()."\n".$e->getTraceAsString();
        }
    }

    public function getEdit(Request $request,$id)
    {
        $user = User::with(['subscription','profile'])->find($id)->toArray();
        $packages = WifiManagerPackages::all();
        JavaScript::put([
            'user_id' => $id,
            'package' => $user['subscription']
        ]);
        //dd($user);
        return view('admin.manager.edit')
            ->with('user',$user)
            ->with('packages',$packages->toArray());
    }

    public function postEdit(Request $request,$id)
    {
        $input = $request->all();
        $user = User::find($id);
        if($request->has('update_profile'))
        {
            //dd($input);
            $user->update($input);
            $user->email = $input['email'];
            $profile = Profile::whereUserId($id)->first();
            $profile->update($input);
            $user->profile()->save($profile);
            $user->save();

            //only package_id > 0
            if(intval($input['package_id']))
                Billing::setUser($user)->subscribe($request->get('package_id'),'App\Models\WifiManagerPackages')->invoice();
            else
                Billing::setUser($user)->unsubscribe()->cancelInvoice();

            $data = array('alert'=>'info','msg'=>'The profile has been updated!','title'=>'Success!');
        }
        else if($request->has('update_password'))
        {
            $user->password = bcrypt($input['password']);
            $user->save();
            $data = array('alert'=>'info','msg'=>'The password has been updated!','title'=>'Success!');
        }
        else
        {
            $data = array('alert'=>'danger','msg'=>'Invalid form','title'=>'Error!');
        }

        return redirect()->route('admin.manager.getEdit',$id)->with('data',$data);
    }


}
