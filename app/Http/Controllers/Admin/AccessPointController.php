<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\Nas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;
use SSH;


class AccessPointController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        return view('admin.nas.register');
    }

    /**
     * @param Requests\RegisterNasRequest|Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postRegister(Requests\RegisterNasRequest $request)
    {
        Nas::create($request->input());
        $data = array('alert'=>'info','msg'=>'New NAS has been registered, please restart RADIUS service!','title'=>'Success!');
        return redirect()->route('admin.accessPoint.getRegister')->with('data',$data);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function getList(Request $request)
    {
        //Session::clear();
        //Session::remove('login_82e5d2c56bdd0811318f0cf078b78bfc');
        //dd(Session::all());
        $nas = Nas::paginate(10);
        return view('admin.nas.view')->with('nas',$nas);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function postDelete(Request $request)
    {
        $nas_id = $request->input('nas_id');
        try{
            Nas::find($nas_id)->delete();
            return "OK";
        }
        catch(\Exception $e){
            return "Failed";
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getUpdate(Request $request,$id)
    {
        $nas = Nas::find($id);
        return view('admin.nas.edit')->with('nas',$nas);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\View\View
     */
    public function postUpdate(Request $request,$id)
    {
        $nas = Nas::find($id);
        $nas->update($request->input());
        $data = array('alert'=>'success','msg'=>'NAS updated!','title'=>'Success!');
        return redirect()->route('admin.accessPoint.getUpdate',$nas['id'])->with('data',$data);
    }

    /**
     * Helper get SSH result
     * @param array $commands
     * @return string
     */
    public function _runSSH(array $commands)
    {
        $output = "";
        SSH::run($commands, function($line)
        use (&$output)
        {
            $output = $line.PHP_EOL;
        });
        return $output;
    }

    /**
     * @return $this
     */
    public function getRadius()
    {
        $conf = Configuration::getKeyValues([
            'monitorLastCheck','monitorRadius','monitorAutoStartRadius','monitorDownSendEmail'
        ],['check'=>1,'value'=>'checked','otherwise'=>'']);
        return view('admin.nas.radius')
            ->with('conf',$conf)
            ->with('logs',$this->_runSSH(['cat /var/log/freeradius/radius.log']));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postRadiusStatus(Request $request)
    {
        $output = "";
        SSH::run(['ps -ef |grep -v grep |grep -cw freeradius'], function($line)
        use (&$output)
        {
            $output = $line.PHP_EOL;
        });

        $return = array(
            'radius_status_time' => Carbon::now()->toDateTimeString(),
            'active'=>true,
            'radius_status_text' => 'RADIUS service running'
        );

        if(intval($output)>0){
            $return['radius_status_text'] = 'RADIUS service running';
            $return['active'] = true;
        }
        else{
            $return['radius_status_text'] = 'RADIUS service stopped';
            $return['active'] = false;
        }

        return response()->json($return);
    }

    public function postRadiusAction(Request $request)
    {
        $action = $request->input('action');
        $output = "";
        $cmd = array();
        switch($action){
            case "start":
                $cmd = ['service freeradius start'];
                //update the radiusStopped to false
                Configuration::updateKeyValue(["radiusStopped"=>0]);
                break;
            case "stop":
                $cmd = ['killall -9 freeradius'];
                break;
            case "restart":
                $cmd = ['service freeradius stop','service freeradius start'];
                //update the radiusStopped to false
                Configuration::updateKeyValue(["radiusStopped"=>0]);
                break;
            case "clearlogs":
                $cmd = ['echo "" > /var/log/freeradius/radius.log'];
                break;
            case "getlogs":
                $cmd = ['tail -n500 /var/log/freeradius/radius.log'];
                break;
        }
        SSH::run($cmd, function($line)
        use (&$output)
        {
            $output .= $line.PHP_EOL;
        });
        \Log::info($output);
        return $output;
    }

    public function postRadiusMonitor(Request $request)
    {
        $input = $request->only(['monitorDownSendEmail','monitorAutoStartRadius','monitorRadius']);
        $conf = Configuration::updateKeyValue($input);
        return response()->json($input);
    }


}
