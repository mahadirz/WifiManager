<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('post')) {
            $this->authenticate();
            return redirect()->route('admin.dashboard.index');
        }

        return view('admin.login');
    }

    /**
     * handle login authentication
     */
    public function authenticate()
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function logout(Request $request)
    {
        Session::flush(); // removes all session data
        return redirect()->route('admin.login.index');
    }

}
