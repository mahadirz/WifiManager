<?php namespace App\Http\Controllers\Admin;

use App\Commands\EmailBlaster;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Bus;
use Route;
use Support;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Request;
use App\Models\User;

class SupportController extends Controller {

    /**
     * @return $this
     */
    public function getNewTicket()
    {
        return view('admin.support.newTicket')->with('user',Auth::user());
    }

    /**
     * @param Support $support
     * @return $this
     */
    public function postNewTicket()
    {
        $receiver = User::find(Request::get('user_id_receiver'));
        $support = Support::setSender(Auth::user())
            ->setReceiver($receiver)
            ->createNewTicket(Request::get('title'))
            ->addMessage(Request::get('body'));
        return view('admin.support.ticketCreated')->with('ticket_id',$support->getTicket()->id);
    }

    /**
     * Show all,closed,opened tickets
     * @return $this
     */
    public function getTickets($id)
    {
        if($id == 'all'){
            $tickets = Support::setSender(\Auth::user())->getAllTickets()->displayInbox(\Auth::id());
            return view('admin.support.allTickets')->with('tickets',$tickets);
        }
        else if($id == 'opened'){
            $tickets = Support::setSender(\Auth::user())->GetOpenedTickets()->displayInbox(\Auth::id());
            return view('admin.support.openedTickets')->with('tickets',$tickets);
        }
        else{
            $tickets = Support::setSender(\Auth::user())->GetClosedTickets()->displayInbox(\Auth::id());
            return view('admin.support.closedTickets')->with('tickets',$tickets);
        }

    }

    /**
     * @return $this
     */
    public function postTickets($id)
    {
        if($id == 'all'){
            $tickets = Support::setSender(\Auth::user())
                ->getAllTickets(Request::get('search_keyword'))
                ->displayInbox(\Auth::id());
            return view('admin.support.allTickets')->with('tickets',$tickets);
        }
        else if($id == 'opened'){
            $tickets = Support::setSender(\Auth::user())
                ->GetOpenedTickets(Request::get('search_keyword'))
                ->displayInbox(\Auth::id());
            return view('admin.support.openedTickets')->with('tickets',$tickets);
        }
        else{
            $tickets = Support::setSender(\Auth::user())
                ->GetClosedTickets(Request::get('search_keyword'))
                ->displayInbox(\Auth::id());
            return view('admin.support.closedTickets')->with('tickets',$tickets);
        }

    }

    /**
     * @param $id
     * @return $this
     */
    public function getViewTicket($id)
    {
        $ticketMessages = Support::retrieveTicketMessage($id);
        //dd($messages->getMessage()->first()->user()->first()->getProfilePhotoUrl());
        //dd($ticketMessages);
        return view('admin.support.viewTicket')
            ->with('ticketMessages',$ticketMessages)
            ->with('menu_id_open','menu_all_tickets')
            ->with('user',Auth::user());
    }

    /**
     * Add new message to ticket
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postViewTicket($id)
    {
        if(Request::has('close_ticket')){
            $ticket = Support::initTicket($id)->setTicketToClosed();
        }
        else{
            $ticket = Support::initTicket($id)
                ->setSender(Auth::user())
                ->setTicketToOpen()
                ->addMessage(Request::get('body'));
        }
        return redirect()->route('admin.support.getViewTicket',$id);
    }

    public function getMailBlast()
    {
        return view('admin.support.mailBlast')->with('user',Auth::user());
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postMailBlast()
    {

        $this->dispatch(
            new EmailBlaster(Auth::user(),'wifimanager',
                Request::get('title'),
                Request::get('cc'),
                Request::get('body'))
        );

        $data = array('alert'=>'success','msg'=>'The Email Blast has been initiated!','title'=>'');
        return redirect()->route('admin.support.getMailBlast')->with('data',$data);
    }

}
