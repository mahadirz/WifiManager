<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Invoice;
use Illuminate\Http\Request;
use Billing;

class BillingController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function getPayments()
    {
        return view('admin.billing.invoices')
            ->with('billing',Billing::getInstance())
            ->with('invoices',Invoice::where('packageable_type',"App\\Models\\WifiManagerPackages")->latest()->paginate(10));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPayments(Request $request)
    {
        if($request->has('addPayment')){
            //add payment
            try {
                //get user of invoice
                Billing::setUser(Invoice::find($request->get('invoice_id'))->user);
                Billing::verifyGatewayPayment($request, 'manual');
                //success
                $data = ['alert'=>'success','msg'=>'Payment added to invoice!'];
            } catch (\Exception $e) {
                //error
                //dd($e->getTraceAsString());
                $data = ['alert'=>'danger','msg'=>$e->getMessage()];
            }

        }
        else{
            //cancel invoice
            $data = ['alert'=>'warning','msg'=>'Invoice cancelled!'];
            Billing::cancelInvoice($request->get('invoice_id'));
        }
        return redirect()->route('admin.billing.getPayments')->with('data',$data);
    }

}
