<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\GuestConstraint;
use Carbon\Carbon;
use Event;
use Radius;
use Auth;
use Illuminate\Http\Request;
use Validator;


class GuestController extends Controller
{

    /**
     * @return \Illuminate\View\View
     */
    public function view(Request $request)
    {
        $start_date = !is_null($request->get('start_date'))?Carbon::createFromFormat('d-m-Y',$request->get('start_date')):Carbon::now()->subHour(24);
        $end_date = !is_null($request->get('end_date'))?Carbon::createFromFormat('d-m-Y',$request->get('end_date')):Carbon::now();
        $list = Radius::generateGuestConnectionLists(Auth::user()->username."-guest",$start_date,$end_date);
        //dd($end);
        $data = array();
        foreach($list as $l){
            $start = Carbon::parse($l->acctstarttime);
            $end = is_null($l->acctstoptime)?"Active":Carbon::parse($l->acctstoptime)->format('d-M-Y h:i a');
            $update = Carbon::parse($l->acctupdatetime);
            $latest = $end=="Active"?Carbon::now():Carbon::parse($l->acctstoptime);
            $mac = Radius::convertMacAddress($l->callingstationid);
            $usage = Radius::Bytes_converter($l->acctinputoctets + $l->acctoutputoctets);
            $total_time = round(($l->acctsessiontime /3600),0)." Hours";
            if($total_time=="0 Hours")
                $total_time = round(($l->acctsessiontime /60),0)." Minutes";
            if($total_time=="0 Minutes")
                $total_time = round(($l->acctsessiontime),0)." Seconds";
            if($end == "Active" && $update < Carbon::now()->subDay()){
                //ignore the acct
                continue;
            }
            $data[] = array("mac"=>$mac,"usage"=>$usage,"total_time"=>$total_time,"start"=>$start->format('d-M-Y h:i a'),"end"=>$end,"update"=>$update);
        }

        if ($request->ajax())
        {
            return response()->json($data);
        }
        else{
            return view('user.guest.view')
                ->with('data',$data)
                ->with('start_date',$start_date->format('d-m-Y'))
                ->with('end_date',$end_date->format('d-m-Y'));
        }
    }

    /**
     * @param Requests\UpdateGuestConstraint $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postSettings(Requests\UpdateGuestConstraint $request)
    {
        $data = ['alert'=>'warning','msg'=>'No action performed!'];
        $requetData = $request->all();
        $requetData['enabled'] = false;
        if($request->has('enabled')){
            $requetData['enabled'] = true;
        }
        if($request->has('save_guest')){
            $radius = Radius::setUsername(Auth::user()->username."-guest");
            $guestConstraint = GuestConstraint::firstOrNew(['user_id'=>Auth::id()]);
            if($requetData['enabled']){
                //enable the guest account
                $this->validate($request, [
                    'password' => 'required|min:6|max:255|confirmed'
                ]);
                $radius->setPassword($request->get('password'));
                $radius->addcheck();
                $radius->unSuspendUser();
                $guestConstraint->enabled = true;
                $guestConstraint->user_id = Auth::id();
                $guestConstraint->save();
                $data = ['alert'=>'success','msg'=>'Guest account activated!'];
            }
            else{
                //disable
                //dont have to check password
                $radius->suspendUser();
                $guestConstraint->enabled = false;
                $guestConstraint->user_id = Auth::id();
                $guestConstraint->save();
                $data = ['alert'=>'success','msg'=>'Guest account deactivated!'];
                //dispatch event
                Event::fire(new \App\Events\UserHasBeenBlockedToNas(Auth::user()->username."-guest"));
            }

        }
        else{
            //update guest constraints
            $requetData = $request->all();
            //dd($requetData);
            $requetData['daily_limit_enabled'] = false;
            if($request->has('daily_limit_enabled')){
                $requetData['daily_limit_enabled'] = true;
            }
            Auth::user()->guestConstraint->update($requetData);
            $data = ['alert'=>'success','msg'=>'Guest constraint updated!'];

        }
        return redirect()->route('user.guest.settings')->with('data',$data);
    }

    private function generateProgressBar($current,$limit){
        if($limit ==0)
            $limit = 9999999999;
        $percentage = ($current/$limit) * 100;
        $output['progressBar_percentage'] = $percentage;
        if($percentage > 70)
            $output['progressBar_label'] = "progress-bar-danger";
        else if($percentage > 40)
            $output['progressBar_label'] = "progress-bar-warning";
        else
            $output['progressBar_label'] = "progress-bar-success";
        return $output;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function settings()
    {
        $subscription = Auth::user()->subscription;
        $subscription = is_null($subscription)?0:Auth::user()->subscription->subscribable;
        $guest_connection = is_null($subscription)?0:$subscription->guest_connection_max;
        $guest_connection_array = [0];
        for($i=1;$i<=$guest_connection;$i++){
            array_push($guest_connection_array,$i);
        }
        $constraints = Auth::user()->guestConstraint()->first();
        $guest_username = Auth::user()->username."-guest";
        $accountInfo = array();

        if(!is_null($constraints)){
            //get current connection to NAS
            $simulConn = Radius::getConnectionTotal($guest_username);

            //is daily limit enabled
            $daily_data_limit_enabled = $constraints->daily_limit_enabled;

            //calculate daily data quota
            $tmp = Radius::Bytes_converter(Radius::getDailyDataUsage($guest_username));
            $dailyDataUsage['used'] = $tmp;
            if($daily_data_limit_enabled) {
                $tmp = $this->generateProgressBar(Radius::getDailyDataUsage($guest_username),($constraints->daily_data_quota*1048576));
                if($constraints->daily_data_quota==0) {
                    $dailyDataUsage['limit'] = "&infin;";
                    $dailyDataUsage['progressBar_percentage'] = 10;
                    $dailyDataUsage['progressBar_label'] = "progress-bar-success";
                }
                else {
                    $dailyDataUsage['limit'] = Radius::Bytes_converter($constraints->daily_data_quota*1048576);
                    $dailyDataUsage = array_merge($dailyDataUsage,$tmp);
                }
            }
            else {
                $dailyDataUsage['progressBar_percentage'] = 10;
                $dailyDataUsage['progressBar_label'] = "progress-bar-success";
                $dailyDataUsage['limit'] = "&infin;";
            }

            //calculate daily hours quota
            $tmp = Radius::getDailyHoursUsage($guest_username);
            $dailyHoursUsage['used'] = $tmp;
            if($daily_data_limit_enabled) {
                $tmp = $this->generateProgressBar(Radius::getDailyHoursUsage($guest_username),($constraints->daily_hours_quota));
                if($constraints->daily_hours_quota==0) {
                    $dailyHoursUsage['limit'] = "&infin;";
                    $dailyHoursUsage['progressBar_percentage'] = 10;
                    $dailyHoursUsage['progressBar_label'] = "progress-bar-success";
                }
                else {
                    $dailyHoursUsage['limit'] = $constraints->daily_hours_quota;
                    $dailyHoursUsage = array_merge($dailyHoursUsage,$tmp);
                }
            }
            else {
                $dailyHoursUsage['progressBar_percentage'] = 10;
                $dailyHoursUsage['progressBar_label'] = "progress-bar-success";
                $dailyHoursUsage['limit'] = "&infin;";
            }

            //connection limit
            $connection_limit['used'] = $simulConn;
            $connection_limit['limit'] = $constraints->total_connection;
            $tmp = $this->generateProgressBar($simulConn,$constraints->total_connection);
            $connection_limit = array_merge($connection_limit,$tmp);

            //daily connection hour
            $daily_connection_hour = Carbon::parse($constraints->daily_time_from)->format("h:i a")." - ".
                Carbon::parse($constraints->daily_time_to)->format("h:i a");

            $accountInfo = [
                "daily_data_usage" => $dailyDataUsage,
                "daily_hours_usage" => $dailyHoursUsage,
                "connection_limit" => $connection_limit,
                "daily_connection_hour" => $daily_connection_hour,
                "daily_data_limit_enabled" => $daily_data_limit_enabled,
            ];
        }



        return view('user.guest.settings')
            ->with('guest_connection_array',$guest_connection_array)
            ->with('guestConstraint',$constraints)
            ->with('accountInfo',$accountInfo);
    }

}
