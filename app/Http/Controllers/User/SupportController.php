<?php namespace App\Http\Controllers\User;

use App\Commands\EmailBlaster;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Bus;
use Route;
use Support;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Request;
use App\Models\User;

class SupportController extends Controller {

    /**
     * @return $this
     */
    public function getNewTicket()
    {
        return view('user.support.newTicket')->with('user',Auth::user());
    }

    /**
     * @return $this
     */
    public function postNewTicket()
    {
        $receiver = Auth::user()->manager();
        $support = Support::setSender(Auth::user())
            ->setReceiver($receiver)
            ->createNewTicket(Request::get('title'))
            ->addMessage(Request::get('body'));

        return view('user.support.ticketCreated')->with('ticket_id',$support->getTicket()->id);
    }

    /**
     * Show all,closed,opened tickets
     * @return $this
     */
    public function getTickets($id)
    {
        if($id == 'all'){
            $tickets = Support::setSender(\Auth::user())->getAllTickets()->displayInbox(\Auth::id());
            return view('user.support.allTickets')->with('tickets',$tickets);
        }
        else if($id == 'opened'){
            $tickets = Support::setSender(\Auth::user())->GetOpenedTickets()->displayInbox(\Auth::id());
            return view('user.support.openedTickets')->with('tickets',$tickets);
        }
        else{
            $tickets = Support::setSender(\Auth::user())->GetClosedTickets()->displayInbox(\Auth::id());
            return view('user.support.closedTickets')->with('tickets',$tickets);
        }

    }

    /**
     * @return $this
     */
    public function postTickets($id)
    {
        if($id == 'all'){
            $tickets = Support::setSender(\Auth::user())
                ->getAllTickets(Request::get('search_keyword'))
                ->displayInbox(\Auth::id());
            return view('user.support.allTickets')->with('tickets',$tickets);
        }
        else if($id == 'opened'){
            $tickets = Support::setSender(\Auth::user())
                ->GetOpenedTickets(Request::get('search_keyword'))
                ->displayInbox(\Auth::id());
            return view('user.support.openedTickets')->with('tickets',$tickets);
        }
        else{
            $tickets = Support::setSender(\Auth::user())
                ->GetClosedTickets(Request::get('search_keyword'))
                ->displayInbox(\Auth::id());
            return view('user.support.closedTickets')->with('tickets',$tickets);
        }

    }

    /**
     * @param $id
     * @return $this
     */
    public function getViewTicket($id)
    {
        if(Support::checkUserCanViewMessage(Auth::id(),$id)){
            $ticketMessages = Support::retrieveTicketMessage($id);
            //dd($messages->getMessage()->first()->user()->first()->getProfilePhotoUrl());
            return view('user.support.viewTicket')
                ->with('ticketMessages',$ticketMessages)
                ->with('menu_id_open','menu_all_tickets')
                ->with('user',Auth::user());
        }

    }

    /**
     * Add new message to ticket
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postViewTicket($id)
    {
        if(Request::has('close_ticket')){
            $ticket = Support::initTicket($id)->setTicketToClosed();
        }
        else{
            $ticket = Support::initTicket($id)
                ->setSender(Auth::user())
                ->setTicketToOpen()
                ->addMessage(Request::get('body'));
        }
        return redirect()->route('user.support.getViewTicket',$id);
    }




}
