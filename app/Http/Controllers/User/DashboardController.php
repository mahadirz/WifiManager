<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Auth;
use Radius;
use App\Models\User;
use Hash;
use JavaScript;
use Request;


class DashboardController extends Controller
{

    public function __construct()
    {
        JavaScript::put([
            'user_id' => Auth::id(),
            'csrf_token'=>csrf_token()
        ]);
    }

    private function generateProgressBar($current,$limit){
        if($limit ==0)
            $limit = 9999999999;
        $percentage = ($current/$limit) * 100;
        $output['progressBar_percentage'] = $percentage;
        if($percentage > 70)
            $output['progressBar_label'] = "progress-bar-danger";
        else if($percentage > 40)
            $output['progressBar_label'] = "progress-bar-warning";
        else
            $output['progressBar_label'] = "progress-bar-success";
        return $output;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        /** @var User $user */
        $user = User::find(Auth::id());

        //get accounting information
        $sql = "SELECT u.id AS user_id, CONCAT( m_u.first_name,  \" \", m_u.last_name ) AS manager_name, manager_s.ends_at AS manager_ends_at, manager_s.suspended AS manager_suspended, user_s.suspended AS user_suspended, manager_s.starts_at AS manager_starts_at, user_s.starts_at AS user_starts_at, user_s.ends_at AS user_ends_at, up.data_limit, up.max_connection_limit, up.daily_limit_enabled, up.daily_data_limit, up.daily_hours_max, up.daily_time_from, up.daily_time_to
FROM  `manager` AS m
LEFT JOIN  `users` AS m_u ON m_u.id = m.manager_id
LEFT JOIN  `users` AS u ON u.id = m.user_id
LEFT JOIN  `subscriptions` AS manager_s ON manager_s.user_id = m.manager_id
LEFT JOIN  `subscriptions` AS user_s ON user_s.user_id = m.user_id
LEFT JOIN  `userPackages` AS up ON up.id = user_s.subscribable_id
WHERE u.username =  '$user->username' LIMIT 1";
        $accounting = DB::select($sql)[0];

        //Account Status	Active
        //check is user is subscribe to package
        $package = is_null(Auth::user()->subscription)?null:Auth::user()->subscription->subscribable;
        $status['text'] = "active";
        $status['label'] = "label-success";
        if($accounting->user_suspended == 1){
            $status['text'] = "Suspended";
            $status['label'] = "label-danger";
        }
        else if(is_null($package)){
            $status['text'] = "No Package defined";
            $status['label'] = "label-danger";
        }
        else if($accounting->manager_suspended == 1){
            $status['text'] = "Manager Suspended";
            $status['label'] = "label-warning";
        }

        //get current connection to NAS
        $simulConn = Radius::getConnectionTotal($user->username);
        if($simulConn>0){
            $connection_status['text'] = "Connected";
            $connection_status['label'] = "label-info";
        }
        else{
            $connection_status['text'] = "Disconnected";
            $connection_status['label'] = "label-primary";
        }

        //is daily limit enabled
        $daily_data_limit_enabled = $accounting->daily_limit_enabled;

        //calculate daily data quota
        $tmp = Radius::Bytes_converter(Radius::getDailyDataUsage($user->username));
        $dailyDataUsage['used'] = $tmp;
        if($daily_data_limit_enabled) {
            $tmp = $this->generateProgressBar(Radius::getDailyDataUsage($user->username),($accounting->daily_data_limit*1048576));
            if($accounting->daily_data_limit==0) {
                $dailyDataUsage['limit'] = "&infin;";
                $dailyDataUsage['progressBar_percentage'] = 10;
                $dailyDataUsage['progressBar_label'] = "progress-bar-success";
            }
            else {
                $dailyDataUsage['limit'] = Radius::Bytes_converter($accounting->daily_data_limit*1048576);
                $dailyDataUsage = array_merge($dailyDataUsage,$tmp);
            }
        }
        else {
            $dailyDataUsage['progressBar_percentage'] = 10;
            $dailyDataUsage['progressBar_label'] = "progress-bar-success";
            $dailyDataUsage['limit'] = "&infin;";
        }

        //calculate daily hours quota
        $tmp = Radius::getDailyHoursUsage($user->username);
        $dailyHoursUsage['used'] = $tmp;
        if($daily_data_limit_enabled) {
            $tmp = $this->generateProgressBar(Radius::getDailyHoursUsage($user->username),($accounting->daily_hours_max));
            if($accounting->daily_hours_max==0) {
                $dailyHoursUsage['limit'] = "&infin;";
                $dailyHoursUsage['progressBar_percentage'] = 10;
                $dailyHoursUsage['progressBar_label'] = "progress-bar-success";
            }
            else {
                $dailyHoursUsage['limit'] = $accounting->daily_hours_max;
                $dailyHoursUsage = array_merge($dailyHoursUsage,$tmp);
            }
        }
        else {
            $dailyHoursUsage['progressBar_percentage'] = 10;
            $dailyHoursUsage['progressBar_label'] = "progress-bar-success";
            $dailyHoursUsage['limit'] = "&infin;";
        }

        //connection limit
        $connection_limit['used'] = $simulConn;
        $connection_limit['limit'] = $accounting->max_connection_limit;
        $tmp = $this->generateProgressBar($simulConn,$accounting->max_connection_limit);
        $connection_limit = array_merge($connection_limit,$tmp);

        //daily connection hour
        $daily_connection_hour = Carbon::parse($accounting->daily_time_from)->format("h:i a")." - ".
            Carbon::parse($accounting->daily_time_to)->format("h:i a");

        $accountInfo = [
            "manager" => $accounting->manager_name,
            "status" => $status,
            "connection_status" => $connection_status,
            "daily_data_usage" => $dailyDataUsage,
            "daily_hours_usage" => $dailyHoursUsage,
            "connection_limit" => $connection_limit,
            "daily_connection_hour" => $daily_connection_hour,
            "daily_data_limit_enabled" => $daily_data_limit_enabled,
        ];

        //dd($accountInfo);

        $totalTransfer = Radius::getAcctInputOutputOctets($user->username);
        $totalUpload = Radius::getAcctInputOutputOctets($user->username,"upload");
        $totalDownload = Radius::getAcctInputOutputOctets($user->username,"download");
        $widgets = [
            array(
                "title" => "Total Data Transfer",
                "within" => Carbon::now()->subMonth()->format("d/m/Y") . "-" . Carbon::now()->format("d/m/Y"),
                "value" => Radius::Bytes_converter($totalTransfer)
            ),
            array(
                "title" => "Total Data Uploaded",
                "within" => Carbon::now()->subMonth()->format("d/m/Y") . "-" . Carbon::now()->format("d/m/Y"),
                "value" => Radius::Bytes_converter($totalUpload)
            ),
            array(
                "title" => "Total Data Downloaded",
                "within" => Carbon::now()->subMonth()->format("d/m/Y") . "-" . Carbon::now()->format("d/m/Y"),
                "value" => Radius::Bytes_converter($totalDownload)
            )
        ];

        //dd(Radius::getDailyHoursUsage($user->username));
        //dd(Radius::getDailyDataUsage($user->username));

        return view('user.index')->with("widgets",$widgets)->with('accountInfo',$accountInfo);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getSettings()
    {
        $user = User::find(Auth::id());
        return view('user.settings')->with('user',$user);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function postSettings()
    {
        $user = User::find(Auth::id());
        if(Request::has('update_profile'))
        {
            $user->update(Request::all());
            $data = array('alert'=>'info','msg'=>'The Profile has been updated','title'=>'Success!');
        }
        else if(Request::has('update_email'))
        {
            if(Hash::check(Request::get('password'),$user->password) ){
                $user->email = Request::get('email');
                $user->save();
                $data = array('alert'=>'info','msg'=>'The email has been updated','title'=>'Success!');
            }
            else{
                $data = array('alert'=>'danger','msg'=>'Password incorrect!','title'=>'Error!');
            }

        }
        else if(Request::has('update_signature')){
            $user->profile->signature = Request::get('signature');
            $user->profile->save();
            $data = array('alert'=>'info','msg'=>'The Signature has been updated','title'=>'Success!');
        }
        else
        {
            $data = array('alert'=>'danger','msg'=>'Error performing actions!','title'=>'Error!');
        }

        return redirect()->route('user.dashboard.getSettings')->with('data',$data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getStatistics()
    {
        $usageStats = Radius::generateUsageStats(Auth::user()->username);
        $js1 = "";
        for($i=0;$i<count($usageStats);$i++){
            $date = $usageStats[$i]->date;
            $download = round($usageStats[$i]->download,2);
            $upload = round($usageStats[$i]->upload,2);
            $total = round($usageStats[$i]->total,2);
            $js1 .= "{x:'$date',a:$download,b:$upload,c:$total},";
        }
        $js1 = substr($js1,0,strlen($js1)-1);

        $hourStats = Radius::generateHourStats(Auth::user()->username);
        $js2 = "";
        for($i=0;$i<count($hourStats);$i++){
            $seconds = $hourStats[$i]->total;
            $total = round($seconds/3600,2);
            $date = $hourStats[$i]->date;
            $js2 .= "{x:'$date',a:$total},";
        }
        $js2 = substr($js2,0,strlen($js2)-1);
        //dummy data
        /*
        $js2 = "{x:'".Carbon::now()->subDay(30)->format("Y-m-d")."',a:".rand(1,1000)."}";
        for($i=29;$i>0;$i--){
            $js2 .= ",{x:'".Carbon::now()->subDay($i)->format("Y-m-d")."',a:".rand(1,1000)."}";
        }
        */
        return view('user.statistics')
            ->with('usageStats',$js1)->with('hourStats',$js2);
    }

}
