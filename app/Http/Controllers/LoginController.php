<?php namespace App\Http\Controllers;

use App\Commands\Emailer;
use App\Events\UserPasswordWasChanged;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Google2fa as Google2faModel;
use App\Models\LoginAttempt;
use App\Models\User;
use Auth;

use Carbon\Carbon;
use Config;
use DB;
use Event;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Mail;
use PragmaRX\Google2FA\Contracts\Google2FA;
use Redirect;
use Session;
use Validator;

class LoginController extends Controller
{


    protected $maxLoginAttempts;
    protected $loginLockMinutes;

    public function __construct(Request $request)
    {
        $this->maxLoginAttempts = env('LOGIN_ATTEMPT_MAX',3);
        $this->loginLockMinutes = env('LOGIN_LOCK_MINUTES',5);

        if($request->route()->getName() != "login.lockOut"){
            //check for ip blacklisted
            $this->blacklist($request);
        }

    }

    /**
     * Show 2Fa verification page
     * @return \Illuminate\View\View
     */
    public function show2Fa()
    {
        if(!Session::has('tfa_user_id'))
            return redirect('login.index');
        return view('auth.tfa');
    }

    /**
     * @param Google2FA $google2FA
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postProcess2Fa(Google2FA $google2FA,Request $request)
    {

        if(!Session::has('tfa_user_id')||!$request->has('token') ||$request->get('token')==null)
            return redirect('login.index');
        $user_id = Session::get('tfa_user_id');
        $rememberMe = Session::get('tfa_remember_me',false);
        $token = $request->get('token');
        $google2faModel = Google2faModel::where('user_id',$user_id)->first();

        //check if token is in backupcode or backupcode2
        if($google2FA->verifyKey($google2faModel->secretkey, $token))
        {
            //the token is valid
        }
        else if($token == $google2faModel->backupcode)
        {
            //set the token to NULL, (used)
            $google2faModel->backupcode = null;
        }
        else if($token == $google2faModel->backupcode2)
        {
            //set the token to NULL, (used)
            $google2faModel->backupcode2 = null;
        }
        else
        {
            //the token invalid
            $this->blacklist($request,false);
            Session::flash('alert','danger');
            Session::flash('message', "The token entered is invalid!");
            return redirect()->back();
        }

        //the token valid
        $google2faModel->save();
        //reset tfa session
        Session::remove('tfa_user_id');
        Session::remove('tfa_remember_me');
        //log in using user_id
        Auth::loginUsingId($google2faModel->user_id,$rememberMe);
        //well now, let's check who this user might be
        //and direct to its panel
        if(Auth::user()->hasRole('user'))
            return redirect()->route('user.dashboard.index');
        else if(Auth::user()->hasRole('wifimanager'))
            return redirect()->route('manager.dashboard.index');
        else if(Auth::user()->hasRole('administrator'))
            return redirect()->route('admin.dashboard.index');
        else
            return redirect()->back();

    }

    /**
     * Show locout page
     * @return \Illuminate\View\View
     */
    public function lockOut()
    {
        //dd(Session::all());
        return view('auth.loginLockOut');
    }

    /**
     * @param Request $request
     * @param bool|true $check_only
     * @return $this|bool
     */
    protected function blacklist(Request $request,$check_only=true)
    {
        //protection against brute force login
        $ip = $request->getClientIp();

        if($check_only)
        {
            //only check whithout modifying db
            $loginAttempts = LoginAttempt::where('ip',$ip)
                ->where('attempts','>',$this->maxLoginAttempts)
                ->where('created_at','>',Carbon::now()->subMinute($this->loginLockMinutes))
                ->first();

            if(!is_null($loginAttempts))
            {
                //condition triggered
                /** @var Carbon $lockDateTimeout */
                $lockDateTimeout = $loginAttempts->created_at;
                //display lock out page
                Session::put('lockDateTimeout',$lockDateTimeout->addMinute($this->loginLockMinutes));
                Redirect::route('login.lockOut')->send();
            }

        }
        else
        {
            //log the attempt
            $loginAttempts = LoginAttempt::where('ip',$ip)
                ->where('created_at','>',Carbon::now()->subMinute($this->loginLockMinutes))
                ->first();
            if(is_null($loginAttempts)){
                $loginAttempts = new LoginAttempt();
                $loginAttempts->ip = $ip;
                $loginAttempts->attempts = 1;
            }
            else
                $loginAttempts->attempts++;

            $loginAttempts->username = $request->get('username');
            $loginAttempts->save();

        }
        return false;
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        //if user already logged-in bring him to dashboard
        if(!is_null(Auth::user())){
            return $this->redirectToDashboard();
        }
        //display normal login page
        return view('auth.login');
    }

    /**
     * handle login authentication
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function authenticate(Request $request)
    {
        //ensure the username & password field exist
        if(!$request->has('username') || !$request->has('password'))
        {
            //this field not exist
            Session::flash('alert','danger');
            Session::flash('message', "The username or password entered is incorrect!");
            return redirect()->back();
        }

        $input = $request->only(['username','password']);
        $rememberMe = $request->has('remember_me');
        if(filter_var($request->get('username'), FILTER_VALIDATE_EMAIL))
        {
            //modify array to email
            $input['email'] = $input['username'];
            unset($input['username']);
        }

        if(Auth::validate($input))
        {
            //credentials is valid
            //next check if enabled 2FA
            $user = User::where('username',$request->get('username'))
                ->orWhere('email',$request->get('username'))
                ->first();
            if(!is_null($user->google2fa) && $user->google2fa->enabled)
            {
                //2FA enabled, so send to 2FA route first
                //but wait, we must tell the 2FA about the credential
                //is already validated, so send user_id
                Session::set('tfa_user_id',$user->id);
                Session::set('tfa_remember_me',$rememberMe);
                return redirect()->route('login.show2Fa');
            }
            else
            {
                //the user not enabled 2FA, login as normal
                Auth::loginUsingId($user->id,$rememberMe);

                //the user might try to access certain url but not authenticated
                //redirect back to that page
//                if(Session::has('_previous') && isset(Session::get('_previous')['url'])){
//                    dd(Session::get('_previous')['url']);
//                    Session::remove('_previous');
//                    if (filter_var(Session::get('_previous')['url'], FILTER_VALIDATE_URL)){
//                        return redirect()->to(Session::get('_previous')['url']);
//                    }
//                }


                //well now, let's check who this user might be
                //and direct to its panel
               // return $this->redirectToDashboard();
                if(Auth::user()->hasRole('user'))
                    return Redirect::route('user.dashboard.index');
                else if(Auth::user()->hasRole('wifimanager'))
                    return redirect()->route('manager.dashboard.index');
                else if (Auth::user()->hasRole('administrator'))
                    return redirect()->route('admin.dashboard.index');
                else
                    return "Error";

                //end of authentication procedure
            }

        }
        else
        {
            //invalid
            //add to blacklist
            $this->blacklist($request,false);
            Session::flash('alert','danger');
            Session::flash('message', "The username or password entered is incorrect!");
            return redirect()->back();
        }
    }

    /**
     * Redirect the user according to role
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectToDashboard()
    {
        if(Auth::user()->hasRole('user'))
            return Redirect::route('user.dashboard.index')->send();
        else if(Auth::user()->hasRole('wifimanager'))
            return redirect()->route('manager.dashboard.index')->send();
        else if (Auth::user()->hasRole('administrator'))
            return redirect()->route('admin.dashboard.index')->send();
        return "Error";
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function logout(Request $request)
    {
        Auth::logout();
        Session::flush(); // removes all session data
        return redirect()->route('login.index');
    }

    /**
     * Page for password reset form
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getEmail(Request $request)
    {
        return view('auth.email');
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function postEmail(Request $request)
    {
        $username = $request->input('username');
        $email = $request->input('email');
        /** @var User $user */
        $user = User::where('username','=',$username)->where('email','=',$email)->first();
        if(!is_null($user))
        {
            //$token = hash('sha256',openssl_random_pseudo_bytes(32));
            $key = Config::get('app.key');
            $token = hash_hmac('sha256', str_random(40), $key);

            //use Emailer
            $sender = User::where('first_name','System')->first();
            $emailer = new Emailer($sender,
                $user,
                'Wifi Manager Password Recovery',
                ['username' => $user->username,'token'=>$token]
            );
            $emailer->view = 'auth.password';
            $this->dispatch($emailer);

//            Mail::queue(['html' => 'auth.password'], ['username' => $user->username,'token'=>$token], function(Message $message)
//            use ($user)
//            {
//                $message->from(env('EMAIL_PASSWORD_RESET','password_recovery@wifimanager.app'), 'Wifi Manager (HEWMS)');
//                $message->to($user->email, $user->getFullName())->subject('Wifi Manager Password Recovery');
//            });

            //reset any other token that exist
            DB::table('password_resets')->where('email', '=', $user->email)->delete();

            DB::table('password_resets')->insert(
                ['email' => $user->email, 'token' => $token,'created_at'=>Carbon::now()]
            );

            Session::flash('message','Your password reset link has been emailed to you');
            Session::flash('alert','success');
            return view('auth.email');
        }

        Session::flash('message','Cannot find the associated user in database');
        Session::flash('alert','danger');
        return view('auth.email');
    }


    /**
     * @param $token
     * @return mixed|static
     */
    protected function getUserFromToken($token)
    {
        $user = DB::table('password_resets')
            ->where('token', $token)
            ->where('created_at','>',Carbon::now()->subHour(env('RESET_TOKEN_VALID',1)))
            ->first();
        return $user;
    }

    /**
     * Show reset password page after user visit link
     * @param $token
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getReset($token,Request $request)
    {
        $user = $this->getUserFromToken($token);
        if(is_null($user))
        {
            //the token isn't valid
            return redirect()->route('login.index');
        }
        return view('auth.reset');
    }

    /**
     * @param $token
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function postReset($token,Request $request)
    {
        $user = $this->getUserFromToken($token);
        if(is_null($user))
        {
            //the token isn't valid
            return redirect()->route('login.index');
        }

        $validator = Validator::make(
            $request->input(),
            [
                'password' => 'required|min:6|max:200',
                'password_confirmation' => 'required|same:password'
            ]
        );

        if ($validator->fails())
        {
            // The given data did not pass validation
            //dd($validator->messages());
            return redirect()->back()->withErrors($validator);
        }

        $userModel = User::where('email','=',$user->email)->first();
        $userModel->password = bcrypt($request->input('password'));
        $userModel->save();
        //fire event
        Event::fire(new UserPasswordWasChanged($userModel,$request->input('password')));

        DB::table('password_resets')->where('email', '=', $user->email)->delete();

        Session::flash('message','Your password has been reset, you may login using your new password');
        Session::flash('alert','success');
        return redirect()->route('login.index');
    }

}
