<?php namespace App\Http\Controllers\Ajax;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use PragmaRX\Google2FA\Contracts\Google2FA;
use App\Models\Google2fa as Google2faModel;
use Request;

class Security extends Controller {


    /**
     * @param $len
     * @return string
     */
    private function genRandomDigit($len=10)
    {
        $digit = "";
        for($i=0;$i<$len;$i++){
            $digit .= mt_rand(0,9);
        }
        return $digit;
    }

    /**
     * @param Google2FA $google2fa
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postGenerate2FaQRCodeUrl(Google2FA $google2fa)
    {
        try{
            $secret = $google2fa->generateSecretKey(32);
            //\Log::info(print_r($secret,1));
            $google2faModel = Google2faModel::firstOrNew(['user_id'=>Auth::id()]);
            $google2faModel->user_id = Auth::id();
            $google2faModel->secretkey = $secret;
            $google2faModel->backupcode = $this->genRandomDigit();
            $google2faModel->backupcode2 = $this->genRandomDigit();
            $google2faModel->save();
            $google2fa_url = $google2fa->getQRCodeGoogleUrl(
                'Wifi-Manager-Application',
                $google2faModel->user()->first()->email,
                $secret
            );
            return response()->json(['image_url'=>$google2fa_url]);
        }
        catch(\Exception $e){
            return response()->json(['Exception'=>$e->getMessage()]);
        }

    }

    /**
     * Ajax disable 2 FA
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postDisable2Fa()
    {
        try {
            $google2faModel = Google2faModel::firstOrNew(['user_id'=>Auth::id()]);
            $google2faModel->user_id = Auth::id();
            $google2faModel->enabled = 0;
            $google2faModel->save();
            return response()->json(['success'=>true]);

        } catch (\Exception $e) {
            return response()->json(['success'=>false,'msg'=>$e->getMessage()]);
        }

    }

    /**
     * @param Google2FA $google2fa
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postVerify2FaQRCode(Google2FA $google2fa)
    {
        try{
            if(Request::has('code')){
                $code = Request::get('code');
                $user = Auth::user();
                $valid = $google2fa->verifyKey($user->google2fa->secretkey, $code);
                if($valid){
                    $user->google2fa->enabled = 1;
                    $user->google2fa->save();
                    return response()->json([
                        'valid'=>true,
                        'backupcode'=>$user->google2fa->backupcode,
                        'backupcode2'=>$user->google2fa->backupcode2,
                    ]);
                }
                return response()->json(['valid'=>false]);
            }
        }
        catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }
        return response()->json(['valid'=>false,'msg'=>'empty code']);
    }

}
