<?php namespace App\Http\Controllers\Ajax;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Support as SupportLibrary;
use Illuminate\Http\Request;
use URL;

class Support extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function postMessages()
    {
        $messages = SupportLibrary::retrieveUnreadMessages(Auth::id())->getMessages();
        $arrayMessage = [];

        foreach($messages as $m){
            $ticket_url = URL::route('admin.support.getViewTicket',$m->ticket->id);
            if(Auth::user()->hasRole('wifimanager')){
                $ticket_url = URL::route('manager.support.getViewTicket',$m->ticket->id);
            }
            else if(Auth::user()->hasRole('user')){
                $ticket_url = URL::route('user.support.getViewTicket',$m->ticket->id);
            }
            array_push($arrayMessage,[
                "profile_url"=>$m->user->getProfilePhotoUrl(),
                "title" => substr($m->ticket->title,0,45),
                "full_name" => $m->user->getFullName(),
                "ticket_status" => $m->ticket->status,
                "ticket_url" => $ticket_url
            ]);
        }
        $data = ['count'=>count($messages),'messages'=>$arrayMessage];
        return response()->json($data);
    }

}
