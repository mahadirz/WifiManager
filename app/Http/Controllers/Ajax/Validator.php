<?php namespace App\Http\Controllers\Ajax;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;
use App\Libraries\RequestValidator;
use Log;

class Validator extends Controller {

    protected $requestValidator;
    protected $request;

    /**
     * @param Request $request
     * @param RequestValidator $requestValidator
     */
    public function __construct(Request $request,RequestValidator $requestValidator)
    {
        $this->requestValidator = $requestValidator;
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function postUsername()
    {
        //Log::info(print_r($this->request->input('username'),1));
        $username = $this->request->input('username');
        if(!$this->requestValidator->username($username)->fails())
        {
            return response()->json('true');
        }
        $msg = $this->requestValidator->getLastValidator()->messages()->first();
        return response()->json($msg);
    }

    /**
     * @return string
     */
    public function postEmail()
    {
        $email = $this->request->input('email');
        $user_id=$this->request->input('user_id');
        //Log::info(print_r($user_id,1));
        if(!$this->requestValidator->email($email,$user_id)->fails())
        {
            return response()->json('true');
        }
        $msg = $this->requestValidator->getLastValidator()->messages()->first();
        //Log::critical(print_r($msg,1));
        return response()->json($msg);
    }

}
