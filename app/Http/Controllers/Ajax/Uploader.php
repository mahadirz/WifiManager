<?php namespace App\Http\Controllers\Ajax;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use Auth;
use Request;
use URL;

class Uploader extends Controller {


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postProfile()
    {
        if (Request::hasFile('photo'))
        {
            $file = Request::file('photo');
            $mimeType = $file->getMimeType();
            if ($file->isValid() && ($mimeType == "image/jpeg" ||
                    $mimeType =="image/png" || $mimeType == "image/gif" || $mimeType =="image/x-ms-bmp"))
            {
                //generate file_id
                $user = User::find(Auth::id());
                $photo_id = $user->getProfilePhotoId();
                $photo_name = $photo_id.User::PHOTO_EXT;
                Request::file('photo')->move(base_path()."/".User::PHOTO_LOCATION,$photo_name);

                return response()->json(['photo_path'=>$user->getProfilePhotoUrl()]);
            }
            return response()->json(['error'=>'Invalid file.']);
        }
        return response()->json(['error'=>'No files found for upload.']);
    }

}
