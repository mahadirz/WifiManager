<?php namespace App\Http\Controllers\Ajax;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Role;
use App\Models\User;
use Auth;
use DB;
use Entrust;
use Request;

class AutoComplete extends Controller {

    /**
     * Generate list of auto name suggestion
     * for ticket recipient
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postReceiverAutoComplete()
    {
        $data = array('suggestions'=>array());
        try {
            if(Entrust::hasRole('administrator')){
                //admin can send to manager
                $users = Role::whereName('wifimanager')->first()->users()->get();
                $q2 = Role::with('users')->where('name','wifimanager');
            }
            else if(Entrust::hasRole('wifimanager')){
                //manager can send to admin and user
                $user1 = Role::whereName('administrator')->first()->users()->get();
                $user2 = Auth::user()->manage()->get('users');
                $users = $user1->merge($user2);
            }
            else{
                //user can send to manager only
                $users = Role::whereName('wifimanager')->first()->users()->get();
                $q2 = Role::with('users')->where('name','wifimanager');
            }

            $keyword = Request::get('keyword');

            if(count($users)>5){
                //try showing by input entered

                if(Entrust::hasRole('wifimanager')){

                    $user_ids = DB::table('manager')->select('user_id')->join('users','users.id','=','manager.user_id')
                        ->where('manager_id', Auth::id())->where(function ($q)
                        use ($keyword) {
                            $q->where('first_name', 'like', '%' . $keyword . '%')
                                ->orWhere('email', 'like', '%' . $keyword . '%')
                                ->orWhere('username', 'like', '%' . $keyword . '%');

                        })->get();
                    foreach ($user_ids as $u) {
                        $user = User::find($u->user_id);
                        array_push($data['suggestions'], [
                            'value' => $user->getFullName(),
                            'data' => $u->user_id,
                            'profile_photo_url' => $user->getProfilePhotoUrl()
                        ]);
                    }

                }
                else{

                    $role = $q2->whereHas('users', function ($q)
                    use ($keyword) {
                        $q->where('first_name', 'like', '%' . $keyword . '%')
                            ->orWhere('email', 'like', '%' . $keyword . '%')
                            ->orWhere('username', 'like', '%' . $keyword . '%');

                    })->get()->first();


                    foreach ($role->users as $role) {
                        array_push($data['suggestions'], [
                            'value' => $role->users->first_name . " " . $role->users->last_name,
                            'data' => $role->users->id,
                            'profile_photo_url' => $role->users->getProfilePhotoUrl()
                        ]);
                    }
                }
            }
            else if(count($users)>0){
                foreach($users as $user)
                {
                    array_push($data['suggestions'],[
                        'value'=>$user->first_name." ".$user->last_name,
                        'data'=>$user->id,
                        'profile_photo_url' => $user->getProfilePhotoUrl()
                    ]);
                }
            }
            else{
                $data = [];
            }

        } catch (\Exception $e) {
            $data = $e->getMessage();
        }


        return response()->json($data);
    }

}
