<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\Role;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;

class Emailer extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

    /**
     * @var User
     */
    public $sender;
    /**
     * @var User
     */
    public $receiver;
    /**
     * @var string
     */
    public $subject;
    /**
     * @var array
     */
    public $cc = array();
    /**
     * @var string
     */
    public  $rawCc;
    /**
     * @var string
     */
    public $body;

    /**
     * @var string
     */
    public $view = 'emails.general';

    /**
     * @var string
     */
    public $type = 'html';

    /**
     * Create a new command instance.
     *
     * @param User $sender
     * @param User $receiver
     * @param string $subject
     * @param string|array $body
     * @param string $cc
     */
	public function __construct(User $sender,User $receiver,$subject,$body,$cc='')
	{
		$this->sender = $sender;
        $this->receiver = $receiver;
        $this->subject = $subject;
        $this->rawCc = $cc;

        $ccs = explode(';',$cc);
        foreach($ccs as $c){
            if(filter_var($c,FILTER_VALIDATE_EMAIL)){
                array_push($this->cc,$c);
            }
        }

        $this->body = $body;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$receiver = $this->receiver;
        $senderFullName = $this->sender->getFullName();
        $senderEmail = $this->sender->email;
        $subject = $this->subject;
        $cc = $this->cc;

        $body = is_array($this->body)?$this->body:['body'=>$this->body];

        Mail::send([$this->type => $this->view],$body, function($message)
        use ($senderFullName,$senderEmail,$subject,$cc,$receiver)
        {
            $message->from($senderEmail, $senderFullName);
            $message->subject($subject);

            $message->to($receiver->email,$receiver->getFullName());
            if(count($cc)>0){
                foreach($cc as $email){
                    $message->cc($email);
                }
            }

        });

        $body = view($this->view,$body);

        //save copy to database
        $mail = \App\Models\Mail::create([
            'body' => $body,
            'email_sender' => $this->sender->email,
            'email_receiver' => $receiver->email,
            'subject' => $this->subject,
            'cc' => $this->rawCc
        ]);
        $mail->sender_user_id = $this->sender->id;
        $mail->receiver_user_id = $receiver->id;
        $mail->save();
	}

}
