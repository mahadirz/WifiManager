<?php namespace App\Commands;

use App\Commands\Command;

use App\Models\Role;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;

class EmailBlaster extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

    /**
     * @var User
     */
    protected $sender;
    /**
     * @var string
     */
    protected $receiverRoleName;
    /**
     * @var string
     */
    protected $subject;
    /**
     * @var array
     */
    protected $cc = array();
    /**
     * @var string
     */
    protected  $rawCc;
    /**
     * @var string
     */
    protected $body;

    /**
     * Create a new command instance.
     *
     * @param User $sender
     * @param string $receiverRoleName
     * @param string $subject
     * @param string $cc
     * @param string $body
     */
	public function __construct(User $sender,$receiverRoleName,$subject,$cc,$body)
	{
		$this->sender = $sender;
        $this->receiverRoleName = $receiverRoleName;
        $this->subject = $subject;
        $this->rawCc = $cc;

        $ccs = explode(';',$cc);
        foreach($ccs as $c){
            if(filter_var($c,FILTER_VALIDATE_EMAIL)){
                array_push($this->cc,$c);
            }
        }

        $this->body = $body;
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		$users = Role::whereName($this->receiverRoleName)->first()->users;
        $senderFullName = $this->sender->getFullName();
        $senderEmail = $this->sender->email;
        $subject = $this->subject;
        $cc = $this->cc;
        foreach($users as $user){

            //filter user
            if($this->receiverRoleName == "user"){
                if ($user->manager()->id != $this->sender->id) {
                    //the user dont belong to manager
                    continue;
                }
            }

            /** @var User $user */
            //make replacement for variables
            $body = $this->body;
            $body = str_replace('{username}',$user->username,$body);
            $body = str_replace('{fullname}',$user->getFullName(),$body);
            $body = str_replace('{firstname}',$user->first_name,$body);
            $body = str_replace('{lastname}',$user->last_name,$body);
            $body = str_replace('{email}',$user->email,$body);

            Mail::send(['html' => 'admin.email.blast'],['body'=>$body], function($message)
                use ($senderFullName,$senderEmail,$subject,$cc,$user)
            {
                $message->from($senderEmail, $senderFullName);
                $message->subject($subject);

                $message->to($user->email,$user->getFullName());
                if(count($cc)>0){
                    foreach($cc as $email){
                        $message->cc($email);
                    }
                }

            });

            //save copy to database
            $mail = \App\Models\Mail::create([
                'body' => $body,
                'email_sender' => $this->sender->email,
                'email_receiver' => $user->email,
                'subject' => $this->subject,
                'cc' => $this->rawCc
            ]);
            $mail->sender_user_id = $this->sender->id;
            $mail->receiver_user_id = $user->id;
            $mail->save();
        }
	}

}
